package brunonm.nfce.utils;

import org.ksoap2.serialization.SoapObject;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.ModeloNF;
import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;

/**
 * Classe responsável por criar o Xml de requisição do Status do Serviço dos
 * webServices do Sefaz
 * 
 * @author Bruno Melo
 *
 */
public class CriadorXmlStatusServico implements CriadorXml {

	private static final String NAMESPACE = "http://www.portalfiscal.inf.br/nfe";

	private TpAmb tpAmb;
	private Estados estados;
	private Versao versao;

	/**
	 * Construtor com os parâmetros necessários para montar a requisição
	 * 
	 * @param estados
	 *            O estado do Webservice
	 * @param versao
	 *            A Versão da NF
	 * @param tpAmb
	 *            O tipo de ambiente requisitado
	 */
	public CriadorXmlStatusServico(Estados estados, Versao versao, TpAmb tpAmb) {
		this.tpAmb = tpAmb;
		this.estados = estados;
		this.versao = versao;
	}

	/**
	 * Cria o arcabouço padrão do xml para requisição dos Status do serviço
	 * 
	 * @return SoapObject necessario para montar o xml.
	 * @throws VersaoNFCeErradaException
	 *             Será lançado quando houver discrepâncias nas informações das
	 *             versões das notas
	 */
	@Override
	public SoapObject criarSoapObject() throws VersaoNFCeErradaException {
		SoapObject consStatServ = new SoapObject(null, "consStatServ");
		consStatServ.addAttribute(null, "xmlns", NAMESPACE);
		consStatServ.addAttribute("versao", versao.getVersao());

		// tpAmb Element
		consStatServ.addProperty("tpAmb", String.valueOf(this.tpAmb.getAmbiente()));

		// cUF Element
		consStatServ.addProperty("cUF", String.valueOf(estados.getCodigoIbge()));

		// xServ Element
		consStatServ.addProperty("xServ", "STATUS");
		
		return consStatServ;
	}

	@Override
	public Estados getUF() {
		return estados;
	}

	@Override
	public Versao getVersao() {
		return versao;
	}

	@Override
	public Servico getServico() {
		return Servico.STATUS_SERVICO;
	}

	@Override
	public TpAmb getTpAmb() {
		return tpAmb;
	}

	@Override
	public TpEmis getTpEmis() {
		return TpEmis.NORMAL;
	}

	@Override
	public ModeloNF getModeloNF() {
		return ModeloNF.NFCe;
	}

}
