package brunonm.nfce.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Calendar;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.HttpsServiceConnectionSE;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.KeepAliveHttpsTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import brunonm.nfce.enums.Servico;
import brunonm.nfce.exceptions.CertificadoInvalidoException;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;
import brunonm.nfce.exceptions.WebServiceInexistenteException;
import brunonm.nfce.modelo.InfoCert;

/**
 * Classe responsável por unir todos os elementos necessários para consumir os
 * serviços disponíveis pelo Sefaz. Dentre os serviços disponíveis tem:
 * NFCeAutorização; NFeRetAutorização; NfeStatusServico; NfeConsultaProtocolo;
 * NfeInutilização; NfeConsultaCadastro; RecepçãoEvento (Cancelamento)
 * 
 * @author Bruno Melo
 *
 */
public class EnviarNFCe {

	private static final char SEPARATOR = File.separatorChar;

	private CriadorXml criadorXml;
	private InfoCert infoCert;
	private CriadorEnvelopeSoap criadorEnvelopeSoap;
	private Config config;

	// configurações
	private boolean salvaArquivosDeEnvioEResposta;
	private String arquivosPath;
	private boolean salvarEnvelopeSOAP;
	private String envelopesPath;
	private String pathNFe;
	private boolean separarPastasMensalmente;

	/**
	 * Construtor com as informações necessárias para enviar uma solicitação ao
	 * Sefaz.
	 * 
	 * @param criadorXml
	 *            pode ser:
	 * @see brunonm.nfce.utils.CriadorXmlAutorizacao
	 * @see brunonm.nfce.utils.CriadorXmlStatusServico
	 * @param infoCert
	 *            Informações sobre o certificado necessárias para estabelecer
	 *            uma conexão segura com o Sefaz
	 * @throws CertificadoInvalidoException
	 *             É lançado caso haja algum problema com as informações do
	 *             certificado.
	 */
	public EnviarNFCe(InfoCert infoCert) throws CertificadoInvalidoException {
		this.infoCert = infoCert;

		if (infoCert == null)
			throw new CertificadoInvalidoException("A informação do certificado não podem ser nulas");

		setupSistema();
		config = new Config();
	}
	


	/**
	 * Envia uma requisição para consultar o Status do serviço do Sefaz.
	 * 
	 * @return resposta da consulta do status do serviço
	 * @throws VersaoNFCeErradaException
	 *             É lançado quando existe discrepância nas informações da NFe.
	 * @throws WebServiceInexistenteException
	 *             É lançado quando existe um problema com os webservices.
	 */
	public RespostaConsultaStatus requisicaoConsultaStatusServico(CriadorXmlStatusServico criadorXmlStatusServico)
			throws VersaoNFCeErradaException, WebServiceInexistenteException {
		this.criadorXml = criadorXmlStatusServico;
		SoapObject consultaStatusResponse = enviarRequisicao();
		return new RespostaConsultaStatus(consultaStatusResponse);
	}

	private SoapObject enviarRequisicao() throws VersaoNFCeErradaException, WebServiceInexistenteException {
		criadorEnvelopeSoap = new CriadorEnvelopeSoap(criadorXml);
		SoapObject result = null;
		try {

			String url = WebServerHelper.pegarUrlWebServer(criadorXml.getServico(), criadorXml.getUF(),
					criadorXml.getTpEmis(), criadorXml.getTpAmb(), criadorXml.getModeloNF(), criadorXml.getVersao());

			if (url != null) {
				SoapSerializationEnvelope envelope = criadorEnvelopeSoap.createSOAPRequest();

				HttpTransportSE androidHttpTransport = new HttpTransportSE(url);
			
				androidHttpTransport.debug = true;
				
				androidHttpTransport.call(WebServerHelper.pegarSOAPAction(criadorXml.getServico()), envelope);

				result = (SoapObject) envelope.getResponse();

				String requestDump = androidHttpTransport.requestDump;
				String responseDump = androidHttpTransport.responseDump;

				salvarLogsDeEnvio(requestDump);

				Document respostaDocument = salvarLogsDeResposta(responseDump);

				salvarArquivoNFe(requestDump, respostaDocument);

			} else {
				throw new WebServiceInexistenteException(
						"Não existe um Web Service especifico para o serviço solicitado!");
			}

		} catch (IOException | XmlPullParserException | ParserConfigurationException | SAXException  e) {
			e.printStackTrace();
		}
		return result;
	}

	private void salvarArquivoNFe(String requestDump, Document documentResposta) throws VersaoNFCeErradaException {
		try {
			if (criadorXml.getServico() == Servico.AUTORIZACAO) {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db;
				db = dbf.newDocumentBuilder();

				NodeList protNFe = documentResposta.getElementsByTagName("protNFe");
				for (int i = 0; i < protNFe.getLength(); i++) {
					Node currentNode = protNFe.item(i);
					if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
						int cStat = Integer.parseInt(
								((Element) currentNode).getElementsByTagName("cStat").item(0).getTextContent());
						if (cStat == 100 || cStat == 150 || cStat == 110) {
							Document NFeDocument = criarNFeDocument(db, currentNode, requestDump);
							String nfePath = criarPaths(pathNFe, true);
							CriadorXmlHelper.criarArquivoNFe(NFeDocument, nfePath, criadorXml.getVersao());
						}
					}
				}
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
	}

	private Document criarNFeDocument(DocumentBuilder db, Node currentNode, String requestDump)
			throws VersaoNFCeErradaException, SAXException, IOException {
		Document doc = db.newDocument();
		doc.setXmlStandalone(true);

		// nfeProc Element
		Element nfeProc = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "nfeProc");
		nfeProc.setAttribute("versao", criadorXml.getVersao().getVersao());
		doc.appendChild(nfeProc);

		Document criarXmlDocument = db.parse(new InputSource(new ByteArrayInputStream(requestDump.getBytes("utf-8"))));
		Node NFeNode = criarXmlDocument.getElementsByTagName("NFe").item(0);

		nfeProc.appendChild(doc.adoptNode(NFeNode.cloneNode(true)));
		nfeProc.appendChild(doc.adoptNode(currentNode.cloneNode(true)));

		return doc;
	}

	private void salvarLogsDeEnvio(String soapRequest)
			throws IOException, VersaoNFCeErradaException, SAXException, ParserConfigurationException {
		if (salvaArquivosDeEnvioEResposta || salvarEnvelopeSOAP) {
			String path = criarPaths(arquivosPath, false);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document document = db.parse(new InputSource(new ByteArrayInputStream(soapRequest.getBytes("utf-8"))));

			if (salvaArquivosDeEnvioEResposta) {

				Node node = document.getElementsByTagName("nfeDadosMsg").item(0);

				CriadorXmlHelper.criarArquivoXml(path, node, criadorXml.getServico(), criadorXml.getVersao(), true);
			}
			if (salvarEnvelopeSOAP) {
				CriadorXmlHelper.criarArquivoXml(document, path, criadorXml.getServico(), true);
			}
		}
	}

	private Document salvarLogsDeResposta(String soapResponse)
			throws IOException, VersaoNFCeErradaException, ParserConfigurationException, SAXException {
		String path = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(new InputSource(new ByteArrayInputStream(soapResponse.getBytes("utf-8"))));
		Document bodyDoc = extrairRespostaDocument(soapResponse);

		if (salvaArquivosDeEnvioEResposta || salvarEnvelopeSOAP) {
			path = criarPaths(envelopesPath, false);

			if (salvaArquivosDeEnvioEResposta) {

				Node node = document.getElementsByTagName("soap:Body").item(0).getChildNodes().item(0);

				CriadorXmlHelper.criarArquivoXml(path, node, criadorXml.getServico(), criadorXml.getVersao(), false);
			}
			if (salvarEnvelopeSOAP) {
				CriadorXmlHelper.criarArquivoXml(document, path, criadorXml.getServico(), false);
			}
		}
		return bodyDoc;
	}

	private Document extrairRespostaDocument(String soapResponse)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document bodyDoc = db.parse(new InputSource(new ByteArrayInputStream(soapResponse.getBytes("utf-8"))));
		Document respostaDoc = db.newDocument();
		respostaDoc.setXmlStandalone(true);

		NodeList respostaNode = bodyDoc.getChildNodes().item(0).getChildNodes();
		respostaDoc.appendChild(respostaDoc.adoptNode(respostaNode.item(0).cloneNode(true)));

		return respostaDoc;
	}

	private String criarPaths(String basePath, boolean isNFe) throws IOException {
		String path = basePath;
		if (path.length() > 0)
			if (path.charAt(path.length() - 1) != SEPARATOR)
				path += SEPARATOR;
		if (isNFe) {
			if (separarPastasMensalmente) {
				Calendar now = Calendar.getInstance();
				path += String.valueOf(now.get(Calendar.YEAR)) + SEPARATOR;
				path += String.valueOf(now.get(Calendar.MONTH) + 1) + SEPARATOR;
			}
		}
		File pathToFile = new File(path);
		if (!pathToFile.exists()) {
			pathToFile.mkdir();
		}
		return path;
	}

	public Config configuracao() {
		return this.config;
	}

	private void salvar(Config config) {
		this.salvaArquivosDeEnvioEResposta = config.salvaArquivosDeEnvioEResposta;
		this.arquivosPath = config.arquivosPath;
		this.salvarEnvelopeSOAP = config.salvarEnvelopeSOAP;
		this.envelopesPath = config.envelopesPath;
		this.pathNFe = config.pathNFe;
		this.separarPastasMensalmente = config.separarPastasMensalmente;
	}

	public class Config {

		private boolean salvaArquivosDeEnvioEResposta = false;
		private String arquivosPath = "Logs";
		private boolean salvarEnvelopeSOAP = false;
		private String envelopesPath = "";
		private String pathNFe = "NFe";
		private boolean separarPastasMensalmente = true;

		public Config() {
			salvarConfiguracoes();
		}

		public Config salvaArquivosDeEnvioEResposta(boolean salvaArquivosDeEnvioEResposta, String arquivosPath) {
			this.salvaArquivosDeEnvioEResposta = salvaArquivosDeEnvioEResposta;
			this.arquivosPath = arquivosPath;
			return this;
		}

		public Config salvarEnvelopeSOAP(boolean salvarEnvelopeSOAP, String envelopesPath) {
			this.salvarEnvelopeSOAP = salvarEnvelopeSOAP;
			this.envelopesPath = envelopesPath;
			return this;
		}

		public Config pastaArquivosNFe(String pathNFe) {
			this.pathNFe = pathNFe;
			return this;
		}

		public Config separarPastasMensalmente(boolean separarPastasMensalmente) {
			this.separarPastasMensalmente = separarPastasMensalmente;
			return this;
		}

		public void salvarConfiguracoes() {
			salvar(this);
		}

	}

	/**
	 * Configura as informações de sistema e do certificado.
	 */
	private void setupSistema() {
		System.clearProperty("javax.net.ssl.keyStore");
		System.clearProperty("javax.net.ssl.keyStorePassword");
		System.clearProperty("javax.net.ssl.trustStore");
		System.clearProperty("javax.net.ssl.trustStorePassword");
		System.clearProperty("https.protocols");

		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
		System.setProperty("javax.net.ssl.keyStore", this.infoCert.getCaminhoCert());
		System.setProperty("javax.net.ssl.keyStorePassword", this.infoCert.getSenhaCert());

		System.setProperty("javax.net.ssl.trustStoreType", "JKS");
		System.setProperty("javax.net.ssl.trustStore", "src/main/resources/cacerts/NFeCacertsProd");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

		System.setProperty("https.protocols", "TLSv1.2");
	}

}
