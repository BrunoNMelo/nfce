package brunonm.nfce.utils;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;

class CriadorXmlHelper {

	/**
	 * Cria um arquivo xml e salva no local especificada pelo usuário.
	 * 
	 * @param path
	 *            Local onde sera salvo o xml
	 * @param soapRequest
	 *            Document padão para criação do arquivo xml.
	 * @param servico
	 *            Tipo de serviço;
	 * @param isPedido
	 *            Se é uma solicitação ao Sefaz.
	 * @param isSoap
	 *            Se é um arquivo Soap.
	 * @return Caminho completo do aquivo salvo.
	 */
	protected static String criarArquivoXml(String path, Node node, Servico servico, Versao versao,
			boolean isPedido) {
		String fileName = null;
		try {
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			DOMSource source = new DOMSource(node);

			fileName = criaNomeArquivoXml(servico, isPedido, false);

			StreamResult result = new StreamResult(new File(path + fileName));
			transformer.transform(source, result);

//			verificacaoDoSchema(path + fileName, versao, servico, isPedido);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return path + fileName;
	}
	
	public static String criarArquivoXml(Document document, String path, Servico servico, boolean isPedido) {
		String fileName = null;
		try {
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			DOMSource source = new DOMSource(document);

			fileName = criaNomeArquivoXml(servico, isPedido, true);

			StreamResult result = new StreamResult(new File(path + fileName));
			transformer.transform(source, result);

		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return path + fileName;
	}

	protected static void criarArquivoNFe(Document nFeDocument, String nfePath, Versao versao) {
		try {
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(nFeDocument);

			String fileName = criaNomeArquivoNFe(nFeDocument);

			StreamResult result = new StreamResult(new File(nfePath + fileName));
			transformer.transform(source, result);

//			verificacaoDoSchemaNFe(nfePath + fileName, versao);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private static String criaNomeArquivoNFe(Document nFeDocument) {
		String id = nFeDocument.getElementsByTagName("chNFe").item(0).getTextContent();
		return id + "-nfe.xml";
	}

	/**
	 * Cria uma versão XML da NFCe só que em formato String.
	 * 
	 * @param doc
	 *            Document base para criar a String
	 * @return retorna o String da NFCe.
	 * @throws VersaoNFCeErradaException
	 */
	protected static String criarStringXml(Document doc) throws VersaoNFCeErradaException {
		String xmlString = null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();

			transformer.transform(new DOMSource(doc), new StreamResult(writer));

			xmlString = writer.getBuffer().toString();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return xmlString;
	}

	/**
	 * Gera o nome do arquivo a ser salvo
	 * 
	 * @param servico
	 *            Tipo de serviço;
	 * @param isPedido
	 *            Se é uma solicitação ao Sefaz.
	 * @param isSoap
	 *            Se é um arquivo Soap.
	 * @return nome do arquivo a ser salvo.
	 */
	private static String criaNomeArquivoXml(Servico servico, boolean isPedido, boolean isSoap) {
		StringBuilder fileName = new StringBuilder();
		SimpleDateFormat sd = new SimpleDateFormat("YYYYMMddHHmmss");
		fileName.append(sd.format(new Date()));

		if (isPedido)
			fileName.append("-ped");

		switch (servico) {
		case AUTORIZACAO:
			fileName.append("-lot");
			break;

		case CONSULTA_CADASTRO:
			fileName.append("-cad");
			break;

		case INUTILIZACAO:
			fileName.append("-inu");
			break;

		case RET_AUTORIZACAO:
			fileName.append("-rec");
			break;

		case STATUS_SERVICO:
			fileName.append("-sta");
			break;

		case CONSULTA_PROTOCOLO:
			fileName.append("-sit");
			break;

		case EVENTO:
			fileName.append("-eve");
			break;
		case DOWNLOAD_NFE:
			fileName.append("-dln");
			break;
		}

		if (isSoap)
			fileName.append("-soap");

		fileName.append(".xml");

		return fileName.toString();
	}

	/**
	 * passa o xml criado para verificação do schema
	 * 
	 * @param servico
	 * @param isPedido
	 * @param retEnvi
	 * 
	 * @param fileName
	 *            caminho com o arquivo xml a ser assinado
	 */
	private static void verificacaoDoSchema(String file, Versao versao, Servico servico, boolean isPedido) {
		String schemaPath = null;
		if (versao == Versao.V310) {
			if (!isPedido) {
				switch (servico) {
				case AUTORIZACAO:
					schemaPath = "src/main/resources/PL_008i2/retEnviNFe_v3.10.xsd";
					break;
				case CONSULTA_CADASTRO:
					schemaPath = "src/main/resources/PL_006j/retConsCad_v2.00.xsd";
					break;
				case CONSULTA_PROTOCOLO:
					schemaPath = "src/main/resources/PL_008i2/retConsSitNFe_v3.10.xsd";
					break;
				case DOWNLOAD_NFE:
					schemaPath = "src/main/resources/PL_008i2/leiauteNFe_v3.10.xsd";
					break;
				case EVENTO:
					schemaPath = "src/main/resources/Evento_Canc/retEnvEventoCancNFe_v1.00.xsd";
					break;
				case INUTILIZACAO:
					schemaPath = "src/main/resources/PL_008i2/retInutNFe_v3.10.xsd";
					break;
				case RET_AUTORIZACAO:
					schemaPath = "src/main/resources/PL_008i2/retConsReciNFe_v3.10.xsd";
					break;
				case STATUS_SERVICO:
					schemaPath = "src/main/resources/PL_008i2/retConsStatServ_v3.10.xsd";
					break;
				}
			} else {
				switch (servico) {
				case AUTORIZACAO:
					schemaPath = "src/main/resources/PL_008i2/enviNFe_v3.10.xsd";
					break;

				case CONSULTA_PROTOCOLO:
					schemaPath = "src/main/resources/PL_008i2/consSitNFe_v3.10.xsd";
					break;

				case CONSULTA_CADASTRO:
					schemaPath = "src/main/resources/PL_006j/consCad_v2.00.xsd";
					break;

				case EVENTO:
					schemaPath = "src/main/resources/Evento_Canc/envEventoCancNFe_v1.00.xsd";
					break;

				case INUTILIZACAO:
					schemaPath = "src/main/resources/PL_008i2/inutNFe_v3.10.xsd";
					break;

				case RET_AUTORIZACAO:
					schemaPath = "src/main/resources/PL_008i2/consReciNFe_v3.10.xsd";
					break;

				case STATUS_SERVICO:
					schemaPath = "src/main/resources/PL_008i2/consStatServ_v3.10.xsd";
					break;
				case DOWNLOAD_NFE:
					schemaPath = "src/main/resources/PL_008i2/leiauteNFe_v3.10.xsd";
					break;
				}
			}
		} else if (versao == Versao.V400) {
			if (!isPedido) {
				switch (servico) {
				case AUTORIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/retEnviNFe_v4.00.xsd";
					break;
				case CONSULTA_CADASTRO:
					schemaPath = "src/main/resources/PL_006j/retConsCad_v2.00.xsd";
					break;
				case CONSULTA_PROTOCOLO:
					schemaPath = "src/main/resources/PL_009_V4/retConsSitNFe_v4.00.xsd";
					break;
				case DOWNLOAD_NFE:
					schemaPath = "src/main/resources/PL_009_V4/leiauteNFe_v4.00.xsd";
					break;
				case EVENTO:
					schemaPath = "src/main/resources/Evento_Canc/retEnvEventoCancNFe_v1.00.xsd";
					break;
				case INUTILIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/retInutNFe_v4.00.xsd";
					break;
				case RET_AUTORIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/retConsReciNFe_v4.00.xsd";
					break;
				case STATUS_SERVICO:
					schemaPath = "src/main/resources/PL_009_V4/retConsStatServ_v4.00.xsd";
					break;
				}
			} else {
				switch (servico) {
				case AUTORIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/enviNFe_v4.00.xsd";
					break;

				case CONSULTA_PROTOCOLO:
					schemaPath = "src/main/resources/PL_009_V4/consSitNFe_v4.00.xsd";
					break;

				case CONSULTA_CADASTRO:
					schemaPath = "src/main/resources/PL_006j/consCad_v2.00.xsd";
					break;

				case EVENTO:
					schemaPath = "src/main/resources/Evento_Canc/envEventoCancNFe_v1.00.xsd";
					break;

				case INUTILIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/inutNFe_v4.00.xsd";
					break;

				case RET_AUTORIZACAO:
					schemaPath = "src/main/resources/PL_009_V4/consReciNFe_v4.00.xsd";
					break;

				case STATUS_SERVICO:
					schemaPath = "src/main/resources/PL_009_V4/consStatServ_v4.00.xsd";
					break;
				case DOWNLOAD_NFE:
					schemaPath = "src/main/resources/PL_009_V4/leiauteNFe_v4.00.xsd";
					break;
				}
			}
		}
		verificarSchema(file, schemaPath);
	}

	private static void verificacaoDoSchemaNFe(String xmlPath, Versao versao) {
		String schemaPath = null;
		switch (versao) {
		case V310:
			schemaPath = "src/main/resources/PL_008i2/procNFe_v3.10.xsd";
			break;
		case V400:
			schemaPath = "src/main/resources/PL_009_V4/procNFe_v4.00.xsd";
			break;
		}

		verificarSchema(xmlPath, schemaPath);
	}

	private static void verificarSchema(String file, String schemaPath) {
		File schemaFile = new File(schemaPath);
		Source xmlFile = new StreamSource(new File(file));
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			validator.validate(xmlFile);
			System.out.println("xml is valid");
		} catch (SAXException | IOException e) {
			System.out.println("xml is NOT valid reason:" + e);
		}
	}

}
