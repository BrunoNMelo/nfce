package brunonm.nfce.utils;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.ModeloNF;
import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.Versao;

/**
 * Classe auxiliar que retorna as url necessárias para montagem do Envelope SOAP
 * e para acessar os WebServices
 * 
 * @author Bruno Melo
 *
 */
class WebServerHelper {

	/**
	 * Pega o link especifico do web services para consumir o serviço do Sefaz.
	 * @param servico Qual serviço será utilizado
	 * @param uF Estado que está será emitido a Nota
	 * @param tpEmis Tipo de emissão, se é contingência ou normal. 
	 * @param tpAmb Tipo de ambiente, se é produção ou homologação
	 * @param modeloNF qual o modelo da NFe.
	 * @param versao versão da NFe
	 * @return Url especifica para consumir o serviço especifico do WebService.
	 */
	protected static String pegarUrlWebServer(Servico servico, Estados uF, TpEmis tpEmis, TpAmb tpAmb, ModeloNF modeloNF,
			Versao versao) {
		String url = null;
		switch (modeloNF) {
		case NFCe:
			switch (tpAmb) {
			case HOMOLOGACAO:
				url = pegarWebServiceNFCeHomologacao(uF, tpEmis, servico, modeloNF);
				break;
			case PRODUCAO:
				url = pegarWebServiceNFCeProducao(uF, tpEmis, servico, modeloNF);
				break;
			}
			break;
		case NFe:
			switch (tpAmb) {
			case HOMOLOGACAO:
				url = pegarWebServiceNFeHomologacao(uF, tpEmis, servico, modeloNF);
				break;
			case PRODUCAO:
				url = pegarWebServiceNFeProducao(uF, tpEmis, servico, modeloNF);
				break;
			}
			break;
		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFCe de produção
	 * 
	 * @param uF
	 *            Estado que será emitido a NFe
	 * @param tpEmis
	 *            necessário para definir se a nota sera emitido em contigencia
	 *            ou não.
	 * @param servico
	 *            Qual serviço será consumido.
	 * @param modeloNF
	 *            Qual o modelo na NFe.
	 * @return url com o webservice do para NFCe de produção
	 */
	private static String pegarWebServiceNFCeProducao(Estados uF, TpEmis tpEmis, Servico servico, ModeloNF modeloNF) {
		String url = null;
		if (tpEmis == TpEmis.NORMAL) {
			switch (uF) {
			case PB:
			case AC:
			case AL:
			case BA:
			case CE:
			case DF:
			case ES:
			case MA:
			case PA:
			case PI:
			case RJ:
			case RN:
			case RO:
			case SC:
			case SE:
			case AP:
			case RR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://cad.svrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case EVENTO:
					url = "https://nfce.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfce.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case AM:
				switch (servico) {
				case AUTORIZACAO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "nfce.sefaz.am.gov.br/nfce-services/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MG:
			case TO:
				switch (servico) {
				default:
					url = null;
					break;
				}
				break;

			case MS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.fazenda.ms.gov.br/producao/services2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MT:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/NfeAutorizacao";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.sefaz.mt.gov.br/nfcews/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.sefaz.pe.gov.br/nfce-ws/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeAutorizacao3";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeConsulta3";
					break;
				case EVENTO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeRecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeInutilizacao3";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeRetAutorizacao3";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.fazenda.pr.gov.br/nfce/NFeStatusServico3";
					break;
				default:
					url = null;
					break;
				}
				break;

			case RS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case EVENTO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case SP:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce.fazenda.sp.gov.br/ws/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce.fazenda.sp.gov.br/ws/NFeConsulta2.asmx";
					break;
				case EVENTO:
					url = "https://nfce.fazenda.sp.gov.br/ws/RecepcaoEvento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfce.fazenda.sp.gov.br/ws/NFeInutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce.fazenda.sp.gov.br/ws/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfce.fazenda.sp.gov.br/ws/NFeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case GO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;
			}
		} else {
			url = pegarUrlContigenciaProducao(uF, servico, modeloNF);
		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFCe de homologação
	 * 
	 * @param uF
	 *            Estado que será emitido a NFe
	 * @param tpEmis
	 *            necessário para definir se a nota sera emitido em contigencia
	 *            ou não.
	 * @param servico
	 *            Qual serviço será consumido.
	 * @param modeloNF
	 *            Qual o modelo na NFe.
	 * @return url com o webservice do para NFCe de homologação
	 */
	private static String pegarWebServiceNFCeHomologacao(Estados uF, TpEmis tpEmis, Servico servico,
			ModeloNF modeloNF) {
		String url = null;
		if (tpEmis == TpEmis.NORMAL) {
			switch (uF) {
			case PB:
			case AC:
			case AL:
			case BA:
			case DF:
			case ES:
			case MA:
			case PA:
			case PI:
			case RJ:
			case RN:
			case RO:
			case SC:
			case SE:
			case AP:
			case RR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case EVENTO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;
			case AM:
				switch (servico) {
				case AUTORIZACAO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "homnfce.sefaz.am.gov.br/nfce-services/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MG:
			case TO:
				switch (servico) {
				default:
					url = null;
					break;
				}
				break;

			case MS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfce.fazenda.ms.gov.br/homologacao/services2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MT:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.sefaz.mt.gov.br/nfcews/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "	https://homologacao.sefaz.mt.gov.br/nfcews/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://homologacao.sefaz.mt.gov.br/nfcews/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.sefaz.mt.gov.br/nfcews/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.sefaz.mt.gov.br/nfcews/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.sefaz.mt.gov.br/nfcews/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfcehomolog.sefaz.pe.gov.br/nfce-ws/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeAutorizacao3";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeConsulta3";
					break;
				case EVENTO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeRecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeInutilizacao3";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeRetAutorizacao3";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfce.fazenda.pr.gov.br/nfce/NFeStatusServico3";
					break;
				default:
					url = null;
					break;
				}
				break;

			case RS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case EVENTO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfce-homologacao.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case SP:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/nfeautorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/nfeconsulta2.asmx";
					break;
				case EVENTO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/nferetautorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfce.fazenda.sp.gov.br/ws/nfestatusservico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case CE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfceh.sefaz.ce.gov.br/nfce/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case GO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;
			}
		} else {
			url = pegarUrlContigenciaHomologacao(uF, servico, modeloNF);
		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFe de homologação
	 * 
	 * @param uF
	 *            Estado que será emitido a NFe
	 * @param tpEmis
	 *            necessário para definir se a nota sera emitido em contigencia
	 *            ou não.
	 * @param servico
	 *            Qual serviço será consumido.
	 * @param modeloNF
	 *            Qual o modelo na NFe.
	 * @return url com o webservice do para NFE de homologação
	 */
	private static String pegarWebServiceNFeHomologacao(Estados uF, TpEmis tpEmis, Servico servico, ModeloNF modeloNF) {
		String url = null;
		if (tpEmis == TpEmis.NORMAL) {
			switch (uF) {
			case MA:
			case PA:
			case PI:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					if (uF == Estados.MA) {
						url = "https://sistemas.sefaz.ma.gov.br/wscadastro/CadConsultaCadastro2";
					} else {
						url = null;
					}
					break;
				case EVENTO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx";
					break;
				case DOWNLOAD_NFE:
					url = "https://hom.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx";
					break;
				}
				break;

			case AC:
			case AL:
			case AP:
			case DF:
			case ES:
			case PB:
			case RJ:
			case RN:
			case RO:
			case RR:
			case SC:
			case SE:
			case TO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					switch (uF) {
					case AC:
					case RN:
					case PB:
					case SC:
						url = "https://cad.svrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx";
						break;
					default:
						url = null;
						break;
					}
					break;
				case EVENTO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case AM:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2";
					break;
				case EVENTO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://homnfe.sefaz.am.gov.br/services2/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case BA:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/NfeConsulta/NfeConsulta.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx";
					break;
				case EVENTO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/NfeInutilizacao/NfeInutilizacao.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://hnfe.sefaz.ba.gov.br/webservices/NfeStatusServico/NfeStatusServico.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case CE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2";
					break;
				case DOWNLOAD_NFE:
					url = "https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF";
					break;
				}
				break;

			case GO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "	https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MG:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/cadconsultacadastro2";
					break;
				case EVENTO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MT:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homologacao.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeConsulta3";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeInutilizacao3";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeStatusServico3";
					break;
				default:
					url = null;
					break;
				}
				break;

			case RS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://cad.sefazrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx";
					break;
				case EVENTO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe-homologacao.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case SP:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeconsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/cadconsultacadastro2.asmx";
					break;
				case EVENTO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://homologacao.nfe.fazenda.sp.gov.br/ws/nfestatusservico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;
			}

		} else {
			url = pegarUrlContigenciaHomologacao(uF, servico, modeloNF);
		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFe de produção
	 * 
	 * @param uF
	 *            Estado que será emitido a NFe
	 * @param tpEmis
	 *            necessário para definir se a nota sera emitido em contigencia
	 *            ou não.
	 * @param servico
	 *            Qual serviço será consumido.
	 * @param modeloNF
	 *            Qual o modelo na NFe.
	 * @return url com o webservice do para NFE de produção
	 */
	private static String pegarWebServiceNFeProducao(Estados uF, TpEmis tpEmis, Servico servico, ModeloNF modeloNF) {
		String url = null;
		if (tpEmis == TpEmis.NORMAL) {
			switch (uF) {
			case MA:
			case PA:
			case PI:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://www.sefazvirtual.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					if (uF == Estados.MA) {
						url = "https://sistemas.sefaz.ma.gov.br/wscadastro/CadConsultaCadastro2";
					} else {
						url = null;
					}
					break;
				case EVENTO:
					url = "https://www.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://www.sefazvirtual.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://www.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://www.sefazvirtual.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx";
					break;
				case DOWNLOAD_NFE:
					url = "	https://www.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx";
					break;
				}
				break;

			case AC:
			case AL:
			case AP:
			case DF:
			case ES:
			case PB:
			case RJ:
			case RN:
			case RO:
			case RR:
			case SC:
			case SE:
			case TO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					switch (uF) {
					case AC:
					case RN:
					case PB:
					case SC:
						url = "https://cad.svrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx";
						break;
					default:
						url = null;
						break;
					}
					break;
				case EVENTO:
					url = "https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case AM:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.am.gov.br/services2/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case BA:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/NfeConsulta/NfeConsulta.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/NfeInutilizacao/NfeInutilizacao.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.ba.gov.br/webservices/NfeStatusServico/NfeStatusServico.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case CE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2";
					break;
				case DOWNLOAD_NFE:
					url = "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF";
					break;
				}
				break;

			case GO:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "	https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MG:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/cadconsultacadastro2";
					break;
				case EVENTO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.fazenda.mg.gov.br/nfe2/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/NfeConsulta2";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.fazenda.ms.gov.br/producao/services2/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case MT:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao2";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PE:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2";
					break;
				case EVENTO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2";
					break;
				default:
					url = null;
					break;
				}
				break;

			case PR:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeConsulta3";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/CadConsultaCadastro2";
					break;
				case EVENTO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento";
					break;
				case INUTILIZACAO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeInutilizacao3";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.fazenda.pr.gov.br/nfe/NFeStatusServico3";
					break;
				default:
					url = null;
					break;
				}
				break;

			case RS:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://cad.sefazrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx";
					break;
				case EVENTO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;

			case SP:
				switch (servico) {
				case AUTORIZACAO:
					url = "https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx";
					break;
				case CONSULTA_PROTOCOLO:
					url = "https://nfe.fazenda.sp.gov.br/ws/nfeconsulta2.asmx";
					break;
				case CONSULTA_CADASTRO:
					url = "https://nfe.fazenda.sp.gov.br/ws/cadconsultacadastro2.asmx";
					break;
				case EVENTO:
					url = "https://nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx";
					break;
				case INUTILIZACAO:
					url = "https://nfe.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx";
					break;
				case RET_AUTORIZACAO:
					url = "https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx";
					break;
				case STATUS_SERVICO:
					url = "https://nfe.fazenda.sp.gov.br/ws/nfestatusservico2.asmx";
					break;
				default:
					url = null;
					break;
				}
				break;
			}

		} else {
			url = pegarUrlContigenciaProducao(uF, servico, modeloNF);
		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFe de homologação em contingência
	 * @param uF
	 *            Informações do Estado
	 * @param servico
	 *            tipo de serviço
	 * @param modeloNF
	 * @return Url para webservice de contigencia para homologação
	 */
	private static String pegarUrlContigenciaHomologacao(Estados uF, Servico servico, ModeloNF modeloNF) {
		String url = null;
		switch (uF) {
		case AC:
		case AL:
		case AP:
		case DF:
		case ES:
		case MG:
		case PB:
		case RJ:
		case RN:
		case RO:
		case RR:
		case RS:
		case SC:
		case SE:
		case SP:
		case TO:
			switch (servico) {
			case AUTORIZACAO:
				url = "https://hom.svc.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx";
				break;
			case CONSULTA_PROTOCOLO:
				url = "https://hom.svc.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx";
				break;
			case EVENTO:
				url = "https://hom.svc.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx";
				break;
			case RET_AUTORIZACAO:
				url = "https://hom.svc.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
				break;
			case STATUS_SERVICO:
				url = "https://hom.svc.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx";
				break;
			default:
				url = null;
				break;
			}
			break;

		case AM:
		case BA:
		case CE:
		case GO:
		case MA:
		case MS:
		case MT:
		case PA:
		case PE:
		case PI:
		case PR:
			switch (servico) {
			case AUTORIZACAO:
				if (modeloNF == ModeloNF.NFCe)
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
				else
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
				break;
			case CONSULTA_PROTOCOLO:
				if (modeloNF == ModeloNF.NFCe)
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
				else
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
				break;
			case EVENTO:
				if (modeloNF == ModeloNF.NFCe)
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
				else
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
				break;
			case RET_AUTORIZACAO:
				if (modeloNF == ModeloNF.NFCe)
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
				else
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
				break;
			case STATUS_SERVICO:
				if (modeloNF == ModeloNF.NFCe)
					url = "https://nfe-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
				else
					url = "https://nfce-homologacao.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
				break;
			default:
				url = null;
				break;
			}
			break;

		}
		return url;
	}

	/**
	 * Pega o link dos web services para NFe de produção em contingência
	 * @param uF
	 *            Informações do Estado
	 * @param servico
	 *            tipo de serviço
	 * @param modeloNF
	 * @return Url para webservice de contigencia para producao
	 */
	private static String pegarUrlContigenciaProducao(Estados uF, Servico servico, ModeloNF modeloNF) {
		String url = null;
		switch (uF) {
		case AC:
		case AL:
		case AP:
		case DF:
		case ES:
		case MG:
		case PB:
		case RJ:
		case RN:
		case RO:
		case RR:
		case RS:
		case SC:
		case SE:
		case SP:
		case TO:
			switch (servico) {
			case AUTORIZACAO:
				url = "https://www.svc.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx";
				break;
			case CONSULTA_PROTOCOLO:
				url = "https://www.svc.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx";
				break;
			case EVENTO:
				url = "https://www.svc.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx";
				break;
			case RET_AUTORIZACAO:
				url = "https://www.svc.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx";
				break;
			case STATUS_SERVICO:
				url = "https://www.svc.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx";
				break;
			default:
				url = null;
				break;
			}
			break;

		case AM:
		case BA:
		case CE:
		case GO:
		case MA:
		case MS:
		case MT:
		case PA:
		case PE:
		case PI:
		case PR:
			switch (servico) {
			case AUTORIZACAO:
				if (modeloNF == ModeloNF.NFe)
					url = "https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
				else
					url = "https://nfce.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
				break;
			case CONSULTA_PROTOCOLO:
				if (modeloNF == ModeloNF.NFe)
					url = "https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
				else
					url = "https://nfce.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx";
				break;
			case EVENTO:
				if (modeloNF == ModeloNF.NFe)
					url = "https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
				else
					url = "https://nfce.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
				break;
			case RET_AUTORIZACAO:
				if (modeloNF == ModeloNF.NFe)
					url = "https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
				else
					url = "https://nfce.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx";
				break;
			case STATUS_SERVICO:
				if (modeloNF == ModeloNF.NFe)
					url = "https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
				else
					url = "https://nfce.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx";
				break;
			default:
				url = null;
				break;
			}
			break;

		}
		return url;
	}

	/**
	 * Retornar o namespace que será utilizado para montar o envelope SOAP.
	 * 
	 * @param servico
	 *            Serviço que será consumido pelo servidor.
	 * @return Retorna url do xmlns que será usado para construir o envelope
	 *         SOAP
	 */
	protected static String pegarXmlns(Servico servico) {
		String xmlns = null;
		switch (servico) {
		case CONSULTA_PROTOCOLO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeConsulta2";
			break;

		case AUTORIZACAO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeAutorizacao";
			break;

		case STATUS_SERVICO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico2";
			break;

		case INUTILIZACAO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2";
			break;

		case RET_AUTORIZACAO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetAutorizacao";
			break;

		case CONSULTA_CADASTRO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/CadConsultaCadastro2";
			break;

		case EVENTO:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento";
			break;

		case DOWNLOAD_NFE:
			xmlns = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF";
			break;
		}
		return xmlns;
	}
	
	/**
	 * Retornar o SOAPAction que será utilizado para montar o envelope SOAP.
	 * 
	 * @param servico
	 *            Serviço que será consumido pelo servidor.
	 * @return Retorna SOAPAction que será usado para construir o envelope
	 *         SOAP
	 */
	protected static String pegarSOAPAction(Servico servico) {
		String SOAPAction = null;
		switch (servico) {
		case CONSULTA_PROTOCOLO:
			SOAPAction = pegarXmlns(servico) + "/nfeConsultaNF2";
			break;

		case AUTORIZACAO:
			SOAPAction = pegarXmlns(servico) + "/nfeAutorizacaoLote";
			break;

		case STATUS_SERVICO:
			SOAPAction = pegarXmlns(servico) + "/nfeStatusServicoNF2";
			break;

		case INUTILIZACAO:
			SOAPAction = pegarXmlns(servico) + "/nfeInutilizacaoNF2";
			break;

		case RET_AUTORIZACAO:
			SOAPAction = pegarXmlns(servico) + "/nfeRetAutorizacaoLote";
			break;

		case CONSULTA_CADASTRO:
			SOAPAction = pegarXmlns(servico) + "/consultaCadastro2";
			break;

		case EVENTO:
			SOAPAction = pegarXmlns(servico) + "/nfeRecepcaoEvento";
			break;

		case DOWNLOAD_NFE:
			SOAPAction = pegarXmlns(servico) + "/nfeDownloadNF";
			break;
		}
		return SOAPAction;
	}
	
	/**
	 * Retornar o SOAPAction que será utilizado para montar o envelope SOAP.
	 * 
	 * @param servico
	 *            Serviço que será consumido pelo servidor.
	 * @return Retorna SOAPAction que será usado para construir o envelope
	 *         SOAP
	 */
	protected static String pegarSOAPMethodName(Servico servico) {
		String methodName = null;
		switch (servico) {
		case CONSULTA_PROTOCOLO:
			methodName = "nfeConsultaNF2";
			break;

		case AUTORIZACAO:
			methodName = "nfeAutorizacaoLote";
			break;

		case STATUS_SERVICO:
			methodName = "nfeStatusServicoNF2";
			break;

		case INUTILIZACAO:
			methodName = "nfeInutilizacaoNF2";
			break;

		case RET_AUTORIZACAO:
			methodName = "nfeRetAutorizacaoLote";
			break;

		case CONSULTA_CADASTRO:
			methodName = "consultaCadastro2";
			break;

		case EVENTO:
			methodName = "nfeRecepcaoEvento";
			break;

		case DOWNLOAD_NFE:
			methodName = "nfeDownloadNF";
			break;
		}
		return methodName;
	}

}