package brunonm.nfce.utils;

import org.ksoap2.serialization.SoapObject;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.ModeloNF;
import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;

public interface CriadorXml {
	
	public SoapObject criarSoapObject() throws VersaoNFCeErradaException;
	TpAmb getTpAmb();
	TpEmis getTpEmis();
	Versao getVersao();
	Estados getUF();
	Servico getServico();
	ModeloNF getModeloNF();

}
