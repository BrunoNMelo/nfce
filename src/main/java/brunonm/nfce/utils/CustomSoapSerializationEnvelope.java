package brunonm.nfce.utils;

import java.io.IOException;

import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlSerializer;

public class CustomSoapSerializationEnvelope extends SoapSerializationEnvelope {

	protected CustomSoapSerializationEnvelope(int version) {
		super(version);
	}
	
	@Override
	public void write(XmlSerializer writer) throws IOException {
		writer.setPrefix("xsi", xsi);
        writer.setPrefix("xsd", xsd);
        writer.setPrefix("soap12", env);
        writer.startTag(env, "Envelope");
        writer.startTag(env, "Header");
        writeHeader(writer);
        writer.endTag(env, "Header");
        writer.startTag(env, "Body");
        writeBody(writer);
        writer.endTag(env, "Body");
        writer.endTag(env, "Envelope");
	}

}
