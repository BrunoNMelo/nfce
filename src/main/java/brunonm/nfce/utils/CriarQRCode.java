package brunonm.nfce.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.modelo.Dest;
import brunonm.nfce.modelo.Emit;
import brunonm.nfce.modelo.Ide;
import brunonm.nfce.modelo.Total;

/**
 * Classe responsável por criar o QR code para a NFC-e;
 * 
 * @author Bruno Melo
 *
 */
class CriarQRCode {

	private Ide ide;
	private Total total;
	private Document doc;
	private String digVal;
	private Emit emit;
	private Dest dest;

	/**
	 * Construtor com as informações necessárias para a criação do QR code.
	 * 
	 * @param ide
	 *            informações de identificação geral da NFCe.
	 * @param emit
	 *            informações do emitente da NFCe
	 * @param total
	 *            informações com o total da NFCe.
	 * @param dest
	 *            informações do destinatário da NFCe
	 * @param doc
	 *            documento base com as informações para gerar o QRCode.
	 */
	protected CriarQRCode(Ide ide, Emit emit, Total total, Dest dest, Document doc) {
		this.ide = ide;
		this.emit = emit;
		this.total = total;
		this.dest = dest;
		this.doc = doc;
	}

	public Document criarDocumentoComQRCode() {
		try {
			if (pegarUrl() != null) {

				Element infNFeSuplElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "infNFeSupl");

				NodeList signature = doc.getElementsByTagName("Signature");
				NodeList digestValue = doc.getElementsByTagName("DigestValue");

				digVal = String.format("%056x",
						new BigInteger(1, digestValue.item(0).getTextContent().getBytes("UTF-8")));

				// qrCode element
				Element qrCode = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "qrCode");
				qrCode.appendChild(doc.createCDATASection(gerarQRCode()));
				infNFeSuplElement.appendChild(qrCode);

				signature.item(0).getParentNode().insertBefore(infNFeSuplElement, signature.item(0));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return doc;

	}

	/**
	 * Gera a String que deve ser passado no campo QR code da NFC-e.
	 * 
	 * @return String com o QR code.
	 */
	private String gerarQRCode() {
		StringBuilder qrCode = new StringBuilder();
		try {
			String chNFe = ide.getNfID();
			String nVersao = "100";
			String tpAmb = String.valueOf(ide.getTpAmb().getAmbiente());
			String dhEmi = String.format("%050x", new BigInteger(1, ide.getDhEmi().getBytes("UTF-8")));
			String vNF = total.getvNF();
			String vICMS = total.getvICMS();
			String cIdToken = emit.getcIdToken();
			String cSC = emit.getcSC();

			qrCode.append("chNFe=" + chNFe.replace("NFe", ""));
			qrCode.append("&nVersao=" + nVersao);
			qrCode.append("&tpAmb=" + tpAmb);
			if (dest != null) {
				if (dest.getCnpj() != null)
					qrCode.append("&cDest=" + dest.getCnpj());
				
				if (dest.getCpf() != null)
					qrCode.append("&cDest=" + dest.getCpf());
				
				if (dest.getIdEstrangeiro() != null)
					qrCode.append("&cDest=" + dest.getIdEstrangeiro());
			}
			qrCode.append("&dhEmi=" + dhEmi.toUpperCase());
			qrCode.append("&vNF=" + vNF);
			qrCode.append("&vICMS=" + vICMS);
			qrCode.append("&digVal=" + digVal.toUpperCase());
			qrCode.append("&cIdToken=" + cIdToken);

			String hashCode = gerarHashQRCode(qrCode.toString(), cSC);

			qrCode.append("&cHashQRCode=" + hashCode.toUpperCase());

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return (pegarUrl() + qrCode.toString());
	}

	/**
	 * @return URL referente ao servidor de consulta do QR Code.
	 */
	private String pegarUrl() {
		String url = null;

		if (ide.getTpAmb() == TpAmb.PRODUCAO) {
			switch (ide.getcUFNome()) {
			case "Rondônia":
				url = "http://www.nfce.sefin.ro.gov.br/consultanfce/consulta.jsp?";
				break;

			case "Acre":
				url = "www.sefaznet.ac.gov.br/nfce/qrcode?";
				break;

			case "Amazonas":
				url = "sistemas.sefaz.am.gov.br/nfceweb/consultarNFCe.jsp?";
				break;

			case "Roraima":
				url = "https://www.sefaz.rr.gov.br/nfce/servlet/qrcode?";
				break;

			case "Pará":
				url = "https://appnfc.sefa.pa.gov.br/portal/view/consultas/nfce/nfceForm.seam?";
				break;

			case "Amapá":
				url = "https://www.sefaz.ap.gov.br/nfce/nfcep.php?";
				break;

			case "Tocantins":
				url = null;
				break;

			case "Maranhão":
				url = "http://www.nfce.sefaz.ma.gov.br/portal/consultaNFe.do?method=preFilterCupom&?";
				break;

			case "Piauí":
				url = "http://webas.sefaz.pi.gov.br/nfceweb/consultarNFCe.jsf?";
				break;

			case "Ceará":
				url = "http://nfceh.sefaz.ce.gov.br/pages/ShowNFCe.html?";
				break;

			case "Rio Grande do Norte":
				url = "http://nfce.set.rn.gov.br/consultarNFCe.aspx?";
				break;

			case "Paraíba":
				url = "http://www.receita.pb.gov.br/nfce?";
				break;

			case "Pernambuco":
				url = "http://nfce.sefaz.pe.gov.br/nfce-web/consultarNFCe?";
				break;

			case "Alagoas":
				url = "http://nfce.sefaz.al.gov.br/QRCode/consultarNFCe.jsp?";
				break;

			case "Sergipe":
				url = "http://www.nfce.se.gov.br/portal/consultarNFCe.jsp?";
				break;

			case "Bahia":
				url = "http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?";
				break;

			case "Minas Gerais":
				url = null;
				break;

			case "Espírito Santo":
				url = "http://homologacao.sefaz.es.gov.br/ConsultaNFCe/qrcode.aspx?";
				break;

			case "Rio de Janeiro":
				url = "http://www4.fazenda.rj.gov.br/consultaNFCe/QRCode?";
				break;

			case "São Paulo":
				url = "https://www.nfce.fazenda.sp.gov.br/NFCeConsultaPublica/Paginas/ConsultaQRCode.aspx?";
				break;

			case "Paraná":
				url = "http://www.dfeportal.fazenda.pr.gov.br/dfe-portal/rest/servico/consultaNFCe?";
				break;

			case "Santa Catarina":
				url = null;
				break;

			case "Rio Grande do Sul":
				url = "https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx?";
				break;

			case "Mato Grosso do Sul":
				url = "http://www.dfe.ms.gov.br/nfce/qrcode?";
				break;

			case "Mato Grosso":
				url = "http://www.sefaz.mt.gov.br/nfce/consultanfce?";
				break;

			case "Goiás":
				url = null;
				break;

			case "Distrito Federal":
				url = "http://dec.fazenda.df.gov.br/ConsultarNFCe.aspx?";
				break;
			}

		} else if (ide.getTpAmb() == TpAmb.HOMOLOGACAO) {
			switch (ide.getcUFNome()) {
			case "Rondônia":
				url = "http://www.nfce.sefin.ro.gov.br/consultanfce/consulta.jsp?";
				break;

			case "Acre":
				url = "http://hml.sefaznet.ac.gov.br/nfce/qrcode?";
				break;

			case "Amazonas":
				url = "homnfce.sefaz.am.gov.br/nfceweb/consultarNFCe.jsp?";
				break;

			case "Roraima":
				url = "http://200.174.88.103:8080/nfce/servlet/qrcode?";
				break;

			case "Pará":
				url = "https://appnfc.sefa.pa.gov.br/portal-homologacao/view/consultas/nfce/nfceForm.seam?";
				break;

			case "Amapá":
				url = "https://www.sefaz.ap.gov.br/nfcehml/nfce.php?";
				break;

			case "Tocantins":
				url = null;
				break;

			case "Maranhão":
				url = "http://www.hom.nfce.sefaz.ma.gov.br/portal/consultarNFCe.jsp?";
				break;

			case "Piauí":
				url = "http://webas.sefaz.pi.gov.br/nfceweb-homologacao/consultarNFCe.jsf?";
				break;

			case "Ceará":
				url = "http://nfceh.sefaz.ce.gov.br/pages/ShowNFCe.html?";
				break;

			case "Rio Grande do Norte":
				url = "http://hom.nfce.set.rn.gov.br/consultarNFCe.aspx?";
				break;

			case "Paraíba":
				url = "http://www.receita.pb.gov.br/nfcehom?";
				break;

			case "Pernambuco":
				url = "http://nfcehomolog.sefaz.pe.gov.br/nfce-web/consultarNFCe?";
				break;

			case "Alagoas":
				url = "http://nfce.sefaz.al.gov.br/QRCode/consultarNFCe.jsp?";
				break;

			case "Sergipe":
				url = "http://www.hom.nfe.se.gov.br/portal/consultarNFCe.jsp?";
				break;

			case "Bahia":
				url = "http://hnfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?";
				break;

			case "Minas Gerais":
				url = null;
				break;

			case "Espírito Santo":
				url = "http://homologacao.sefaz.es.gov.br/ConsultaNFCe/qrcode.aspx?";
				break;

			case "Rio de Janeiro":
				url = "http://www4.fazenda.rj.gov.br/consultaNFCe/QRCode?";
				break;

			case "São Paulo":
				url = "https://www.homologacao.nfce.fazenda.sp.gov.br/NFCeConsultaPublica/Paginas/ConsultaQRCode.aspx?";
				break;

			case "Paraná":
				url = "http://www.dfeportal.fazenda.pr.gov.br/dfe-portal/rest/servico/consultaNFCe?";
				break;

			case "Santa Catarina":
				url = null;
				break;

			case "Rio Grande do Sul":
				url = "https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx?";
				break;

			case "Mato Grosso do Sul":
				url = "http://www.dfe.ms.gov.br/nfce/qrcode?";
				break;

			case "Mato Grosso":
				url = "http://homologacao.sefaz.mt.gov.br/nfce/consultanfce?";
				break;

			case "Goiás":
				url = null;
				break;

			case "Distrito Federal":
				url = "http://dec.fazenda.df.gov.br/ConsultarNFCe.aspx?";
				break;
			}
		}
		return url;
	}

	private static String gerarHashQRCode(String preUrl, String cSC) {
		String sha1 = "";
		try {
			String value = preUrl + cSC;
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(value.getBytes("UTF-8"));
			sha1 = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return sha1;
	}

	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
