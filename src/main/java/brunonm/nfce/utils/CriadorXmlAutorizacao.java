package brunonm.nfce.utils;

import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.ModeloNF;
import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;
import brunonm.nfce.modelo.Dest;
import brunonm.nfce.modelo.Det;
import brunonm.nfce.modelo.Emit;
import brunonm.nfce.modelo.Endereco;
import brunonm.nfce.modelo.Ide;
import brunonm.nfce.modelo.Imposto;
import brunonm.nfce.modelo.InfAdic;
import brunonm.nfce.modelo.InfoCert;
import brunonm.nfce.modelo.Pag;
import brunonm.nfce.modelo.Total;

/**
 * Classe responsável por criar o Xml de requisição de Autorização
 * 
 * @author Bruno Melo
 *
 */
public class CriadorXmlAutorizacao implements CriadorXml {

	private static final String NAMESPACE = "http://www.portalfiscal.inf.br/nfe";

	private Versao versao;
	private Ide ide;
	private Emit emit;
	private Dest dest;
	private List<Det> dets;
	private List<Pag> pags;
	private InfAdic infAdic;
	private InfoCert infoCert;
	private Total total;
	private int idLote;
	private boolean indSinc = true;

	/**
	 * Parametros necessarios para criação de um XML já assinada para ser
	 * enviado ao Sefaz.
	 * 
	 * @param versao
	 *            Versão da NFCe.
	 * @param infoCert
	 *            Informações sobre o certificado.
	 * @param idLote
	 *            Identificador de controle do envio do lote.
	 * @param ide
	 *            Informações de identificação geral da NFCe.
	 * @param emit
	 *            Informações sobre o Emitente da NFCe.
	 * @param dets
	 *            Informações sobre os produtos e os impostos do produto.
	 * @param pags
	 *            Informações sobre o pagamento.
	 * @param infAdic
	 *            Informações adicionais.
	 */
	public CriadorXmlAutorizacao(Versao versao, InfoCert infoCert, int idLote, Ide ide, Emit emit, List<Det> dets,
			List<Pag> pags, InfAdic infAdic) {
		this.versao = versao;
		this.infoCert = infoCert;
		this.idLote = idLote;
		this.ide = ide;
		this.emit = emit;
		this.dets = dets;
		this.pags = pags;
		this.infAdic = infAdic;
		total = new Total(dets);
	}

	/**
	 * Cria o arcabouço padrão do xml para Autorização
	 * 
	 * @return Document com o xml já estruturado.
	 * @throws VersaoNFCeErradaException
	 *             Será lançado quando houver discrepâncias nas informações das
	 *             versões das notas
	 */
	public SoapObject criarSoapObject() throws VersaoNFCeErradaException {
		SoapObject enviNFe = new SoapObject(null, "enviNFe");
		enviNFe.addAttribute(null, "xmlns", NAMESPACE);
		enviNFe.addAttribute("versao", versao.getVersao());
		PropertyInfo p = new PropertyInfo();
		p.setName("teste");
		p.setValue(criaStringDocument());
		p.setType(Object.class);
		enviNFe.addProperty(p);
		// System.out.println(enviNFe.toString());
		return enviNFe;
	}

	private String criaStringDocument() throws VersaoNFCeErradaException {
		StringWriter writer = null;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = criarRootElement(doc);

			// ide elements
			Element ideElement = criarIdeElement(doc);
			rootElement.appendChild(ideElement);

			// emit elements
			Element emitElement = criarEmitElement(doc);
			rootElement.appendChild(emitElement);

			// dest elements
			criarEAnexarDestElement(doc, rootElement);

			// det elements
			criarEAnexarDetsElement(doc, rootElement);

			// total elements
			Element totalElement = criarTotalElement(doc);
			rootElement.appendChild(totalElement);

			// transp Element
			Element transpElement = criarTranspElement(doc);
			rootElement.appendChild(transpElement);

			// pag elements
			criarEAnexarPagsElement(doc, rootElement);

			// infAdic Element
			criarEAnexarinfAdicElement(doc, rootElement);

			doc = assinarXml(doc);

			// infNFeSupl
			CriarQRCode criarQRCode = new CriarQRCode(ide, emit, total, dest, doc);
			doc = criarQRCode.criarDocumentoComQRCode();

			// enviNFe
			doc = criaEnviNFe(doc, docBuilder);

			DOMSource domSource = new DOMSource(doc);
			writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(domSource, result);

		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
		return writer.toString();
	}

	private Document criaEnviNFe(Document doc, DocumentBuilder docBuilder) throws ParserConfigurationException {
		Document NFeDocument = docBuilder.newDocument();
		NFeDocument.setXmlStandalone(true);
		Element enviNFe = NFeDocument.createElementNS("http://www.portalfiscal.inf.br/nfe", "enviNFe");
		enviNFe.setAttribute("versao", versao.getVersao());
		NFeDocument.appendChild(enviNFe);

		// idLote
		Element idLote = NFeDocument.createElementNS("http://www.portalfiscal.inf.br/nfe", "idLote");
		idLote.setTextContent(String.valueOf(this.idLote));
		enviNFe.appendChild(idLote);

		// indSinc
		Element indSinc = NFeDocument.createElementNS("http://www.portalfiscal.inf.br/nfe", "indSinc");
		indSinc.setTextContent(getIndSinc());
		enviNFe.appendChild(indSinc);

		Node nfeNode = doc.getFirstChild();
		Node enviNFeNode = NFeDocument.getDocumentElement();
		Node firstDocImportedNode = NFeDocument.adoptNode(nfeNode);
		enviNFeNode.appendChild(firstDocImportedNode);

		return NFeDocument;
	}

	/**
	 * assina o xml usando o certificado informado
	 * 
	 * @param doc
	 *            caminho com o arquivo xml a ser assinado
	 * @return
	 */
	private Document assinarXml(Document doc) {
		Document xmlAssinado = null;
		try {
			AssinadorXml assinadorXml = new AssinadorXml(infoCert);

			xmlAssinado = assinadorXml.AssinarXml(ide.getNfID(), doc);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return xmlAssinado;
	}

	/**
	 * Cria o elemento raiz com as Informações da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param nFe
	 * @return node com a raiz do xml onde serão inserido os outros elementos
	 */
	private Element criarRootElement(Document doc) {
		Element nFe = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "NFe");
		doc.appendChild(nFe);

		Element infNFe = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "infNFe");
		nFe.appendChild(infNFe);

		infNFe.setAttribute("versao", versao.getVersao());
		infNFe.setAttribute("Id", ide.getNfID());
		return infNFe;
	}

	/**
	 * Cria um node com todos os elemtos do Informações de identificação da
	 * NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @return node com o elemento ide da NFe.
	 * @throws VersaoNFCeErradaException
	 */
	private Element criarIdeElement(Document doc) throws VersaoNFCeErradaException {
		Element ideElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ide");

		// cUF element
		Element cUF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cUF");
		cUF.appendChild(doc.createTextNode(String.valueOf(ide.getcUF().getCodigoIbge())));
		ideElement.appendChild(cUF);

		// cNF element
		Element cNF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cNF");
		cNF.appendChild(doc.createTextNode(ide.getcNF()));
		ideElement.appendChild(cNF);

		// natOp element
		Element natOp = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "natOp");
		natOp.appendChild(doc.createTextNode(ide.getNatOP()));
		ideElement.appendChild(natOp);

		if (versao == Versao.V310) {
			// indPag element
			if (ide.getIndPag() == null)
				throw new VersaoNFCeErradaException(
						"NFCe da versão 3.10 deve utilizar o construtror do Ide com o IndPag");
			Element indPag = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "indPag");
			indPag.appendChild(doc.createTextNode(ide.getIndPag()));
			ideElement.appendChild(indPag);
		}

		// mod element
		Element mod = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "mod");
		mod.appendChild(doc.createTextNode(ide.getMod().getModeloNF()));
		ideElement.appendChild(mod);

		// serie element
		Element serie = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "serie");
		serie.appendChild(doc.createTextNode(ide.getSerie()));
		ideElement.appendChild(serie);

		// nNF element
		Element nNF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "nNF");
		nNF.appendChild(doc.createTextNode(ide.getnNF()));
		ideElement.appendChild(nNF);

		// dhEmi element
		Element dhEmi = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "dhEmi");
		dhEmi.appendChild(doc.createTextNode(ide.getDhEmi()));
		ideElement.appendChild(dhEmi);

		// dhSaiEnt element (opcional)
		if (ide.getDhSaiEnt() != null) {
			Element dhSaiEnt = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "dhSaiEnt");
			dhSaiEnt.appendChild(doc.createTextNode(ide.getDhSaiEnt()));
			ideElement.appendChild(dhSaiEnt);
		}

		// tpNF element
		Element tpNF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tpNF");
		tpNF.appendChild(doc.createTextNode(String.valueOf(ide.getTpNF())));
		ideElement.appendChild(tpNF);

		// idDest element
		Element idDest = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "idDest");
		idDest.appendChild(doc.createTextNode(ide.getIdDest()));
		ideElement.appendChild(idDest);

		// cMunFG element
		Element cMunFG = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cMunFG");
		cMunFG.appendChild(doc.createTextNode(String.valueOf(ide.getcMunFG())));
		ideElement.appendChild(cMunFG);

		// tpImp element
		Element tpImp = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tpImp");
		tpImp.appendChild(doc.createTextNode(String.valueOf(ide.getTpImp())));
		ideElement.appendChild(tpImp);

		// tpEmis element
		Element tpEmis = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tpEmis");
		tpEmis.appendChild(doc.createTextNode(String.valueOf(ide.getTpEmis().getFormaEmissaoNFe())));
		ideElement.appendChild(tpEmis);

		// cDV element
		Element cDV = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cDV");
		cDV.appendChild(doc.createTextNode(String.valueOf(ide.getcDV())));
		ideElement.appendChild(cDV);

		// tpAmb element
		Element tpAmb = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tpAmb");
		tpAmb.appendChild(doc.createTextNode(String.valueOf(ide.getTpAmb().getAmbiente())));
		ideElement.appendChild(tpAmb);

		// finNFe element
		Element finNFe = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "finNFe");
		finNFe.appendChild(doc.createTextNode(String.valueOf(ide.getFinNFe())));
		ideElement.appendChild(finNFe);

		// indFinal element
		Element indFinal = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "indFinal");
		indFinal.appendChild(doc.createTextNode(ide.getIndFinal()));
		ideElement.appendChild(indFinal);

		// indPres element
		if (versao == Versao.V310 && ide.getIndPres().equals("5"))
			throw new VersaoNFCeErradaException(
					"NFCe da versão 3.10 não podem utilizar o operado IndPres 5: Operação presencial, fora do estabelecimento");
		Element indPres = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "indPres");
		indPres.appendChild(doc.createTextNode(ide.getIndPres()));
		ideElement.appendChild(indPres);

		// procEmi element
		Element procEmi = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "procEmi");
		procEmi.appendChild(doc.createTextNode(String.valueOf(ide.getProcEmi())));
		ideElement.appendChild(procEmi);

		// verProc element
		Element verProc = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "verProc");
		verProc.appendChild(doc.createTextNode(ide.getVerProc()));
		ideElement.appendChild(verProc);

		if (ide.getTpEmis() != TpEmis.NORMAL) {
			// dhCont element

			if (ide.getDhCont() == null)
				throw new IllegalArgumentException(
						"NFC-e com tipo de emissão diferente de 1 deve conter o campo dhCont");
			Element dhCont = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "dhCont");
			dhCont.appendChild(doc.createTextNode(ide.getDhCont()));
			ideElement.appendChild(dhCont);

			// xJust element
			if (ide.getxJust() == null)
				throw new IllegalArgumentException(
						"NFC-e com tipo de emissão diferente de 1 deve conter o campo xJust");
			Element xJust = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xJust");
			xJust.appendChild(doc.createTextNode(ide.getxJust()));
			ideElement.appendChild(xJust);
		}

		return ideElement;
	}

	/**
	 * Cria um node com todos os elemtos da Identificação do emitente da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @return node com o elemento emit da NFe.
	 * @throws VersaoNFCeErradaException
	 */
	private Element criarEmitElement(Document doc) throws VersaoNFCeErradaException {
		Element emitElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "emit");
		Endereco endereco = emit.getEndereco();

		// CNPJ element
		if (emit.getCnpj() != null) {
			Element cNPJ = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CNPJ");
			cNPJ.appendChild(doc.createTextNode(emit.getCnpj()));
			emitElement.appendChild(cNPJ);
		}

		// CPF element
		if (emit.getCpf() != null) {
			Element cPF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CPF");
			cPF.appendChild(doc.createTextNode(emit.getCpf()));
			emitElement.appendChild(cPF);
		}

		// xNome element
		Element xNome = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xNome");
		xNome.appendChild(doc.createTextNode(emit.getxNome()));
		emitElement.appendChild(xNome);

		// xFant element
		if (emit.getxFant() != null) {
			Element xFant = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xFant");
			xFant.appendChild(doc.createTextNode(emit.getxFant()));
			emitElement.appendChild(xFant);
		}

		Element enderEmit = criaEnderecoElement(doc, endereco, EnderTipo.EMIT);
		emitElement.appendChild(enderEmit);

		// IE element
		Element iE = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "IE");
		iE.appendChild(doc.createTextNode(emit.getIe()));
		emitElement.appendChild(iE);

		// IEST element
		if (emit.getiEST() != null) {
			Element iEST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "IEST");
			iEST.appendChild(doc.createTextNode(emit.getiEST()));
			emitElement.appendChild(iEST);
		}

		if (emit.getiM() != null) {
			// IM element
			Element iM = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "IM");
			iM.appendChild(doc.createTextNode(emit.getiM()));
			emitElement.appendChild(iM);

			// CNAE element
			if (emit.getcNAE() != null) {
				Element cNAE = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CNAE");
				cNAE.appendChild(doc.createTextNode(emit.getcNAE()));
				emitElement.appendChild(cNAE);
			}
		}

		// CRT element
		Element cRT = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CRT");
		cRT.appendChild(doc.createTextNode(String.valueOf(emit.getCrt())));
		emitElement.appendChild(cRT);

		return emitElement;
	}

	/**
	 * Cria os elementos de endereço do emitente ou destinatário.
	 * 
	 * @param doc
	 *            Documento principal
	 * @param endereco
	 *            endereço do emitente ou destinatário
	 * @return elemento endereço com as informações já anexadas.
	 * @throws VersaoNFCeErradaException
	 */
	private Element criaEnderecoElement(Document doc, Endereco endereco, EnderTipo enderTipo)
			throws VersaoNFCeErradaException {
		// enderEmit element
		Element enderEmit = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", enderTipo.getEnderTipo());

		// xLgr element
		Element xLgr = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xLgr");
		xLgr.appendChild(doc.createTextNode(endereco.getxLgr()));
		enderEmit.appendChild(xLgr);

		// nro element
		Element nro = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "nro");
		nro.appendChild(doc.createTextNode(endereco.getNro()));
		enderEmit.appendChild(nro);

		// xCpl element
		if (endereco.getxCpl() != null) {
			Element xCpl = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xCpl");
			xCpl.appendChild(doc.createTextNode(endereco.getxCpl()));
			enderEmit.appendChild(xCpl);
		}

		// xBairro element
		Element xBairro = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xBairro");
		xBairro.appendChild(doc.createTextNode(endereco.getxBairro()));
		enderEmit.appendChild(xBairro);

		// cMun element
		Element cMun = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cMun");
		cMun.appendChild(doc.createTextNode(String.valueOf(endereco.getcMun())));
		enderEmit.appendChild(cMun);

		// xMun element
		Element xMun = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xMun");
		xMun.appendChild(doc.createTextNode(endereco.getxMun()));
		enderEmit.appendChild(xMun);

		// UF element
		Element uF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "UF");
		uF.appendChild(doc.createTextNode(endereco.getUf()));
		enderEmit.appendChild(uF);

		// CEP element
		if (endereco.getCep() != null) {
			Element cEP = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CEP");
			cEP.appendChild(doc.createTextNode(endereco.getCep()));
			enderEmit.appendChild(cEP);
		} else if (endereco.getCep() == null && enderTipo == EnderTipo.EMIT) {
			throw new VersaoNFCeErradaException(
					"A informação de endereço do emitente deve, obrigatoriamente, conter o CEP");
		}

		// cPais element
		Element cPais = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cPais");
		if (enderTipo == EnderTipo.EMIT) {
			cPais.appendChild(doc.createTextNode("1058"));
		} else {
			cPais.appendChild(doc.createTextNode(String.valueOf(endereco.getcPais())));
		}
		enderEmit.appendChild(cPais);

		// xPais element
		Element xPais = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xPais");
		if (enderTipo == EnderTipo.EMIT) {
			xPais.appendChild(doc.createTextNode("BRASIL"));
		} else {
			xPais.appendChild(doc.createTextNode(String.valueOf(endereco.getxPais())));
		}
		enderEmit.appendChild(xPais);

		// fone element
		if (endereco.getFone() != null) {
			Element fone = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "fone");
			fone.appendChild(doc.createTextNode(endereco.getFone()));
			enderEmit.appendChild(fone);
		}
		return enderEmit;
	}

	/**
	 * Cria e anexa os elementos de Identificação do Destinatário da NF-e.
	 * 
	 * @param doc
	 *            Documento principal
	 * @param rootElement
	 *            node com a raiz do xml
	 * @throws VersaoNFCeErradaException
	 */
	private void criarEAnexarDestElement(Document doc, Element rootElement) throws VersaoNFCeErradaException {
		if (dest != null) {
			Element destElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "dest");
			Endereco endereco = dest.getEndereco();

			// CNPJ element
			if (dest.getCnpj() != null) {
				Element CNPJ = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CNPJ");
				CNPJ.appendChild(doc.createTextNode(dest.getCnpj()));
				destElement.appendChild(CNPJ);
			}

			// CPF element
			if (dest.getCpf() != null) {
				Element CPF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CPF");
				CPF.appendChild(doc.createTextNode(dest.getCpf()));
				destElement.appendChild(CPF);
			}

			// idEstrangeiro element
			if (dest.getIdEstrangeiro() != null) {
				Element idEstrangeiro = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "idEstrangeiro");
				idEstrangeiro.appendChild(doc.createTextNode(dest.getIdEstrangeiro()));
				destElement.appendChild(idEstrangeiro);
			}

			// xNome element
			if (dest.getxNome() != null) {
				Element xNome = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xNome");
				xNome.appendChild(doc.createTextNode(dest.getxNome()));
				destElement.appendChild(xNome);
			} else if (dest.getxNome() == null && ide.getMod() == ModeloNF.NFe) {
				throw new VersaoNFCeErradaException("NFCe no modelo 55 deve, obrigatoriamente, conter o xNome");
			}

			// enderDest element
			if (dest.getEndereco() != null) {
				Element enderEmit = criaEnderecoElement(doc, endereco, EnderTipo.DEST);
				destElement.appendChild(enderEmit);
			} else if (dest.getEndereco() == null && ide.getMod() == ModeloNF.NFe) {
				throw new VersaoNFCeErradaException(
						"NFCe no modelo 55 deve, obrigatoriamente, conter o campo endereço");
			}

			// indIEDest element
			Element indIEDest = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "indIEDest");
			indIEDest.appendChild(doc.createTextNode(dest.getIndIEDest()));
			destElement.appendChild(indIEDest);

			// IE element
			if (dest.getIndIEDest().equals("1")) {
				if (dest.getiE() == null)
					throw new VersaoNFCeErradaException(
							"Destinatario com indIEDest = CONTRIBUINTE_ICMS devem, obrigatoriamente, ter o campo IE preenchido");
				Element IE = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "IE");
				IE.appendChild(doc.createTextNode(dest.getiE()));
				destElement.appendChild(IE);
			}

			// ISUF element
			if (dest.getiSUF() != null) {
				Element ISUF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ISUF");
				ISUF.appendChild(doc.createTextNode(dest.getiSUF()));
				destElement.appendChild(ISUF);
			}

			// IM element
			if (dest.getiM() != null) {
				Element IM = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "IM");
				IM.appendChild(doc.createTextNode(dest.getiM()));
				destElement.appendChild(IM);
			}

			// email element
			if (dest.getEmail() != null) {
				Element email = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "email");
				email.appendChild(doc.createTextNode(dest.getEmail()));
				destElement.appendChild(email);
			}

			rootElement.appendChild(destElement);
		} else {
			if (ide.getMod() == ModeloNF.NFe)
				throw new VersaoNFCeErradaException(
						"NFCe no modelo 55 deve, obrigatoriamente, conter as informações do destinatário.");
		}
	}

	/**
	 * Cria e anexa todos os elementos de Detalhamento de Produtos e Serviços
	 * da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param rootElement
	 *            node com a raiz do xml
	 */
	private void criarEAnexarDetsElement(Document doc, Element rootElement) {
		int nItem = 1;
		for (int i = 0; i < dets.size(); i++) {
			Element detElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "det");
			detElement.setAttribute("nItem", String.valueOf(nItem));

			Element prodElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "prod");
			Det currentDet = dets.get(i);

			// cProd element
			Element cProd = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cProd");
			cProd.appendChild(doc.createTextNode(currentDet.getcProd()));
			prodElement.appendChild(cProd);

			// cEAN element
			Element cEAN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cEAN");
			cEAN.appendChild(doc.createTextNode(currentDet.getcEAN()));
			prodElement.appendChild(cEAN);

			// xProd element
			Element xProd = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "xProd");
			xProd.appendChild(doc.createTextNode(currentDet.getxProd()));
			prodElement.appendChild(xProd);

			// NCM element
			Element nCM = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "NCM");
			nCM.appendChild(doc.createTextNode(String.valueOf(currentDet.getnCM())));
			prodElement.appendChild(nCM);

			// NVE element
			if (currentDet.getnVE() != null) {
				Element nVE = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "NVE");
				nVE.appendChild(doc.createTextNode(currentDet.getnVE()));
				prodElement.appendChild(nVE);
			}

			// EXTIPI element
			if (currentDet.geteXTIPI() != null) {
				Element eXTIPI = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "EXTIPI");
				eXTIPI.appendChild(doc.createTextNode(currentDet.geteXTIPI()));
				prodElement.appendChild(eXTIPI);
			}

			// CFOP element
			Element cFOP = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CFOP");
			cFOP.appendChild(doc.createTextNode(String.valueOf(currentDet.getcFOP())));
			prodElement.appendChild(cFOP);

			// uCom element
			Element uCom = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "uCom");
			uCom.appendChild(doc.createTextNode(currentDet.getuCom()));
			prodElement.appendChild(uCom);

			// qCom element
			Element qCom = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "qCom");
			qCom.appendChild(doc.createTextNode(currentDet.getqCom()));
			prodElement.appendChild(qCom);

			// vUnCom element
			Element vUnCom = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vUnCom");
			vUnCom.appendChild(doc.createTextNode(currentDet.getvUnCom()));
			prodElement.appendChild(vUnCom);

			// vProd element
			Element vProd = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vProd");
			vProd.appendChild(doc.createTextNode(currentDet.getvProd()));
			prodElement.appendChild(vProd);

			// cEANTrib element
			Element cEANTrib = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cEANTrib");
			cEANTrib.appendChild(doc.createTextNode(currentDet.getcEANTrib()));
			prodElement.appendChild(cEANTrib);

			// uTrib element
			Element uTrib = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "uTrib");
			uTrib.appendChild(doc.createTextNode(currentDet.getuTrib()));
			prodElement.appendChild(uTrib);

			// qTrib element
			Element qTrib = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "qTrib");
			qTrib.appendChild(doc.createTextNode(dets.get(i).getqTrib()));
			prodElement.appendChild(qTrib);

			// vUnTrib element
			Element vUnTrib = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vUnTrib");
			vUnTrib.appendChild(doc.createTextNode(currentDet.getvUnTrib()));
			prodElement.appendChild(vUnTrib);

			// vFrete element
			if (currentDet.getvFrete() != null) {
				Element vFrete = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFrete");
				vFrete.appendChild(doc.createTextNode(currentDet.getvFrete()));
				prodElement.appendChild(vFrete);
			}

			// vSeg element
			if (currentDet.getvSeg() != null) {
				Element vSeg = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vSeg");
				vSeg.appendChild(doc.createTextNode(currentDet.getvSeg()));
				prodElement.appendChild(vSeg);
			}

			// vDesc element
			if (currentDet.getvDesc() != null) {
				Element vDesc = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vDesc");
				vDesc.appendChild(doc.createTextNode(currentDet.getvDesc()));
				prodElement.appendChild(vDesc);
			}

			// vOutro element
			if (currentDet.getvOutro() != null) {
				Element vOutro = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vOutro");
				vOutro.appendChild(doc.createTextNode(currentDet.getvOutro()));
				prodElement.appendChild(vOutro);
			}

			// indTot element
			Element indTot = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "indTot");
			indTot.appendChild(doc.createTextNode(currentDet.getIndTot()));
			prodElement.appendChild(indTot);

			Element impostoICMSElement = criarICMSImpostoElement(doc, currentDet);
			Element impostoPISElement = criarPISImpostoElement(doc, currentDet);
			Element impostoCOFINSElement = criarCOFINSImpostoElement(doc, currentDet);

			Element impostoElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "imposto");

			detElement.appendChild(prodElement);
			impostoElement.appendChild(impostoICMSElement);
			if (emit.getCrt() != 3) {
				impostoElement.appendChild(impostoPISElement);
				impostoElement.appendChild(impostoCOFINSElement);
			}

			detElement.appendChild(impostoElement);
			rootElement.appendChild(detElement);
			nItem++;
		}
	}

	/**
	 * Cria o elementos de imposto ICMS para ser anexado ao det. da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param det
	 *            Det do element atual.
	 * @return node com o elemento imposto do det.
	 */
	private Element criarICMSImpostoElement(Document doc, Det det) {
		Element iCMSBaseElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ICMS");

		Imposto imposto = det.getImposto();
		switch (emit.getCrt()) {

		case 1:
		case 2:
			if (imposto.getcSOSN() == null)
				throw new IllegalArgumentException(
						"Empresas do Simples nacional deve ter produtos tributados pelo CSOSN.");

			String cSOSN = imposto.getcSOSN();

			Element icmsCSOSN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ICMSSN" + cSOSN);

			// orig element
			Element origCSOSN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "orig");
			origCSOSN.appendChild(doc.createTextNode(String.valueOf(imposto.getOrig())));
			icmsCSOSN.appendChild(origCSOSN);

			// CSOSN element
			Element CSOSN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CSOSN");
			CSOSN.appendChild(doc.createTextNode(imposto.getcSOSN()));
			icmsCSOSN.appendChild(CSOSN);

			switch (cSOSN) {
			case "101":
			case "201":
			case "202":
			case "900":
				imposto.setvBC(det.getvProd());
				if (cSOSN.equals("900")) {
					if (checkGrupoOpcionalModBC900(cSOSN, imposto)) {
						// modBC element
						if (imposto.getModBC() == null)
							throw new IllegalArgumentException(
									"Empresas com CSOSN " + cSOSN + " devem ter o ModBC preenchido.");
						Element modBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modBC");
						modBC.appendChild(doc.createTextNode(imposto.getModBC()));
						icmsCSOSN.appendChild(modBC);

						// vBC element
						Element vBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBC");
						vBC.appendChild(doc.createTextNode(imposto.getvBC()));
						icmsCSOSN.appendChild(vBC);

						// pRedBC element
						if (imposto.getpRedBC() != null) {
							Element pRedBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pRedBC");
							pRedBC.appendChild(doc.createTextNode(imposto.getpRedBC()));
							icmsCSOSN.appendChild(pRedBC);
						}

						// pICMS element
						if (imposto.getpICMS() == null)
							throw new IllegalArgumentException(
									"Empresas com CSOSN " + cSOSN + " devem ter o pICMS preenchido.");
						Element pICMS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pICMS");
						pICMS.appendChild(doc.createTextNode(imposto.getpICMS()));
						icmsCSOSN.appendChild(pICMS);

						// vICMS element
						Element vICMS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMS");
						vICMS.appendChild(doc.createTextNode(imposto.getvICMS()));
						icmsCSOSN.appendChild(vICMS);
					}
				}

				if (!cSOSN.equals("101")) {
					// modBCST element
					if (imposto.getModBCST() == null)
						throw new IllegalArgumentException(
								"Empresas com CSOSN " + cSOSN + " devem ter o modBCST preenchido.");
					if (checkGrupoOpcionalModBCST900(cSOSN, imposto)) {
						Element modBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modBCST");
						modBCST.appendChild(doc.createTextNode(imposto.getModBCST()));
						icmsCSOSN.appendChild(modBCST);
					}

					// pMVAST element
					if (imposto.getpMVAST() != null) {
						if (checkGrupoOpcionalModBCST900(cSOSN, imposto)) {
							Element pMVAST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pMVAST");
							pMVAST.appendChild(doc.createTextNode(imposto.getpMVAST()));
							icmsCSOSN.appendChild(pMVAST);
						}
					}

					// vBCST element
					if (imposto.getvBCST() == null)
						throw new IllegalArgumentException(
								"Empresas com CSOSN " + cSOSN + " devem ter o vBCST preenchido.");
					if (checkGrupoOpcionalModBCST900(cSOSN, imposto)) {
						Element vBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBCST");
						vBCST.appendChild(doc.createTextNode(imposto.getvBCST()));
						icmsCSOSN.appendChild(vBCST);
					}

					// pICMSST element
					if (imposto.getpICMSST() == null)
						throw new IllegalArgumentException(
								"Empresas com CSOSN " + cSOSN + " devem ter o pICMSST preenchido.");
					if (checkGrupoOpcionalModBCST900(cSOSN, imposto)) {
						Element pICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pICMSST");
						pICMSST.appendChild(doc.createTextNode(imposto.getpICMSST()));
						icmsCSOSN.appendChild(pICMSST);
					}

					// vICMSST element
					if (imposto.getvICMSST() == null)
						throw new IllegalArgumentException(
								"Empresas com CSOSN " + cSOSN + " devem ter o vICMSST preenchido.");
					if (checkGrupoOpcionalModBCST900(cSOSN, imposto)) {
						Element vICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMSST");
						vICMSST.appendChild(doc.createTextNode(imposto.getvICMSST()));
						icmsCSOSN.appendChild(vICMSST);
					}
				}

				if (!cSOSN.equals("202")) {
					// pCredSN element
					if (checkGrupoOpcionalPCredSN900(cSOSN, imposto)) {
						Element pCredSN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pCredSN");
						pCredSN.appendChild(doc.createTextNode(imposto.getpCredSN()));
						icmsCSOSN.appendChild(pCredSN);
					}

					// vCredICMSSN element
					if (checkGrupoOpcionalPCredSN900(cSOSN, imposto)) {
						Element vCredICMSSN = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vCredICMSSN");
						vCredICMSSN.appendChild(doc.createTextNode(imposto.getvCredICMSSN()));
						icmsCSOSN.appendChild(vCredICMSSN);
					}
				}

				break;
			}
			iCMSBaseElement.appendChild(icmsCSOSN);
			break;

		case 3:
			imposto.setvBC(det.getvProd());
			String cST = imposto.getcST();
			if (cST == null)
				throw new IllegalArgumentException("Empresas no Regime normal deve ter produtos tributados pelo CST.");
			if (cST.equals("41") || cST.equals("50"))
				cST = "40";

			Element icmsCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ICMS" + cST);

			// orig element
			Element origCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "orig");
			origCST.appendChild(doc.createTextNode(String.valueOf(imposto.getOrig())));
			icmsCST.appendChild(origCST);

			// CST element
			Element CST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CST");
			CST.appendChild(doc.createTextNode(imposto.getcST()));
			icmsCST.appendChild(CST);

			switch (cST) {

			case "00":
			case "10":
			case "20":
			case "51":
			case "70":
			case "90":
				// modBC element
				if (imposto.getModBC() == null)
					if (!cST.equals("51") || !cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o ModBC preenchido.");
				if (imposto.getModBC() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
					Element modBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modBC");
					modBC.appendChild(doc.createTextNode(imposto.getModBC()));
					icmsCST.appendChild(modBC);
				}

				// pMVAST element
				if (imposto.getpMVAST() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
					Element pMVAST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pMVAST");
					pMVAST.appendChild(doc.createTextNode(imposto.getpMVAST()));
					icmsCST.appendChild(pMVAST);
				}

				// pRedBC element
				if (cST.equals("20") || cST.equals("51") || cST.equals("70")) {
					if (imposto.getpRedBC() == null)
						if (!cST.equals("51"))
							throw new IllegalArgumentException(
									"Empresas com CST " + cST + " devem ter o pRedBC preenchido.");
					if (imposto.getpRedBC() != null) {
						Element pRedBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pRedBC");
						pRedBC.appendChild(doc.createTextNode(imposto.getpRedBC()));
						icmsCST.appendChild(pRedBC);
					}
				}

				// vBC element
				imposto.setvBC(det.getvProd());
				if (imposto.getvBC() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
					Element vBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBC");
					vBC.appendChild(doc.createTextNode(imposto.getvBC()));
					icmsCST.appendChild(vBC);
				}

				// pRedBC element para cst 90 (opcional)
				if (cST.equals("90")) {
					if (imposto.getpRedBC() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
						Element pRedBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pRedBC");
						pRedBC.appendChild(doc.createTextNode(imposto.getpRedBC()));
						icmsCST.appendChild(pRedBC);
					}
				}

				// pICMS element
				if (imposto.getpICMS() == null)
					if (!cST.equals("51") || !cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o pICMS preenchido.");
				if (imposto.getpICMS() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
					Element pICMS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pICMS");
					pICMS.appendChild(doc.createTextNode(imposto.getpICMS()));
					icmsCST.appendChild(pICMS);
				}

				// vICMS element
				if (imposto.getvICMS() != null && checkGrupoOpcionalModBC90(cST, imposto)) {
					Element vICMS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMS");
					vICMS.appendChild(doc.createTextNode(imposto.getvICMS()));
					icmsCST.appendChild(vICMS);
				}

				// Break dos grupos 00, 20, 51
				if (cST.equals("00") || cST.equals("20") || cST.equals("51"))
					break;

				// modBCST element
				if (imposto.getModBCST() == null)
					if (!cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o modBCST preenchido.");
				if (checkGrupoOpcionalModBCST90(cST, imposto)) {
					Element modBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modBCST");
					modBCST.appendChild(doc.createTextNode(imposto.getModBCST()));
					icmsCST.appendChild(modBCST);
				}

				// pMVAST element
				if (imposto.getpMVAST() != null && checkGrupoOpcionalModBCST90(cST, imposto)) {
					Element pMVAST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pMVAST");
					pMVAST.appendChild(doc.createTextNode(imposto.getpMVAST()));
					icmsCST.appendChild(pMVAST);
				}

				// vBCST element
				if (imposto.getvBCST() == null)
					if (!cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o vBCST preenchido.");
				if (checkGrupoOpcionalModBCST90(cST, imposto)) {
					Element vBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBCST");
					vBCST.appendChild(doc.createTextNode(imposto.getvBCST()));
					icmsCST.appendChild(vBCST);
				}

				// pICMSST element
				if (imposto.getpICMSST() == null)
					if (!cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o pICMSST preenchido.");
				if (checkGrupoOpcionalModBCST90(cST, imposto)) {
					Element pICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pICMSST");
					pICMSST.appendChild(doc.createTextNode(imposto.getpICMSST()));
					icmsCST.appendChild(pICMSST);
				}

				// vICMSST element
				if (imposto.getvICMSST() == null)
					if (!cST.equals("90"))
						throw new IllegalArgumentException(
								"Empresas com CST " + cST + " devem ter o vICMSST preenchido.");
				if (checkGrupoOpcionalModBCST90(cST, imposto)) {
					Element vICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMSST");
					vICMSST.appendChild(doc.createTextNode(imposto.getvICMSST()));
					icmsCST.appendChild(vICMSST);
				}
				break;

			case "30":
				// modBCST element
				if (imposto.getModBCST() == null)
					throw new IllegalArgumentException("Empresas com CST " + cST + " devem ter o modBCST preenchido.");
				Element modBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modBCST");
				modBCST.appendChild(doc.createTextNode(imposto.getModBCST()));
				icmsCST.appendChild(modBCST);

				// pMVAST element
				if (imposto.getpMVAST() != null) {
					Element pMVAST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pMVAST");
					pMVAST.appendChild(doc.createTextNode(imposto.getpMVAST()));
					icmsCST.appendChild(pMVAST);
				}

				// vBCST element
				if (imposto.getvBCST() == null)
					throw new IllegalArgumentException("Empresas com CST " + cST + " devem ter o vBCST preenchido.");
				Element vBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBCST");
				vBCST.appendChild(doc.createTextNode(imposto.getvBCST()));
				icmsCST.appendChild(vBCST);

				// pICMSST element
				if (imposto.getpICMSST() == null)
					throw new IllegalArgumentException("Empresas com CST " + cST + " devem ter o pICMSST preenchido.");
				Element pICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pICMSST");
				pICMSST.appendChild(doc.createTextNode(imposto.getpICMSST()));
				icmsCST.appendChild(pICMSST);

				// vICMSST element
				if (imposto.getvICMSST() == null)
					throw new IllegalArgumentException("Empresas com CST " + cST + " devem ter o vICMSST preenchido.");
				Element vICMSST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMSST");
				vICMSST.appendChild(doc.createTextNode(imposto.getvICMSST()));
				icmsCST.appendChild(vICMSST);

				break;
			}
			iCMSBaseElement.appendChild(icmsCST);

			break;
		}

		return iCMSBaseElement;
	}

	/**
	 * Cria o elementos de imposto PIS para ser anexado ao det. da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param det
	 *            Det do element atual.
	 * @return node com o elemento imposto do det.
	 */
	private Element criarPISImpostoElement(Document doc, Det det) {
		Element pISBaseElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "PIS");
		Imposto imposto = det.getImposto();

		switch (emit.getCrt()) {

		case 1:
		case 2:
			imposto.setvBC(det.getvProd());
			Element PISOutrElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "PISOutr");
			// CST element
			Element cST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CST");
			cST.appendChild(doc.createTextNode("49"));
			PISOutrElement.appendChild(cST);

			// vBC element
			Element vBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBC");
			vBC.appendChild(doc.createTextNode(imposto.getvBC()));
			PISOutrElement.appendChild(vBC);

			// pPIS element
			Element pPIS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pPIS");
			pPIS.appendChild(doc.createTextNode(imposto.getpPIS()));
			PISOutrElement.appendChild(pPIS);

			// vPIS element
			Element vPIS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vPIS");
			vPIS.appendChild(doc.createTextNode(imposto.getvPIS()));
			PISOutrElement.appendChild(vPIS);

			pISBaseElement.appendChild(PISOutrElement);
			break;
		case 3:

			break;

		}

		return pISBaseElement;
	}

	/**
	 * Cria o elementos de imposto COFINS para ser anexado ao det. da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param det
	 *            Det do element atual.
	 * @return node com o elemento imposto do det.
	 */
	private Element criarCOFINSImpostoElement(Document doc, Det det) {
		Element cOFINSBaseElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "COFINS");
		Imposto imposto = det.getImposto();

		switch (emit.getCrt()) {

		case 1:
		case 2:
			imposto.setvBC(det.getvProd());
			Element COFINSOutrElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "COFINSOutr");
			// CST element
			Element cST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CST");
			cST.appendChild(doc.createTextNode("49"));
			COFINSOutrElement.appendChild(cST);

			// vBC element
			Element vBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBC");
			vBC.appendChild(doc.createTextNode(imposto.getvBC()));
			COFINSOutrElement.appendChild(vBC);

			// pCOFINS element
			Element pCOFINS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pCOFINS");
			pCOFINS.appendChild(doc.createTextNode(imposto.getpCOFINS()));
			COFINSOutrElement.appendChild(pCOFINS);

			// vCOFINS element
			Element vCOFINS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vCOFINS");
			vCOFINS.appendChild(doc.createTextNode(imposto.getvCOFINS()));
			COFINSOutrElement.appendChild(vCOFINS);

			cOFINSBaseElement.appendChild(COFINSOutrElement);
			break;
		case 3:

			break;

		}

		return cOFINSBaseElement;
	}

	/**
	 * Cria o elementos de total para ser anexado a NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @return node com o elemento total da NFe.
	 */
	private Element criarTotalElement(Document doc) {
		Element totalElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "total");
		Element icmsTotElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "ICMSTot");
		totalElement.appendChild(icmsTotElement);

		// vBC element
		Element vBC = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBC");
		vBC.appendChild(doc.createTextNode(total.getvBC()));
		icmsTotElement.appendChild(vBC);

		// vICMS element
		Element vICMS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMS");
		vICMS.appendChild(doc.createTextNode(total.getvICMS()));
		icmsTotElement.appendChild(vICMS);

		// vICMSDeson element
		Element vICMSDeson = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vICMSDeson");
		vICMSDeson.appendChild(doc.createTextNode(total.getvICMSDeson()));
		icmsTotElement.appendChild(vICMSDeson);

		// vFCPUFDest element
		Element vFCPUFDest = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFCPUFDest");
		vFCPUFDest.appendChild(doc.createTextNode("0.00"));
		icmsTotElement.appendChild(vFCPUFDest);

		if (versao == Versao.V400) {
			// vFCP element
			Element vFCP = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFCP");
			vFCP.appendChild(doc.createTextNode(total.getvFCP()));
			icmsTotElement.appendChild(vFCP);
		}

		// vBCST element
		Element vBCST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vBCST");
		vBCST.appendChild(doc.createTextNode(total.getvBCST()));
		icmsTotElement.appendChild(vBCST);

		// vST element
		Element vST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vST");
		vST.appendChild(doc.createTextNode(total.getvST()));
		icmsTotElement.appendChild(vST);

		if (versao == Versao.V400) {
			// vFCPST element
			Element vFCPST = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFCPST");
			vFCPST.appendChild(doc.createTextNode(total.getvFCPST()));
			icmsTotElement.appendChild(vFCPST);
		}

		if (versao == Versao.V400) {
			// vFCPSTRet element
			Element vFCPSTRet = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFCPSTRet");
			vFCPSTRet.appendChild(doc.createTextNode(total.getvFCPSTRet()));
			icmsTotElement.appendChild(vFCPSTRet);
		}

		// vProd element
		Element vProd = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vProd");
		vProd.appendChild(doc.createTextNode(total.getvProd()));
		icmsTotElement.appendChild(vProd);

		// vFrete element
		Element vFrete = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vFrete");
		vFrete.appendChild(doc.createTextNode(total.getvFrete()));
		icmsTotElement.appendChild(vFrete);

		// vSeg element
		Element vSeg = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vSeg");
		vSeg.appendChild(doc.createTextNode(total.getvSeg()));
		icmsTotElement.appendChild(vSeg);

		// vDesc element
		Element vDesc = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vDesc");
		vDesc.appendChild(doc.createTextNode(total.getvDesc()));
		icmsTotElement.appendChild(vDesc);

		// vII element
		Element vII = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vII");
		vII.appendChild(doc.createTextNode(total.getvII()));
		icmsTotElement.appendChild(vII);

		// vIPI element
		Element vIPI = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vIPI");
		vIPI.appendChild(doc.createTextNode(total.getvIPI()));
		icmsTotElement.appendChild(vIPI);

		if (versao == Versao.V400) {
			// vIPIDevol element
			Element vIPIDevol = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vIPIDevol");
			vIPIDevol.appendChild(doc.createTextNode(total.getvIPIDevol()));
			icmsTotElement.appendChild(vIPIDevol);
		}

		// vPIS element
		Element vPIS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vPIS");
		vPIS.appendChild(doc.createTextNode(total.getvPIS()));
		icmsTotElement.appendChild(vPIS);

		// vCOFINS element
		Element vCOFINS = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vCOFINS");
		vCOFINS.appendChild(doc.createTextNode(total.getvCOFINS()));
		icmsTotElement.appendChild(vCOFINS);

		// vOutro element
		Element vOutro = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vOutro");
		vOutro.appendChild(doc.createTextNode(total.getvOutros()));
		icmsTotElement.appendChild(vOutro);

		// vNF element
		Element vNF = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vNF");
		vNF.appendChild(doc.createTextNode(total.getvNF()));
		icmsTotElement.appendChild(vNF);

		return totalElement;
	}

	/**
	 * Cria o elementos de transp para ser anexado a NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @return node com o elemento transp da NFe.
	 */
	private Element criarTranspElement(Document doc) {

		Element transpElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "transp");

		// modFrete element
		Element modFrete = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "modFrete");
		modFrete.appendChild(doc.createTextNode("9"));
		transpElement.appendChild(modFrete);

		return transpElement;
	}

	/**
	 * Cria e anexa todos os elementos de Grupo de Formas de Pagamento da NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param rootElement
	 *            node com a raiz do xml
	 */
	private void criarEAnexarPagsElement(Document doc, Element rootElement) {
		Element pagElement = doc.createElement("pag");

		double troco = 0;
		for (Pag pag : pags) {
			Element detPagElement = null;
			if (versao == Versao.V400) {
				detPagElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "detPag");
				pagElement.appendChild(detPagElement);
			} else if (versao == Versao.V310) {
				detPagElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "pag");
				rootElement.appendChild(detPagElement);
			}

			// tPag element
			Element tPag = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tPag");
			tPag.appendChild(doc.createTextNode(pag.gettPag()));
			detPagElement.appendChild(tPag);

			// vPag element
			Element vPag = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vPag");
			vPag.appendChild(doc.createTextNode(pag.getvPag()));
			detPagElement.appendChild(vPag);

			if (pag.gettPag().equals("03") || pag.gettPag().equals("04")) {
				Element cardElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "card");

				// tpIntegra element
				Element tpIntegra = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tpIntegra");
				tpIntegra.appendChild(doc.createTextNode(pag.getTpIntegra()));
				cardElement.appendChild(tpIntegra);

				// CNPJ element
				if (pag.getcNPJ() != null) {
					Element cNPJ = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "CNPJ");
					cNPJ.appendChild(doc.createTextNode(pag.getcNPJ()));
					cardElement.appendChild(cNPJ);
				}

				// tBand element
				if (pag.gettBand() != null) {
					Element tBand = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "tBand");
					tBand.appendChild(doc.createTextNode(pag.gettBand()));
					cardElement.appendChild(tBand);
				}

				// cAut element
				if (pag.getcAut() != null) {
					Element cAut = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "cAut");
					cAut.appendChild(doc.createTextNode(pag.getcAut()));
					cardElement.appendChild(cAut);
				}

				detPagElement.appendChild(cardElement);
			}
			if (versao == Versao.V400) {
				// vTroco element
				if (pag.getvTroco() != null) {
					troco += Double.parseDouble(pag.getvTroco());
				}
			}
		}
		if (versao == Versao.V400) {
			if (troco > 0) {
				DecimalFormat df = new DecimalFormat("############0.00");
				Element vTroco = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "vTroco");
				vTroco.appendChild(doc.createTextNode(df.format(troco).replaceAll(",", ".")));
				pagElement.appendChild(vTroco);
			}

			rootElement.appendChild(pagElement);
		}
	}

	/**
	 * Cria o elementos do Grupo de Informações Adicionais para ser anexado a
	 * NF-e
	 * 
	 * @param doc
	 *            Documento principal
	 * @param rootElement
	 *            node com a raiz do xml
	 */
	private void criarEAnexarinfAdicElement(Document doc, Element rootElement) {
		if (infAdic.getInfCpl() != null || infAdic.getInfAdFisco() != null) {
			Element infAdicElement = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "infAdic");

			// infAdFisco element
			if (infAdic.getInfAdFisco() != null) {
				Element infAdFisco = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "infAdFisco");
				infAdFisco.appendChild(doc.createTextNode(infAdic.getInfAdFisco()));
				infAdicElement.appendChild(infAdFisco);
			}

			// infCpl element
			if (infAdic.getInfCpl() != null) {
				Element infCpl = doc.createElementNS("http://www.portalfiscal.inf.br/nfe", "infCpl");
				infCpl.appendChild(doc.createTextNode(infAdic.getInfCpl()));
				infAdicElement.appendChild(infCpl);
			}

			rootElement.appendChild(infAdicElement);
		}
	}

	private boolean checkGrupoOpcionalModBC90(String cST, Imposto imposto) {
		if (!cST.equals("90")) {
			return true;
		} else {
			return imposto.getModBC() != null && imposto.getpICMS() != null;
		}
	}

	private boolean checkGrupoOpcionalModBCST90(String cST, Imposto imposto) {
		if (!cST.equals("90")) {
			return true;
		} else {
			return imposto.getModBCST() != null;
		}
	}

	private boolean checkGrupoOpcionalModBC900(String cSOSN, Imposto imposto) {
		if (!cSOSN.equals("900")) {
			return true;
		} else {
			return imposto.getModBC() != null;
		}
	}

	private boolean checkGrupoOpcionalModBCST900(String cSOSN, Imposto imposto) {
		if (!cSOSN.equals("900")) {
			return true;
		} else {
			return imposto.getModBCST() != null;
		}
	}

	private boolean checkGrupoOpcionalPCredSN900(String cSOSN, Imposto imposto) {
		if (!cSOSN.equals("900")) {
			return true;
		} else {
			return imposto.getpCredSN() != null;
		}
	}

	/**
	 * Identificação do Destinatário da NF-e
	 * 
	 * @param dest
	 *            Destinatário da NF-e
	 */
	public void setDest(Dest dest) {
		this.dest = dest;
	}

	private enum EnderTipo {
		EMIT("enderEmit"), DEST("enderDest");

		private String enderTipo;

		private EnderTipo(String enderTipo) {
			this.enderTipo = enderTipo;
		}

		public String getEnderTipo() {
			return enderTipo;
		}
	}

	@Override
	public Estados getUF() {
		return ide.getcUF();
	}

	@Override
	public TpEmis getTpEmis() {
		return ide.getTpEmis();
	}

	@Override
	public TpAmb getTpAmb() {
		return ide.getTpAmb();
	}

	protected String getIndSinc() {
		return indSinc ? "1" : "0";
	}

	@Override
	public Versao getVersao() {
		return this.versao;
	}

	@Override
	public Servico getServico() {
		return Servico.AUTORIZACAO;
	}

	@Override
	public ModeloNF getModeloNF() {
		return ide.getMod();
	}
}