package brunonm.nfce.utils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;
import org.w3c.dom.Document;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.Servico;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;

/**
 * Classe responsável por criar os envelopes Soaps para envio.
 * 
 * @author Bruno Melo
 *
 */
class CriadorEnvelopeSoap {

	private Servico servico;
	private Estados cUF;
	private Versao versao;
	private CriadorXml criadorXml;

	/**
	 * Construtor para criar um envelope Soap
	 * 
	 * @param criadorXml
	 *            Criador de xml com as informações do xml.
	 */
	protected CriadorEnvelopeSoap(CriadorXml criadorXml) {
		this.criadorXml = criadorXml;
		this.cUF = criadorXml.getUF();
		this.versao = criadorXml.getVersao();
		this.servico = criadorXml.getServico();
	}

	/**
	 * Cria um requisição Soap com as informações completa já pronto para envio
	 * 
	 * @return Mensagem SOAP para envio.
	 * @throws VersaoNFCeErradaException
	 *             é lançado quando a versão da nota está errada
	 */
	protected SoapSerializationEnvelope createSOAPRequest() throws VersaoNFCeErradaException {
		SoapObject request = new SoapObject(WebServerHelper.pegarXmlns(servico), "nfeDadosMsg");
		request.addSoapObject(getBody());

		SoapSerializationEnvelope envelope = new CustomSoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.implicitTypes = true;
		envelope.setAddAdornments(false);
		envelope.setOutputSoapObject(request);

		// set Header
		envelope.headerOut = new Element[1];
		envelope.headerOut[0] = getHeader();

		return envelope;
	}

	/**
	 * Cria o corpo do envelope SOAP.
	 * 
	 * @return SoapObject com o Body;
	 * @throws VersaoNFCeErradaException
	 *             Lançado quando existe uma discrepancia nas versões.
	 */
	private SoapObject getBody() throws VersaoNFCeErradaException {
		SoapObject criarSoapObject = criadorXml.criarSoapObject();
		return criarSoapObject;
	}

	/**
	 * Cria o Header do envelope SOAP.
	 * 
	 * @return Header element.
	 */
	private Element getHeader() {
		String namespace = WebServerHelper.pegarXmlns(servico);

		// Create Element and add Child
		Element header = new Element().createElement(namespace, "nfeCabecMsg");
		header.setPrefix("", namespace);

		// cUF element
		Element cUF = header.createElement(namespace, "cUF");
		cUF.addChild(Node.TEXT, String.valueOf(this.cUF.getCodigoIbge()));
		header.addChild(Node.ELEMENT, cUF);
		// versaoDados element
		Element versaoDados = header.createElement(namespace, "versaoDados");
		versaoDados.addChild(Node.TEXT, versao.getVersao());
		header.addChild(Node.ELEMENT, versaoDados);

		return header;
	}
}
