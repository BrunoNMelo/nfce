package brunonm.nfce.utils;

import java.io.FileInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import brunonm.nfce.modelo.InfoCert;
import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;

class AssinadorXml {

	private static final String C14N = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
	private InfoCert infoCert;

	protected AssinadorXml(InfoCert infocert) {
		this.infoCert = infocert;
	}

	protected Document AssinarXml(String id, Document doc) throws Exception {

		// Create a DOM XMLSignatureFactory that will be used to
		// generate the enveloped signature.
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Instantiate the document to be signed.
//		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//		dbf.setNamespaceAware(true);
//		Document doc = dbf.newDocumentBuilder().parse(new FileInputStream(doc));

		// Create a Reference to the enveloped document (in this case,
		// you are signing the whole document, so a URI of "" signifies
		// that, and also specify the SHA1 digest algorithm and
		// the ENVELOPED Transform.
		List<Reference> refList = criarReferenciaDoEnveloredDocument(fac, doc);

		// Create the SignedInfo.
		SignedInfo si = fac.newSignedInfo(
				fac.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec) null),
				fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), refList);

		// Load the KeyStore and get the signing key and certificate.
		KeyStore.PrivateKeyEntry keyEntry;
		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(new FileInputStream(infoCert.getCaminhoCert()), infoCert.getSenhaCert().toCharArray());

		keyEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(ks.aliases().nextElement(),
				new KeyStore.PasswordProtection(infoCert.getSenhaCert().toCharArray()));
		X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

		// Create the KeyInfo containing the X509Data.
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		List<X509Certificate> x509Content = new ArrayList<>();
		x509Content.add(cert);
		X509Data xd = kif.newX509Data(x509Content);
		KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));

		// Create a DOMSignContext and specify the RSA PrivateKey and
		// location of the resulting XMLSignature's parent element.
		DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), doc.getDocumentElement());

		// Create the XMLSignature, but don't sign it yet.
		XMLSignature signature = fac.newXMLSignature(si, ki);

		// Marshal, generate, and sign the enveloped signature.
		signature.sign(dsc);

		// Find Signature element.
		NodeList nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if (nl.getLength() == 0) {
			throw new Exception("Cannot find Signature element");
		}

		validarXml(fac, nl, ks, signature);
		
		return doc;
	}

	private void validarXml(XMLSignatureFactory fac, NodeList nl, KeyStore ks, XMLSignature signature)
			throws MarshalException, XMLSignatureException {
		// Create a DOMValidateContext and specify a KeySelector
		// and document context.
		DOMValidateContext valContext = new DOMValidateContext(new X509KeySelector(), nl.item(0));

		// Unmarshal the XMLSignature.
		XMLSignature xmlSignature = fac.unmarshalXMLSignature(valContext);

		// Validate the XMLSignature.
		boolean coreValidity = xmlSignature.validate(valContext);

		// Check core validation status.
		if (coreValidity == false) {
			System.err.println("Signature failed core validation");
			boolean sv = signature.getSignatureValue().validate(valContext);
			System.out.println("signature validation status: " + sv);
			if (sv == false) {
				// Check the validation status of each Reference.
				Iterator<?> iterator = signature.getSignedInfo().getReferences().iterator();
				for (int j = 0; iterator.hasNext(); j++) {
					boolean refValid = ((Reference) iterator.next()).validate(valContext);
					System.out.println("ref[" + j + "] validity status: " + refValid);
				}
			}
		} else {
			System.out.println("Signature passed core validation");
		}
	}

	private List<Reference> criarReferenciaDoEnveloredDocument(XMLSignatureFactory fac, Document doc)
			throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
		List<Transform> transformList = new ArrayList<>();

		TransformParameterSpec transformSpec = null;
		Transform envelopedTransform = fac.newTransform(Transform.ENVELOPED, transformSpec);
		Transform c14NTransform = fac.newTransform(C14N, transformSpec);

		transformList.add(envelopedTransform);
		transformList.add(c14NTransform);

		NodeList elements = doc.getElementsByTagName("infNFe");
		Element el = (Element) elements.item(0);
		String idRef = el.getAttribute("Id");
		el.setIdAttribute("Id", true);

		Reference ref = fac.newReference("#".concat(idRef), fac.newDigestMethod(DigestMethod.SHA1, null), transformList,
				null, null);

		List<Reference> refList = new ArrayList<>();
		refList.add(ref);
		return refList;
	}
	
	
	protected class X509KeySelector extends KeySelector {
	    public KeySelectorResult select(KeyInfo keyInfo,
	                                    KeySelector.Purpose purpose,
	                                    AlgorithmMethod method,
	                                    XMLCryptoContext context)
	        throws KeySelectorException {
	        Iterator<?> ki = keyInfo.getContent().iterator();
	        while (ki.hasNext()) {
	            XMLStructure info = (XMLStructure) ki.next();
	            if (!(info instanceof X509Data))
	                continue;
	            X509Data x509Data = (X509Data) info;
	            Iterator<?> xi = x509Data.getContent().iterator();
	            while (xi.hasNext()) {
	                Object o = xi.next();
	                if (!(o instanceof X509Certificate))
	                    continue;
	                final PublicKey key = ((X509Certificate)o).getPublicKey();
	                // Make sure the algorithm is compatible
	                // with the method.
	                if (algEquals(method.getAlgorithm(), key.getAlgorithm())) {
	                    return new KeySelectorResult() {
	                        public Key getKey() { return key; }
	                    };
	                }
	            }
	        }
	        throw new KeySelectorException("No key found!");
	    }

	    private boolean algEquals(String algURI, String algName) {
	        if ((algName.equalsIgnoreCase("DSA") &&
	            algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) ||
	            (algName.equalsIgnoreCase("RSA") &&
	            algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1))) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}
}


