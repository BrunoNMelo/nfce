package brunonm.nfce.utils;

import org.ksoap2.serialization.SoapObject;

public class RespostaConsultaStatus {

	private int tpAmb;
	private String verAplic;
	private int cStat;
	private String xMotivo;
	private String cUF;
	private String dhRecbto;
	private String tMed;
	private String dhRetorno;
	private String xObs;

	protected RespostaConsultaStatus(SoapObject consultaStatusResponse) {
		this.tpAmb = Integer.parseInt(consultaStatusResponse.getProperty(0).toString());
		this.verAplic = consultaStatusResponse.getProperty(1).toString();
		this.cStat = Integer.parseInt(consultaStatusResponse.getProperty(2).toString());
		this.xMotivo = consultaStatusResponse.getProperty(3).toString();
		this.cUF = consultaStatusResponse.getProperty(4).toString();
		this.dhRecbto = consultaStatusResponse.getProperty(5).toString();
		if (consultaStatusResponse.getPropertyCount() > 6)
			this.tMed = consultaStatusResponse.getProperty(6).toString();
		if (consultaStatusResponse.getPropertyCount() > 7)
			this.dhRetorno = consultaStatusResponse.getProperty(7).toString();
		if (consultaStatusResponse.getPropertyCount() > 8)
			this.xObs = consultaStatusResponse.getProperty(8).toString();
	}

	public int getTpAmb() {
		return tpAmb;
	}

	public String getVerAplic() {
		return verAplic;
	}

	public int getcStat() {
		return cStat;
	}

	public String getxMotivo() {
		return xMotivo;
	}

	public String getcUF() {
		return cUF;
	}

	public String getDhRecbto() {
		return dhRecbto;
	}

	public String gettMed() {
		return tMed;
	}

	public String getDhRetorno() {
		return dhRetorno;
	}

	public String getxObs() {
		return xObs;
	}

}
