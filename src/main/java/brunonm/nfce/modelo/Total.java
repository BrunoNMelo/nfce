package brunonm.nfce.modelo;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Classe responsável pelo grupo total
 * 
 * @author Bruno Melo
 *
 */
public class Total {
	// total/IMCSTot
	private String vBC;
	private String vICMS;
	private String vICMSDeson;
	private String vFCP;
	private String vBCST;
	private String vST;
	private String vFCPST;
	private String vFCPSTRet;
	private String vProd;
	private String vfrete;
	private String vSeg;
	private String vDesc;
	private String vII;
	private String vIPI;
	private String vIPIDevol;
	private String vPIS;
	private String vCOFINS;
	private String vOutros;
	private String vNF;

	private final DecimalFormat df;

	/**
	 * Construtor com informações obrigatórias para total.
	 * 
	 * @param dets
	 *            lista com os det da nota.
	 */
	public Total(List<Det> dets) {
		df = new DecimalFormat("############0.00");

		double vBC = 0;
		double vICMS = 0;
		double vFCP = 0;
		double vBCST = 0;
		double vST = 0;
		double vFCPST = 0;
		double vFCPSTRet = 0;
		double vProd = 0;
		double vFrete = 0;
		double vSeg = 0;
		double vDesc = 0;
		double vPIS = 0;
		double vCOFINS = 0;
		double vOutro = 0;
		for (Det det : dets) {
			// vBC
			if (det.getImposto().getvBC() != null)
				vBC += Double.parseDouble(det.getImposto().getvBC());
			
			// vICMS
			if (det.getImposto().getvICMS() != null)
				vICMS += Double.parseDouble(det.getImposto().getvICMS());
			
			// vFCP
			if (det.getImposto().getvFCP() != null)
				vFCP += Double.parseDouble(det.getImposto().getvFCP());
			
			// vBCST
			if (det.getImposto().getvBCST() != null)
				vBCST += Double.parseDouble(det.getImposto().getvBCST());
			
			// vST
			if (det.getImposto().getvICMSST() != null)
				vST += Double.parseDouble(det.getImposto().getvICMSST());
			
//			// vFCPST
//			if (det.getImposto().getvFCPST() != null)
//				vFCPST += Double.parseDouble(det.getImposto().getvFCPST());
//			
//			// vFCPSTRet
//			if (det.getImposto().getvFCPSTRet() != null)
//				vFCPST += Double.parseDouble(det.getImposto().getvFCPSTRet());
			
			// vProd
			vProd += Double.parseDouble(det.getvProd());

			// vFrete
			if (det.getvFrete() != null)
				vFrete += Double.parseDouble(det.getvFrete());
			
			// vSeg
			if (det.getvSeg() != null)
				vSeg += Double.parseDouble(det.getvSeg());
			
			// vDesc
			if (det.getvDesc() != null)
				vDesc += Double.parseDouble(det.getvDesc());
			
			// vPIS
			if (det.getImposto().getvPIS() != null)
				vPIS += Double.parseDouble(det.getImposto().getvPIS());
			
			// vCOFINS
			if (det.getImposto().getvCOFINS() != null)
				vCOFINS += Double.parseDouble(det.getImposto().getvCOFINS());

			// vOutro
			if (det.getvOutro() != null)
				vOutro += Double.parseDouble(det.getvOutro());
		}
		setvProd(df.format(vProd).replaceAll(",", "."));
		setvDesc(df.format(vDesc).replaceAll(",", "."));
		setvBC(df.format(vBC).replaceAll(",", "."));
		setvICMS(df.format(vICMS).replaceAll(",", "."));
		setvFCP(df.format(vFCP).replaceAll(",", "."));
		setvBCST(df.format(vBCST).replaceAll(",", "."));
		setvFCPST(df.format(vFCPST).replaceAll(",", "."));
		setvFCPSTRet(df.format(vFCPSTRet).replaceAll(",", "."));		
		setvST(df.format(vST).replaceAll(",", "."));
		setVfrete(df.format(vFrete).replaceAll(",", "."));
		setvSeg(df.format(vSeg).replaceAll(",", "."));
		setvPIS(df.format(vPIS).replaceAll(",", "."));
		setvCOFINS(df.format(vCOFINS).replaceAll(",", "."));
		setvOutros(df.format(vOutro).replaceAll(",", "."));
		setvNF(df.format(vProd - vDesc + vST + vSeg + vFrete + vOutro).replaceAll(",", "."));
	}

	/**
	 * @param vBC
	 *            BC do ICMS
	 */
	private void setvBC(String vBC) {
		if (!vBC.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vBC está errado.");
		this.vBC = vBC;
	}

	/**
	 * @param vICMS
	 *            Valor Total do ICMS
	 */
	private void setvICMS(String vICMS) {
		if (!vICMS.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vICMS está errado.");
		this.vICMS = vICMS;
	}

	/**
	 * @param vICMSDeson
	 *            Valor Total do ICMS desonerado
	 */
	private void setvICMSDeson(String vICMSDeson) {
		if (!vICMSDeson.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vICMSDeson está errado.");
		this.vICMSDeson = vICMSDeson;
	}

	/**
	 * @param vFCP
	 *            Valor Total do FCP (Fundo de Combate à Pobreza).
	 */
	private void setvFCP(String vFCP) {
		if (!vFCP.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vFCP está errado.");
		this.vFCP = vFCP;
	}

	/**
	 * @param vBCST
	 *            BC do ICMS ST
	 */
	private void setvBCST(String vBCST) {
		if (!vBCST.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vBCST está errado.");
		this.vBCST = vBCST;
	}

	/**
	 * @param vST
	 *            Valor Total do ICMS ST
	 */
	private void setvST(String vST) {
		if (!vST.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vST está errado.");
		this.vST = vST;
	}

	/**
	 * Valor Total do FCP (Fundo de Combate à Pobreza) retido por substituição
	 * tributária.
	 * 
	 * @param vFCPST
	 *            Valor Total do FCP (Fundo de Combate à Pobreza)
	 */
	private void setvFCPST(String vFCPST) {
		if (!vFCPST.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vFCPST está errado.");
		this.vFCPST = vFCPST;
	}

	/**
	 * Valor Total do FCP (Fundo de Combate à Pobreza) retido anteriormente por
	 * substituição tributária.
	 * 
	 * @param vFCPSTRet
	 *            Valor Total do FCP (Fundo de Combate à Pobreza)
	 */
	private void setvFCPSTRet(String vFCPSTRet) {
		if (!vFCPSTRet.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vFCPSTRet está errado.");
		this.vFCPSTRet = vFCPSTRet;
	}

	/**
	 * @param vProd
	 *            Valor Total dos produtos e serviços
	 */
	private void setvProd(String vProd) {
		if (!vProd.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vProd está errado.");
		this.vProd = vProd;
	}

	/**
	 * @param vfrete
	 *            Valor Total do Frete
	 */
	private void setVfrete(String vfrete) {
		if (!vfrete.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vfrete está errado.");
		this.vfrete = vfrete;
	}

	/**
	 * @param vSeg
	 *            Valor Total do Seguro
	 */
	private void setvSeg(String vSeg) {
		if (!vSeg.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vSeg está errado.");
		this.vSeg = vSeg;
	}

	/**
	 * @param vDesc
	 *            Valor Total do Desconto
	 */
	private void setvDesc(String vDesc) {
		if (!vDesc.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vDesc está errado.");
		this.vDesc = vDesc;
	}

	/**
	 * @param vII
	 *            Valor Total do II
	 */
	private void setvII(String vII) {
		if (!vII.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vII está errado.");
		this.vII = vII;
	}

	/**
	 * @param vIPI
	 *            Valor Total do IPI
	 */
	private void setvIPI(String vIPI) {
		if (!vIPI.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vIPI está errado.");
		this.vIPI = vIPI;
	}

	/**
	 * @param vPIS
	 *            Valor do PIS
	 */
	private void setvPIS(String vPIS) {
		if (!vPIS.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vPIS está errado.");
		this.vPIS = vPIS;
	}

	/**
	 * @param vIPIDevol
	 *            Valor Total do IPI devolvido.
	 */
	private void setvIPIDevol(String vIPIDevol) {
		if (!vIPIDevol.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vIPIDevol está errado.");
		this.vIPIDevol = vIPIDevol;
	}

	/**
	 * @param vCOFINS
	 *            Valor do COFINS
	 */
	private void setvCOFINS(String vCOFINS) {
		if (!vCOFINS.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vCOFINS está errado.");
		this.vCOFINS = vCOFINS;
	}

	/**
	 * @param vOutros
	 *            Outras Despesas acessórias
	 */
	private void setvOutros(String vOutros) {
		if (!vOutros.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vOutros está errado.");
		this.vOutros = vOutros;
	}

	/**
	 * @param vNF
	 *            Valor Total da NF-e
	 */
	private void setvNF(String vNF) {
		if (!vNF.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vNF está errado.");
		this.vNF = vNF;
	}

	/**
	 * @return BC do ICMS
	 */
	public String getvBC() {
		return vBC == null ? "0.00" : vBC;
	}

	/**
	 * @return Valor Total do ICMS
	 */
	public String getvICMS() {
		return vICMS == null ? "0.00" : vICMS;
	}

	/**
	 * @return Valor Total do ICMS desonerado
	 */
	public String getvICMSDeson() {
		return vICMSDeson == null ? "0.00" : vICMSDeson;
	}

	/**
	 * @return Valor Total do FCP (Fundo de Combate à Pobreza).
	 */
	public String getvFCP() {
		return vFCP == null ? "0.00" : vFCP;
	}

	/**
	 * @return BC do ICMS ST
	 */
	public String getvBCST() {
		return vBCST == null ? "0.00" : vBCST;
	}

	/**
	 * @return Valor Total do ICMS ST
	 */
	public String getvST() {
		return vST == null ? "0.00" : vST;
	}

	/**
	 * @return Valor Total do FCP (Fundo de Combate à Pobreza)
	 */
	public String getvFCPST() {
		return vFCPST == null ? "0.00" : vFCPST;
	}

	/**
	 * @return Valor Total do FCP (Fundo de Combate à Pobreza)
	 */
	public String getvFCPSTRet() {
		return vFCPSTRet == null ? "0.00" : vFCPSTRet;
	}

	/**
	 * @return Valor Total dos produtos e serviços
	 */
	public String getvProd() {
		return vProd == null ? "0.00" : vProd;
	}

	/**
	 * @return Valor Total do Frete
	 */
	public String getvFrete() {
		return vfrete == null ? "0.00" : vfrete;
	}

	/**
	 * @return Valor Total do Seguro
	 */
	public String getvSeg() {
		return vSeg == null ? "0.00" : vSeg;
	}

	/**
	 * @return Valor Total do Desconto
	 */
	public String getvDesc() {
		return vDesc == null ? "0.00" : vDesc;
	}

	/**
	 * @return Valor Total do II
	 */
	public String getvII() {
		return vII == null ? "0.00" : vII;
	}

	/**
	 * @return Valor Total do IPI
	 */
	public String getvIPI() {
		return vIPI == null ? "0.00" : vIPI;
	}

	/**
	 * @return Valor do PIS
	 */
	public String getvPIS() {
		return vPIS == null ? "0.00" : vPIS;
	}

	/**
	 * @return Valor do COFINS
	 */
	public String getvCOFINS() {
		return vCOFINS == null ? "0.00" : vCOFINS;
	}

	/**
	 * @return Outras Despesas acessórias
	 */
	public String getvOutros() {
		return vOutros == null ? "0.00" : vOutros;
	}

	/**
	 * @return Valor Total da NF-e
	 */
	public String getvNF() {
		return vNF == null ? "0.00" : vNF;
	}

	/**
	 * @return Valor Total do IPI devolvido.
	 */
	public String getvIPIDevol() {
		return vIPIDevol == null ? "0.00" : vIPIDevol;
	}
}