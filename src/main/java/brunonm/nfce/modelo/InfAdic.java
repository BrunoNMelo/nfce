package brunonm.nfce.modelo;

/**
 * Classe responsável pelo grupo infAdic
 * @author Bruno Melo
 *
 */
public class InfAdic {
	private String infCpl;
	private String infAdFisco;
	
	public static class Builder {
		private String infCpl;
		private String infAdFisco;
		
		/**
		 * @param infCpl Informações Complementares de interesse do Contribuinte
		 * @return Builder
		 */
		public Builder comInfCpl(String infCpl) {
			if (infCpl.length() < 1 || infCpl.length() > 5000)
				throw new IllegalArgumentException("O tamanho da infCpl deve ser no mínimo 1 e no máximo 5000.");
			this.infCpl = infCpl;
			return this;
		}
		
		/**
		 * @param infAdFisco Informações Adicionais de Interesse do Fisco
		 * @return Builder
		 */
		public Builder comInfAdFisco(String infAdFisco) {
			if (infAdFisco.length() < 1 || infAdFisco.length() > 2000)
				throw new IllegalArgumentException("O tamanho da infAdFisco deve ser no mínimo 1 e no máximo 2000.");
			this.infAdFisco = infAdFisco;
			return this;
		}
		
		public InfAdic build(){
			return new InfAdic(this);
		}
		
	}

	private InfAdic(Builder builder) {
		infCpl = builder.infCpl;
		infAdFisco = builder.infAdFisco;
	}

	/**
	 * @return Informações Complementares de interesse do Contribuinte
	 */
	public String getInfCpl() {
		return infCpl == null ? null : infCpl;
	}

	/**
	 * @return Informações Adicionais de Interesse do Fisco
	 */
	public String getInfAdFisco() {
		return infAdFisco == null ? null : infAdFisco;
	}
	
}
