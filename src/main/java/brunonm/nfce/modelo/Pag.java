package brunonm.nfce.modelo;

import brunonm.nfce.enums.TBand;
import brunonm.nfce.enums.TPag;
import brunonm.nfce.enums.TpIntegra;

/**
 * Classe responsável pelo grupo de pag
 * 
 * @author Bruno Melo
 *
 */
public class Pag {
	private TPag tPag;
	private String vPag;
	// pag/card
	private TpIntegra tpIntegra;
	private String cNPJ;
	private TBand tBand;
	private String cAut;
	private String vTroco;

	public static class Builder {
		private TPag tPag;
		private String vPag;
		// pag/card
		private TpIntegra tpIntegra;
		private String cNPJ;
		private TBand tBand;
		private String cAut;
		private String vTroco;

		/**
		 * Construtor com informações obrigatórias especifico para pagamentos
		 * sem ser com cartões.
		 * 
		 * @param tPag
		 *            Forma de pagamento
		 * @param vPag
		 *            Valor do Pagamento
		 */
		public Builder(TPag tPag, String vPag) {
			settPag(tPag);
			setvPag(vPag);
			if (tPag == TPag.CARTAO_CREDIDO || tPag == TPag.CARTAO_DEBITO)
				throw new IllegalArgumentException("O tipo de pagamento não pode ser cartão para esse construtor");
		}

		/**
		 * Construtor com informações obrigatórias especifico para pagamentos
		 * com cartões.
		 * 
		 * @param tPag
		 *            Forma de pagamento
		 * @param vPag
		 *            Valor do Pagamento
		 * @param tpIntegra
		 *            Tipo de integração
		 */
		public Builder(TPag tPag, String vPag, TpIntegra tpIntegra) {
			settPag(tPag);
			setvPag(vPag);
			setTpIntegra(tpIntegra);
			if (tPag != TPag.CARTAO_CREDIDO && tPag != TPag.CARTAO_DEBITO)
				throw new IllegalArgumentException("O tipo de pagamento deve ser cartão para esse construtor");
		}

		/**
		 * @param tPag
		 *            Forma de pagamento
		 */
		private void settPag(TPag tPag) {
			this.tPag = tPag;
		}

		/**
		 * @param vPag
		 *            Valor do Pagamento
		 */
		private void setvPag(String vPag) {
			if (!vPag.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vPag está errado.");
			this.vPag = vPag;
		}

		/**
		 * @param cNPJ
		 *            CNPJ da Credenciadora de cartão de crédito e/ou débito
		 * @return Builder
		 */
		public Builder comCNPJ(String cNPJ) {
			if (String.valueOf(cNPJ).length() != 14)
				throw new IllegalArgumentException("O tamanho do cNPJ deve ser exatamente 14");
			this.cNPJ = cNPJ;
			return this;
		}

		/**
		 * @param tBand
		 *            Bandeira da operadora de cartão de crédito e/ou débito
		 * @return Builder
		 */
		public Builder comtBand(TBand tBand) {
			this.tBand = tBand;
			return this;
		}

		/**
		 * @param cAut
		 *            Número de autorização da operação cartão de crédito
		 *            e/ou débito
		 * @return Builder
		 */
		public Builder comcAut(String cAut) {
			if (String.valueOf(cAut).length() < 1 || String.valueOf(cAut).length() > 20)
				throw new IllegalArgumentException("O tamanho da cAut deve ser no mínimo 1 e no máximo 20.");
			this.cAut = cAut;
			return this;
		}

		/**
		 * @param vTroco
		 *            Valor do Troco (opcional)
		 * @return Builder
		 */
		public Builder comvTroco(String vTroco) {
			if (!vTroco.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vTroco está errado.");
			this.vTroco = vTroco;
			return this;
		}

		/**
		 * Tipo de Integração do processo de pagamento com o sistema de
		 * automação da empresa/ 1 - Pagamento integrado com o sistema de
		 * automação da empresa Ex. equipamento TEF , Comercio Eletronico. 2 -
		 * Pagamento não integrado com o sistema de automação da empresa Ex:
		 * equipamento POS
		 * 
		 * @param tpIntegra
		 *            Tipo de Integração do processo de pagamento com o sistema
		 *            de automação da empresa
		 */
		private void setTpIntegra(TpIntegra tpIntegra) {
			this.tpIntegra = tpIntegra;
		}
		
		public Pag build(){
			return new Pag(this);
		}

	}

	private Pag(Builder builder) {
		tPag = builder.tPag;
		vPag = builder.vPag;
		tpIntegra = builder.tpIntegra;
		cNPJ = builder.cNPJ;
		tBand = builder.tBand;
		cAut = builder.cAut;
		vTroco = builder.vTroco;
	}

	/**
	 * @return Forma de pagamento
	 */
	public String gettPag() {
		return tPag.getTipoPag();
	}

	/**
	 * @return Valor do Pagamento
	 */
	public String getvPag() {
		return vPag;
	}

	/**
	 * @return CNPJ da Credenciadora de cartão de crédito e/ou débito
	 */
	public String getcNPJ() {
		return cNPJ == null ? null : cNPJ;
	}

	/**
	 * @return Bandeira da operadora de cartão de crédito e/ou débito
	 */
	public String gettBand() {
		return tBand == null ? null : tBand.gettBandCod();
	}

	/**
	 * @return Número de autorização da operação cartão de crédito e/ou
	 *         débito
	 */
	public String getcAut() {
		return cAut == null ? null : cAut;
	}

	/**
	 * @return Valor do Troco
	 */
	public String getvTroco() {
		return vTroco;
	}

	/**
	 * @return Tipo de Integração do processo de pagamento com o sistema de
	 *         automação da empresa
	 */
	public String getTpIntegra() {
		return tpIntegra.getTpIntegra();
	}

}
