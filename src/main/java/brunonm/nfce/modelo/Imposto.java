package brunonm.nfce.modelo;

import java.text.DecimalFormat;

import brunonm.nfce.enums.Csosn;
import brunonm.nfce.enums.Cst;
import brunonm.nfce.enums.ModBC;
import brunonm.nfce.enums.ModBCST;
import brunonm.nfce.enums.Orig;

/**
 * Classe responsável por armazenar os valores de impostos dos produtos. Para
 * ser inclusa como paramentros dentro da classe Det.
 * 
 * @author Bruno Melo
 *
 */
public class Imposto {

	// imposto
	private Cst cST;
	private String vBC;
	// imposto/ICMS
	private Orig orig;
	private Csosn cSOSN;
	private ModBC modBC;
	private String pICMS;
	private String pFCP;
	private String vFCP;
	// imposto/PIS
	// imposto/PIS/PISOutr
	private String pPis;
	// imposto/COFINS
	// imposto/COFINS/COFINSOutr
	private String pCOFINS;
	// imposto
	private String pRedBC;
	private ModBCST modBCST;
	private String pICMSST;
	private String pMVAST;
	private String pCredSN;

	public static class Builder {
		private Cst cST;
		private String vBC;
		private Orig orig;
		private Csosn cSOSN;
		private ModBC modBC;
		private String pICMS;
		private String pFCP;
		private String vFCP;
		private String pPis;
		private String pCOFINS;
		private String pRedBC;
		private ModBCST modBCST;
		private String pICMSST;
		private String pMVAST;
		private String pCredSN;

		/**
		 * Construtor para tributação de PIS e COFINS
		 * 
		 * @param cST
		 *            Código de Situação Tributária do PIS. 99 - Outras
		 *            Operações.
		 * @param orig
		 *            origem da mercadoria
		 */
		public Builder(Cst cST, Orig orig) {
			setOrig(orig);
			setcST(cST);
		}

		/**
		 * Construtor para tributação de PIS e COFINS do Simples Nacional
		 * 
		 * @param orig
		 *            origem da mercadoria
		 * @param cSOSN
		 *            Código de Situação da Operação – SIMPLES NACIONAL
		 */
		public Builder(Csosn cSOSN, Orig orig) {
			setcSOSN(cSOSN);
			setOrig(orig);
		}

		/**
		 * @param cST
		 *            Código de Situação Tributária.
		 **/
		private void setcST(Cst cST) {
			this.cST = cST;
		}

		/**
		 * @param vBC
		 *            Valor da BC do PIS
		 * @return Builder
		 **/
		public Builder comvBC(String vBC) {
			if (!vBC.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vBC está errado.");
			this.vBC = vBC;
			return this;
		}

		/**
		 * 
		 * @param orig
		 *            origem da mercadoria
		 */
		private void setOrig(Orig orig) {
			this.orig = orig;
		}

		/**
		 * 
		 * @param cSOSN
		 *            Código de Situação da Operação
		 */
		private void setcSOSN(Csosn cSOSN) {
			this.cSOSN = cSOSN;
		}

		/**
		 * @param pPIS
		 *            Alíquota do PIS (em percentual)
		 * @return Builder
		 */
		public Builder compPIS(String pPIS) {
			if (!pPIS.matches("0|0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pPIS está errado.");
			this.pPis = pPIS;
			return this;
		}

		/**
		 * @param pCOFINS
		 *            Alíquota do COFINS (em percentual)
		 * @return Builder
		 */
		public Builder compCOFINS(String pCOFINS) {
			if (!pCOFINS.matches("0|0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pCOFINS está errado.");
			this.pCOFINS = pCOFINS;
			return this;
		}

		/**
		 * @param modBC
		 *            Modalidade de determinação da BC do ICMS
		 * @return Builder
		 */
		public Builder comModBC(ModBC modBC) {
			this.modBC = modBC;
			return this;
		}

		/**
		 * @param pICMS
		 *            Alíquota do ICMS
		 * @return Builder
		 */
		public Builder compICMS(String pICMS) {
			if (!pICMS.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do pICMS está inválido.");
			this.pICMS = pICMS;
			return this;
		}

		/**
		 * @param pFCP
		 *            Percentual de ICMS relativo ao Fundo de Combate à Pobreza
		 *            (FCP).
		 * @return Builder
		 */
		public Builder compFCP(String pFCP) {
			if (!pFCP.matches("0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				this.pFCP = pFCP;
			return this;
		}

		/**
		 * @param vFCP
		 *            Valor do ICMS relativo ao Fundo de Combate à Pobreza
		 *            (FCP).
		 * @return Builder
		 */
		public Builder comvFCP(String vFCP) {
			if (!vFCP.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vFCP está inválido.");
			this.vFCP = vFCP;
			return this;
		}

		/**
		 * @param pRedBC
		 *            getpRedBC
		 * @return Builder
		 */
		public Builder compRedBC(String pRedBC) {
			if (!pRedBC.matches("0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pRedBC está inválido.");
			this.pRedBC = pRedBC;
			return this;
		}

		/**
		 * @param modBCST
		 *            Modalidade de determinação da BC do ICMS ST
		 * @return Builder
		 */
		public Builder comModBCST(ModBCST modBCST) {
			this.modBCST = modBCST;
			return this;
		}

		/**
		 * @param pICMSST
		 *            Alíquota do imposto do ICMS ST
		 * @return Builder
		 */
		public Builder compICMSST(String pICMSST) {
			if (!pICMSST.matches("0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pICMSST está inválido.");
			this.pICMSST = pICMSST;
			return this;
		}

		/**
		 * @param pMVAST
		 *            Percentual da margem de valor Adicionado do ICMS ST
		 * @return Builder
		 */
		public Builder compMVAST(String pMVAST) {
			if (!pMVAST.matches("0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pMVAST está inválido.");
			this.pMVAST = pMVAST;
			return this;
		}

		/**
		 * @param pCredSN
		 *            Alíquota aplicável de cálculo do crédito (Simples
		 *            Nacional).
		 * @return Builder
		 */
		public Builder compCredSN(String pCredSN) {
			if (!pCredSN.matches("0\\.[0-9]{2,4}|[1-9]{1}[0-9]{0,2}(\\.[0-9]{2,4})?"))
				throw new IllegalArgumentException("O formato do pCredSN está inválido.");
			this.pCredSN = pCredSN;
			return this;
		}

		public Imposto build() {
			return new Imposto(this);
		}

	}

	private Imposto(Builder builder) {
		cST = builder.cST;
		vBC = builder.vBC;
		orig = builder.orig;
		cSOSN = builder.cSOSN;
		modBC = builder.modBC;
		pICMS = builder.pICMS;
		pFCP = builder.pFCP;
		vFCP = builder.vFCP;
		pPis = builder.pPis;
		pCOFINS = builder.pCOFINS;
		pRedBC = builder.pRedBC;
		modBCST = builder.modBCST;
		pICMSST = builder.pICMSST;
		pMVAST = builder.pMVAST;
		pCredSN = builder.pCredSN;
	}
	
	/**
	 * @param vBC
	 *            Valor da BC do PIS
	 **/
	public void setvBC(String vBC) {
		if (!vBC.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
			throw new IllegalArgumentException("O formato do vBC está errado.");
		this.vBC = vBC;
	}

	/**
	 * @return Código de Situação Tributária.
	 */
	public String getcST() {
		if (cST != null) {
			return cST.getcST();
		} else {
			return null;
		}

	}

	/**
	 * @return Valor da BC do PIS
	 */
	public String getvBC() {
		return vBC;
	}

	/**
	 * @return origem da mercadoria
	 */
	public int getOrig() {
		return orig.getOrigem();
	}

	/**
	 * @return Código de Situação da Operação
	 */
	public String getcSOSN() {
		return cSOSN == null ? null : cSOSN.getCodSimples();
	}

	/**
	 * @return Alíquota do PIS (em percentual)
	 */
	public String getpPIS() {
		return pPis == null ? "0.0000" : pPis;
	}

	/**
	 * @return Valor do PIS
	 */
	public String getvPIS() {
		DecimalFormat df = new DecimalFormat("############0.00");
		if (getvBC() == null || getpPIS() == null) {
			return null;
		} else {
			double vvBC = Double.parseDouble(getvBC());
			double vvPis = (Double.parseDouble(getpPIS()) / 100);

			return df.format(vvBC * vvPis).replaceAll(",", ".");
		}
	}

	/**
	 * @return Alíquota do COFINS (em percentual)
	 */
	public String getpCOFINS() {
		return pCOFINS == null ? "0.0000" : pCOFINS;
	}

	/**
	 * @return Valor do COFINS
	 */
	public String getvCOFINS() {
		DecimalFormat df = new DecimalFormat("############0.00");
		if (getvBC() == null || getpCOFINS() == null) {
			return null;
		} else {
			double vvBC = Double.parseDouble(getvBC());
			double vpCOFINS = (Double.parseDouble(getpCOFINS()) / 100);

			return df.format(vvBC * vpCOFINS).replaceAll(",", ".");
		}
	}

	/**
	 * @return Modalidade de determinação da BC do ICMS
	 */
	public String getModBC() {
		return modBC == null ? null : modBC.getModBC();
	}

	/**
	 * @return Alíquota do ICMS
	 */
	public String getpICMS() {
		return pICMS;
	}

	/**
	 * @return Valor do ICMS
	 */
	public String getvICMS() {
		DecimalFormat df = new DecimalFormat("############0.00");
		if (getvBC() == null || getpICMS() == null) {
			return null;
		} else {
			double vvBC = Double.parseDouble(getvBC());
			double vpICMS = (Double.parseDouble(getpICMS()) / 100);

			return df.format(vvBC * vpICMS).replaceAll(",", ".");
		}
	}

	/**
	 * @return Percentual de ICMS relativo ao Fundo de Combate à Pobreza (FCP).
	 */
	public String getpFCP() {
		return pFCP;
	}

	/**
	 * @return Valor do ICMS relativo ao Fundo de Combate à Pobreza (FCP).
	 */
	public String getvFCP() {
		return vFCP;
	}

	/**
	 * @return getpRedBC
	 */
	public String getpRedBC() {
		return pRedBC;
	}

	/**
	 * @return Modalidade de determinação da BC do ICMS ST
	 */
	public String getModBCST() {
		return modBCST == null ? null : modBCST.getModBCST();
	}

	/**
	 * @return Valor da BC do ICMS ST
	 */
	public String getvBCST() {
		DecimalFormat df = new DecimalFormat("############0.00");
		double pMVAST = 0.0;
		if (getModBCST() != null) {
			if (getModBCST().equals("4") || getModBCST().equals("5")) {
				if (getpMVAST() == null || getvBC() == null)
					throw new IllegalArgumentException(
							"O valor do pMVAST e o vBC devem ser preenchido para realização do cálculo.");
				pMVAST = (Double.parseDouble(getpMVAST()) / 100);
			} else {
				if (getvBC() == null)
					throw new IllegalArgumentException("O valor vBC deve ser preenchido para realização do cálculo.");
			}
			return df.format((pMVAST * Double.parseDouble(getvBC())) + Double.parseDouble(getvBC())).replaceAll(",",
					".");
		} else {
			return null;
		}
	}

	/**
	 * @return Alíquota do imposto do ICMS ST
	 */
	public String getpICMSST() {
		return pICMSST;
	}

	/**
	 * @return Valor do ICMS ST
	 */
	public String getvICMSST() {
		DecimalFormat df = new DecimalFormat("############0.00");
		if (getpICMSST() == null)
			return null;
		return df.format((Double.parseDouble(getvBCST()) * (Double.parseDouble(getpICMSST()) / 100))
				- Double.parseDouble(getvICMS())).replaceAll(",", ".");
	}

	/**
	 * @return Percentual da margem de valor Adicionado do ICMS ST
	 */
	public String getpMVAST() {
		return pMVAST;
	}

	/**
	 * @return Alíquota aplicável de cálculo do crédito (Simples Nacional).
	 */
	public String getpCredSN() {
		return pCredSN;
	}

	/**
	 * @return Valor crédito do ICMS que pode ser aproveitado nos termos do
	 *         art. 23 da LC 123 (Simples Nacional)
	 */
	public String getvCredICMSSN() {
		DecimalFormat df = new DecimalFormat("############0.00");
		if (getpCredSN() == null)
			throw new IllegalArgumentException("O valor pCredSN deve ser preenchido para realização do cálculo.");
		return df.format(Double.parseDouble(getvBC()) * (Double.parseDouble(getpCredSN()) / 100)).replaceAll(",", ".");
	}

}