package brunonm.nfce.modelo;

/**
 * Classe responsável pelas informações do certificado.
 * @author Bruno Melo
 *
 */
public class InfoCert {

	private String caminhoCert;
	private String senhaCert;

	/**
	 * Construtor com informações especifico para assinatura do xml com o certificado.
	 * @param caminhoCert caminho onde o certificado no padrão PKCS12 está salvo.
	 * @param senhaCert a senha do certificado.
	 */
	public InfoCert(String caminhoCert, String senhaCert){
		this.caminhoCert = caminhoCert;
		this.senhaCert = senhaCert;
	}

	/**
	 * @return caminho onde o certificado no padrão PKCS12 está salvo.
	 */
	public String getCaminhoCert() {
		return caminhoCert;
	}

	/**
	 * @return a senha do certificado.
	 */
	public String getSenhaCert() {
		return senhaCert;
	}
}
