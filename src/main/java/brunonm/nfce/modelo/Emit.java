package brunonm.nfce.modelo;

import brunonm.nfce.enums.Crt;

/**
 * Classe modelo para informações da empresa que está submentendo a NFCe
 * 
 * @author Bruno Melo
 *
 */
public class Emit {
	private String cnpj;
	private String cpf;
	private String xNome;
	private String xFant;
	private Endereco endereco;
	private String ie;
	private Crt crt;
	private String iEST;
	private String iM;
	private String cNAE;
	private String cIdToken;
	private String cSC;

	public static class Builder {
		private String cnpj;
		private String cpf;
		private String xNome;
		private String xFant;
		private Endereco endereco;
		private String ie;
		private Crt crt;
		private String iEST;
		private String iM;
		private String cNAE;
		private String cIdToken;
		private String cSC;

		/**
		 * Construtor com as informações obrigatórias do Emit usando CNPJ
		 * 
		 * @param cnpjCpf
		 *            Número do CNPJ ou CPF do emitente.
		 * @param xNome
		 *            Razão Social ou Nome do emitente
		 * @param endereco
		 *            Endereço do emitente
		 * @param ie
		 *            Tipo Inscrição Estadual do Emitente
		 * @param crt
		 *            Código de Regime Tributário. *
		 * @param idCSC
		 *            Identificador do CSC – Código de Segurança do
		 *            Contribuinte no Banco de Dados da SEFAZ
		 * @param CSC
		 *            Código de Segurança do Contribuinte (antigo Token)
		 */
		public Builder(String cnpjCpf, String xNome, Endereco endereco, String ie, Crt crt, String idCSC, String CSC) {
			if (cnpjCpf.length() != 14 && cnpjCpf.length() != 11)
				throw new IllegalArgumentException("O tamanho do CNPJ deve ser exatamente 14 e do CPF deve ser 11");
			if (cnpjCpf.length() == 14)
				setCnpj(cnpjCpf);
			if (cnpjCpf.length() == 11)
				setCpf(cnpjCpf);
			setxNome(xNome);
			setEndereco(endereco);
			setIe(ie);
			setCrt(crt);
			setcIdToken(idCSC);
			setcSC(CSC);
		}

		/**
		 * 
		 * @param cnpj
		 *            Número do CNPJ do emitente.
		 */
		private void setCnpj(String cnpj) {
			if (!cnpj.matches("[0-9]{14}"))
				throw new IllegalArgumentException("O cnpj está inválido");
			this.cnpj = cnpj;
		}

		/**
		 * @param xNome
		 *            Razão Social ou Nome do emitente
		 */
		private void setxNome(String xNome) {
			if (String.valueOf(xNome).length() < 2 || String.valueOf(xNome).length() > 60)
				throw new IllegalArgumentException("O tamanho da xNome deve ser no mínimo 2 e no máximo 60.");
			this.xNome = xNome;
		}

		/**
		 * @param xFant
		 *            Nome fantasia (opcional)
		 * @return Builder
		 */
		public Builder comxFant(String xFant) {
			if (String.valueOf(xFant).length() < 1 || String.valueOf(xFant).length() > 60)
				throw new IllegalArgumentException("O tamanho da xFant deve ser no mínimo 1 e no máximo 60.");
			this.xFant = xFant;
			return this;
		}

		/**
		 * O tamanho da Inscrição Estadual deve ser no mínimo 2 e no máximo 14,
		 * ou ser 'ISENTO'
		 * 
		 * @param ie
		 *            Tipo Inscrição Estadual do Emitente
		 */
		private void setIe(String ie) {
			ie = ie.toUpperCase();
			if (!ie.matches("[0-9]{2,14}|ISENTO"))
				throw new IllegalArgumentException(
						"O tamanho da Inscrição Estadual deve ser no mínimo 2 e no máximo 14, ou ser 'ISENTO'");
			this.ie = ie;
		}

		/**
		 * @param crt
		 *            Código de Regime Tributário.
		 */
		private void setCrt(Crt crt) {
			this.crt = crt;
		}

		/**
		 * @param cpf
		 *            Número do CPF do emitente.
		 */
		private void setCpf(String cpf) {
			if (!cnpj.matches("[0-9]{11}"))
				throw new IllegalArgumentException("O formtato do CPF inválido");
			this.cpf = cpf;
		}

		/**
		 * E do Substituto Tributário da UF de destino da mercadoria, quando
		 * houver a retenção do ICMS ST para a UF de destino.
		 * 
		 * @param iEST
		 *            IE do Substituto Tributário
		 * @return Builder
		 */
		public Builder comIEST(String iEST) {
			if (!iEST.matches("[0-9]{2,14}"))
				throw new IllegalArgumentException("O formtato do IEST inválido");
			this.iEST = iEST;
			return this;
		}

		/**
		 * Informado na emissão de NF-e conjugada, com itens de produtos
		 * sujeitos ao ICMS e itens de serviços sujeitos ao ISSQN.
		 * 
		 * @param iM
		 *            Inscrição Municipal do Prestador de Serviço
		 * @return Builder
		 */
		public Builder comIM(String iM) {
			if (iM.length() < 1 || iM.length() > 15)
				throw new IllegalArgumentException("O tamanho da iM deve ser no mínimo 1 e no máximo 15.");
			this.iM = iM;
			return this;
		}

		/**
		 * Campo Opcional. Pode ser informado quando a Inscrição Municipal
		 * (id:C19) for informada.
		 * 
		 * @param cNAE
		 *            CNAE fiscal
		 * @return Builder
		 */
		public Builder comCNAE(String cNAE) {
			if (!cNAE.matches("[0-9]{7}"))
				throw new IllegalArgumentException("O formtato do CNAE inválido");
			this.cNAE = cNAE;
			return this;
		}

		/**
		 * Código de Segurança do Contribuinte no Banco de Dados da SEFAZ.
		 * 
		 * @param cIdToken
		 *            Identificador do CSC.
		 * @return Builder
		 */
		private void setcIdToken(String cIdToken) {
			if (!cIdToken.matches("\\d{6}"))
				throw new IllegalArgumentException("O formato do cIdToken está inválido");
			this.cIdToken = cIdToken;
		}

		/**
		 * @param cSC
		 *            Código de Segurança do Contribuinte
		 * @return Builder
		 */
		private void setcSC(String cSC) {
			if (String.valueOf(cSC).length() < 16 || String.valueOf(cSC).length() > 36)
				throw new IllegalArgumentException("O tamanho do cSC deve ser no mínimo 16 e no máximo 36.");
			this.cSC = cSC;
		}

		/**
		 * @param endereco
		 *            Endereço do emitente
		 */
		private void setEndereco(Endereco endereco) {
			this.endereco = endereco;
		}
		
		public Emit build(){
			return new Emit(this);
		}
	}

	/**
	 * @return Número do CNPJ do emitente.
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * @return Razão Social ou Nome do emitente
	 */
	public String getxNome() {
		return xNome.toUpperCase();
	}

	/**
	 * @return Nome fantasia (opcional)
	 */
	public String getxFant() {
		return xFant == null ? null : xFant.toUpperCase();
	}

	/**
	 * @return Tipo Inscrição Estadual do Emitente
	 */
	public String getIe() {
		return ie;
	}

	/**
	 * @return Código de Regime Tributário.
	 */
	public int getCrt() {
		return crt.getRegimeTributario();
	}

	/**
	 * @return Número do CPF do emitente.
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @return IE do Substituto Tributário
	 */
	public String getiEST() {
		return iEST;
	}

	/**
	 * @return Inscrição Municipal do Prestador de Serviço
	 */
	public String getiM() {
		return iM;
	}

	/**
	 * @return CNAE fiscal
	 */
	public String getcNAE() {
		return cNAE;
	}

	/**
	 * Código de Segurança do Contribuinte no Banco de Dados da SEFAZ.
	 * 
	 * @return Identificador do CSC.
	 */
	public String getcIdToken() {
		return cIdToken;
	}

	/**
	 * @return Código de Segurança do Contribuinte
	 */
	public String getcSC() {
		return cSC;
	}

	/**
	 * @return Endereço do emitente
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	private Emit(Builder builder) {
		cnpj = builder.cnpj;
		cpf = builder.cpf;
		xNome = builder.xNome;
		xFant = builder.xFant;
		endereco = builder.endereco;
		ie = builder.ie;
		crt = builder.crt;
		iEST = builder.iEST;
		iM = builder.iM;
		cNAE = builder.cNAE;
		cIdToken = builder.cIdToken;
		cSC = builder.cSC;
	}
}