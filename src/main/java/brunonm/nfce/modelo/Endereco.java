package brunonm.nfce.modelo;

/**
 * Endereço do Emitente ou destinatário
 * 
 * @author Bruno Melo
 *
 */
public class Endereco {
	private String xLgr;
	private String nro;
	private String xCpl;
	private String xBairro;
	private int cMun;
	private String xMun;
	private String uf;
	private String cep;
	private int cPais = 1058; // Cod Brasil
	private String xPais = "BRASIL"; // Nome do país
	private String fone;

	public static class Builder {
		private String xLgr;
		private String nro;
		private String xCpl;
		private String xBairro;
		private int cMun;
		private String xMun;
		private String uf;
		private String cep;
		private int cPais = 1058; // Cod Brasil
		private String xPais = "BRASIL"; // Nome do país
		private String fone;

		/**
		 * Construtor de endereço com os campos obrigatorios para o emitentes
		 * 
		 * @param xLgr
		 *            Logradouro
		 * @param nro
		 *            Número
		 * @param xBairro
		 *            Bairro
		 * @param cMun
		 *            Código do município
		 * @param xMun
		 *            Nome do município
		 * @param uf
		 *            Sigla da UF
		 * @param cep
		 *            CEP
		 */
		public Builder(String xLgr, String nro, String xBairro, int cMun, String xMun, String uf, String cep) {
			setxLgr(xLgr);
			setNro(nro);
			setxBairro(xBairro);
			setcMun(cMun);
			setxMun(xMun);
			setUf(uf);
			comCEP(cep);
		}

		/**
		 * Construtor de endereço com os campos obrigatorios para o destinatário
		 * 
		 * @param xLgr
		 *            Logradouro
		 * @param nro
		 *            Número
		 * @param xBairro
		 *            Bairro
		 * @param cMun
		 *            Código do município
		 * @param xMun
		 *            Nome do município
		 * @param uf
		 *            Sigla da UF
		 */
		public Builder(String xLgr, String nro, String xBairro, int cMun, String xMun, String uf) {
			setxLgr(xLgr);
			setNro(nro);
			setxBairro(xBairro);
			setcMun(cMun);
			setxMun(xMun);
			setUf(uf);
		}

		/**
		 * @param xLgr
		 *            Logradouro
		 */
		private void setxLgr(String xLgr) {
			if (String.valueOf(xLgr).length() < 2 || String.valueOf(xLgr).length() > 60)
				throw new IllegalArgumentException("O tamanho da xLgr deve ser no mínimo 2 e no máximo 60.");
			this.xLgr = xLgr;
		}

		/**
		 * @param nro
		 *            Número
		 */
		private void setNro(String nro) {
			if (nro.length() < 1 || nro.length() > 60)
				throw new IllegalArgumentException("O tamanho da nro deve ser no mínimo 1 e no máximo 60.");
			this.nro = nro;
		}

		/**
		 * @param xCpl
		 *            Complemento
		 * @return Builder
		 */
		public Builder comxCpl(String xCpl) {
			if (String.valueOf(xCpl).length() < 1 || String.valueOf(xCpl).length() > 60)
				throw new IllegalArgumentException("O tamanho da xCpl deve ser no mínimo 1 e no máximo 60.");
			this.xCpl = xCpl;
			return this;
		}

		/**
		 * @param xBairro
		 *            Bairro
		 */
		private void setxBairro(String xBairro) {
			if (String.valueOf(xBairro).length() < 2 || String.valueOf(xBairro).length() > 60)
				throw new IllegalArgumentException("O tamanho da xBairro deve ser no mínimo 2 e no máximo 60.");
			this.xBairro = xBairro;
		}

		/**
		 * @param cMun
		 *            Código do município
		 */
		private void setcMun(int cMun) {
			if (String.valueOf(cMun).length() != 7)
				throw new IllegalArgumentException("O tamanho do cMun deve ser exatamente 7");
			this.cMun = cMun;
		}

		/**
		 * @param xMun
		 *            Nome do município
		 */
		private void setxMun(String xMun) {
			if (String.valueOf(xMun).length() < 2 || String.valueOf(xMun).length() > 60)
				throw new IllegalArgumentException("O tamanho da xMun deve ser no mínimo 2 e no máximo 60.");
			this.xMun = xMun;
		}

		/**
		 * @param uf
		 *            Sigla da UF
		 */
		private void setUf(String uf) {
			if (uf.length() != 2)
				throw new IllegalArgumentException("O tamanho do UF deve ser exatamente 2");
			this.uf = uf;
		}

		/**
		 * @param cep
		 *            Código do CEP
		 * @return Builder
		 */
		public Builder comCEP(String cep) {
			if (!cep.matches("[0-9]{8}"))
				throw new IllegalArgumentException("O cep está inválido");
			this.cep = cep;
			return this;
		}

		/**
		 * Caso não especificado, o padrão será: 1058
		 * 
		 * @param cPais
		 *            Código do País
		 * @return Builder
		 */
		public Builder comcPais(int cPais) {
			if (!String.valueOf(cPais).matches("[0-9]{1,4}"))
				throw new IllegalArgumentException("O cPais está inválido");
			this.cPais = cPais;
			return this;
		}

		/**
		 * Caso não especifico, o padrão será: BRASIL
		 * 
		 * @param xPais
		 *            Nome do País
		 * @return Builder
		 */
		public Builder comxPais(String xPais) {
			if (String.valueOf(xPais).length() < 2 || String.valueOf(xPais).length() > 60)
				throw new IllegalArgumentException("O tamanho da xPais deve ser no mínimo 2 e no máximo 60.");
			this.xPais = xPais;
			return this;
		}

		/**
		 * Preencher com o Código DDD + número do telefone. Nas operações
		 * com exterior é permitido informar o código do país + código da
		 * localidade + número do telefone
		 * 
		 * @param fone
		 *            Telefone
		 * @return Builder
		 */
		public Builder comFone(String fone) {
			if (!fone.matches("[0-9]{6,14}"))
				throw new IllegalArgumentException("O fone está inválido");
			this.fone = fone;
			return this;
		}

		public Endereco build() {
			return new Endereco(this);
		}

	}

	private Endereco(Builder builder) {
		xLgr = builder.xLgr;
		nro = builder.nro;
		xCpl = builder.xCpl;
		xBairro = builder.xBairro;
		cMun = builder.cMun;
		xMun = builder.xMun;
		uf = builder.uf;
		cep = builder.cep;
		cPais = builder.cPais;
		xPais = builder.xPais;
		fone = builder.fone;
	}

	/**
	 * @return Logradouro
	 */
	public String getxLgr() {
		return xLgr.toUpperCase();
	}

	/**
	 * @return Número
	 */
	public String getNro() {
		return nro;
	}

	/**
	 * @return Complemento
	 */
	public String getxCpl() {
		return xCpl == null ? null : xCpl.toUpperCase();
	}

	/**
	 * @return Bairro
	 */
	public String getxBairro() {
		return xBairro.toUpperCase();
	}

	/**
	 * @return Código do município
	 */
	public int getcMun() {
		return cMun;
	}

	/**
	 * @return Nome do município
	 */
	public String getxMun() {
		return xMun;
	}

	/**
	 * @return Sigla da UF
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @return Código do CEP
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * Caso não especificado, o padrão será: 1058
	 * 
	 * @return Código do País
	 */
	public int getcPais() {
		return cPais;
	}

	/**
	 * Caso não especificado, o padrão será: BRASIL
	 * 
	 * @return Nome do País
	 */
	public String getxPais() {
		return xPais;
	}

	/**
	 * @return Telefone
	 */
	public String getFone() {
		return fone;
	}
}
