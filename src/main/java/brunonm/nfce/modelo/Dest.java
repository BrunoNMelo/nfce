package brunonm.nfce.modelo;

import brunonm.nfce.enums.IEDest;

/**
 * Classe modelo para informações do destinatario da NF-e
 * 
 * @author Bruno Melo
 *
 */
public class Dest {
	private String cnpj;
	private String cpf;
	private String idEstrangeiro;
	private String xNome;
	private Endereco endereco;
	private IEDest indIEDest;
	private String iE;
	private String iSUF;
	private String iM;
	private String email;

	public static class Builder {
		// parametros obrigatórios
		private String cnpj;
		private String cpf;
		private String idEstrangeiro;
		private IEDest indIEDest;

		// parametros opcionais
		private String xNome;
		private Endereco endereco;
		private String iE;
		private String iSUF;
		private String iM;
		private String email;

		/**
		 * Construtor para classe Destinatário.
		 * 
		 * @param cnpjCpfIdEstrangeiro
		 *            CNPJ, CPF ou identificaçãoEsntrangeira do destinatário
		 * @param IdEstrangeiro.
		 *            No caso de operação com o exterior, ou para comprador
		 *            estrangeiro.
		 * @param indIEDest
		 *            Identificador da IE do destinatário.
		 */
		public Builder(String cnpjCpfIdEstrangeiro, boolean IdEstrangeiro, IEDest indIEDest) {
			if (IdEstrangeiro) {
				if (!cnpjCpfIdEstrangeiro.matches("([!-ÿ]{0}|[!-ÿ]{5,20})?"))
					throw new IllegalArgumentException("O formato do idEstrangeiro está inválido.");
				setIdEstrangeiro(cnpjCpfIdEstrangeiro);
			} else {
				if (cnpjCpfIdEstrangeiro.length() != 14 && cnpjCpfIdEstrangeiro.length() != 11)
					throw new IllegalArgumentException("O tamanho do CNPJ deve ser exatamente 14 e do CPF deve ser 11");
				if (cnpjCpfIdEstrangeiro.length() == 14)
					setCnpj(cnpjCpfIdEstrangeiro);
				if (cnpjCpfIdEstrangeiro.length() == 11)
					setCpf(cnpjCpfIdEstrangeiro);
			}
			setIndIEDest(indIEDest);
		}

		/**
		 * @param cnpj
		 *            CNPJ do destinatário
		 */
		private void setCnpj(String cnpj) {
			if (!cnpj.matches("[0-9]{14}"))
				throw new IllegalArgumentException("O formato do cnpj está inválido.");
			this.cnpj = cnpj;
		}

		/**
		 * @param cpf
		 *            CPF do destinatário
		 */
		private void setCpf(String cpf) {
			if (!cpf.matches("[0-9]{11}"))
				throw new IllegalArgumentException("O formato do cpf está inválido.");
			this.cpf = cpf;
		}

		/**
		 * @param idEstrangeiro
		 *            Identificação do destinatário no caso de comprador
		 *            estrangeiro
		 */
		private void setIdEstrangeiro(String idEstrangeiro) {
			if (!idEstrangeiro.matches("([!-ÿ]{0}|[!-ÿ]{5,20})?"))
				throw new IllegalArgumentException("O formato do idEstrangeiro está inválido.");
			this.idEstrangeiro = idEstrangeiro;
		}

		/**
		 * @param xNome
		 *            Razão Social ou nome do destinatário
		 * @return Builder
		 */
		public Builder comxNome(String xNome) {
			if (xNome.length() < 2 || xNome.length() > 60)
				throw new IllegalArgumentException("O tamanho do xNome deve ser no mínimo 2 e no máximo 60.");
			this.xNome = xNome;
			return this;
		}

		/**
		 * @param indIEDest
		 *            Indicador da IE do Destinatário
		 */
		private void setIndIEDest(IEDest indIEDest) {
			this.indIEDest = indIEDest;
		}

		/**
		 * @param iE
		 *            Inscrição Estadual do Destinatário
		 * @return Builder
		 */
		public Builder comIE(String iE) {
			if (!iE.matches("[0-9]{2,14}"))
				throw new IllegalArgumentException("O formato do iE está inválido.");
			this.iE = iE;
			return this;
		}

		/**
		 * @param iSUF
		 *            Inscrição na SUFRAMA
		 * @return Builder
		 */
		public Builder comISUF(String iSUF) {
			if (!iSUF.matches("[0-9]{8,9}"))
				throw new IllegalArgumentException("O formato do iSUF está inválido.");
			this.iSUF = iSUF;
			return this;
		}

		/**
		 * @param iM
		 *            Inscrição Municipal do Tomador do Serviço
		 * @return Builder
		 */
		public Builder comIM(String iM) {
			if (iM.length() < 1 || iM.length() > 15)
				throw new IllegalArgumentException("O tamanho do iM deve ser no mínimo 1 e no máximo 15.");
			this.iM = iM;
			return this;
		}

		/**
		 * @param email
		 *            Email
		 * @return Builder
		 */
		public Builder comEmail(String email) {
			if (email.length() < 1 || email.length() > 60)
				throw new IllegalArgumentException("O tamanho do email deve ser no mínimo 1 e no máximo 60.");
			this.email = email;
			return this;
		}

		/**
		 * @param endereco
		 *            Endereço do Destinatário da NF-e
		 * @return Builder           
		 */
		public Builder comEndereco(Endereco endereco) {
			this.endereco = endereco;
			return this;
		}

		public Dest build() {
			return new Dest(this);
		}

	}

	/**
	 * @return Endereço do Destinatário da NF-e
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * @return Email
	 */
	public String getEmail() {
		return email == null ? null : email.toUpperCase();
	}

	/**
	 * @return Inscrição Municipal do Tomador do Serviço
	 */
	public String getiM() {
		return iM;
	}

	/**
	 * @return Inscrição na SUFRAMA
	 */
	public String getiSUF() {
		return iSUF;
	}

	/**
	 * @return Inscrição Estadual do Destinatário
	 */
	public String getiE() {
		return iE;
	}

	/**
	 * @return Indicador da IE do Destinatário
	 */
	public String getIndIEDest() {
		return indIEDest.getiEdest();
	}

	/**
	 * @return Razão Social ou nome do destinatário
	 */
	public String getxNome() {
		return xNome == null ? null : xNome.toUpperCase();
	}

	/**
	 * @return Identificação do destinatário no caso de comprador estrangeiro
	 */
	public String getIdEstrangeiro() {
		return idEstrangeiro;
	}

	/**
	 * @return CPF do destinatário
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @return CNPJ do destinatário
	 */
	public String getCnpj() {
		return cnpj;
	}

	private Dest(Builder builder) {
		this.cnpj = builder.cnpj;
		this.cpf = builder.cpf;
		this.idEstrangeiro = builder.idEstrangeiro;
		this.indIEDest = builder.indIEDest;
		this.xNome = builder.xNome;
		this.email = builder.email;
		this.endereco = builder.endereco;
		this.iE = builder.iE;
		this.iM = builder.iM;
		this.iSUF = builder.iSUF;
	}
}
