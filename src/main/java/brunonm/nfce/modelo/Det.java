package brunonm.nfce.modelo;

import java.text.DecimalFormat;

import brunonm.nfce.enums.IndTot;

/**
 * Classe modelo para informações dos produtos e a sua tributação
 * 
 * @author Bruno Melo
 */
public class Det {
	// prod
	private String cProd;
	private String cEAN;
	private String xProd;
	private int nCM;
	private String nVE;
	private String cEST;
	private String eXTIPI;
	private int cFOP;
	private String uCom;
	private String qCom;
	private String vProd;
	private String cEANTrib;
	private String uTrib;
	private String qTrib;
	private String vDesc;
	private String vFrete;
	private String vSeg;
	private String vOutro;
	private IndTot indTot;
	// // imposto
	private Imposto imposto;

	public static class Builder {
		// prod
		private String cProd;
		private String cEAN;
		private String xProd;
		private int nCM;
		private String nVE;
		private String cEST;
		private String eXTIPI;
		private int cFOP;
		private String uCom;
		private String qCom;
		private String vProd;
		private String cEANTrib;
		private String uTrib;
		private String qTrib;
		private String vDesc;
		private String vFrete;
		private String vSeg;
		private String vOutro;
		private IndTot indTot;
		// // imposto
		private Imposto imposto;
		
		/**
		 * Construtor com as informações obrigatórias dos Det
		 * 
		 * @param cProd
		 *            Código do produto ou serviço.
		 * @param cEAN
		 *            GTIN (Global Trade Item Number) do produto
		 * @param xProd
		 *            Descrição do produto ou serviço
		 * @param nCM
		 *            Código NCM (8 posições)
		 * @param cFOP
		 *            Cfop
		 * @param uCom
		 *            Unidade comercial
		 * @param qCom
		 *            Quantidade Comercial do produto
		 * @param vProd
		 *            Valor bruto do produto ou serviço.
		 * @param cEANTrib
		 *            GTIN (Global Trade Item Number) da unidade tributável
		 * @param indTot
		 *            valor do item (vProd)
		 * @param imposto
		 *            Classe com os impostos do produto
		 */
		public Builder(String cProd, String cEAN, String xProd, int nCM, int cFOP, String uCom, String qCom,
				String vProd, String cEANTrib, IndTot indTot, Imposto imposto) {
			setcProd(cProd);
			setcEAN(cEAN);
			setxProd(xProd);
			setnCM(nCM);
			setcFOP(cFOP);
			setuCom(uCom);
			setqCom(qCom);
			setvProd(vProd);
			setcEANTrib(cEANTrib);
			setIndTot(indTot);
			setImposto(imposto);
		}

		/**
		 * Preencher com CFOP caso se trate de itens não relacionados com
		 * mercadorias/produto e que o contribuinte não possua codificação
		 * própria Formato ”CFOP9999”.
		 * 
		 * @param cProd
		 *            Código do produto ou serviço.
		 */
		private void setcProd(String cProd) {
			if (String.valueOf(cProd).length() < 1 || String.valueOf(cProd).length() > 60)
				throw new IllegalArgumentException("O tamanho do cProd deve ser no mínimo 1 e no máximo 60.");
			this.cProd = cProd;
		}

		/**
		 * antigo código EAN ou código de barras
		 * 
		 * @param cEAN
		 *            GTIN (Global Trade Item Number) do produto
		 */
		private void setcEAN(String cEAN) {
			if (cEAN.length() != 8 && cEAN.length() != 0 && (cEAN.length() < 12 || cEAN.length() > 14))
				throw new IllegalArgumentException("O tamanho do cEAN deve ser 0, ou 8, ou entre 12 a 14.");
			this.cEAN = cEAN;
		}

		/**
		 * @param xProd
		 *            Descrição do produto ou serviço
		 */
		private void setxProd(String xProd) {
			if (String.valueOf(xProd).length() < 1 || String.valueOf(xProd).length() > 120)
				throw new IllegalArgumentException("O tamanho do xProd deve ser no mínimo 1 e no máximo 120.");
			this.xProd = xProd;
		}

		/**
		 * Será permitida a informação do gênero (posição do capítulo do NCM)
		 * quando a operação não for de comércio exterior
		 * (importação/exportação) ou o produto não seja tributado pelo IPI. Em
		 * caso de item de serviço ou item que não tenham produto (Ex.
		 * transferência de crédito, crédito do ativo imobilizado, etc.),
		 * informar o código 00 (zeros) (v2.0)
		 * 
		 * @param nCM
		 *            Código NCM (8 posições)
		 */
		private void setnCM(int nCM) {
			if ((String.valueOf(nCM).length() != 2) && (String.valueOf(nCM).length() != 8))
				throw new IllegalArgumentException("O tamanho do nCM deve ser 2, ou 8");
			this.nCM = nCM;
		}

		/**
		 * @param nVE
		 *            Nomenclatura de Valor aduaneio e Estatístico (Opcional)
		 * @return Builder
		 */
		public Builder comNVE(String nVE) {
			if (!nVE.matches("[A-Z]{2}[0-9]{4}"))
				throw new IllegalArgumentException("O formato do nVE está errado.");
			this.nVE = nVE;
			return this;
		}

		/**
		 * CEST, que identifica a mercadoria sujeita aos regimes de substituicao
		 * tributária e de antecipação do recolhimento do imposto (Opcional)
		 * 
		 * @param cEST
		 *            Código especificador da Substuicao Tributaria
		 * @return Builder
		 */
		public Builder comCEST(String cEST) {
			if (!String.valueOf(cEST).matches("[0-9]{7}"))
				throw new IllegalArgumentException("O tamanho do cEST deve ser 7.");
			this.cEST = cEST;
			return this;
		}

		/**
		 * @param eXTIPI
		 *            Código EX TIPI (3 posições) (Opcional)
		 * @return Builder
		 */
		public Builder comEXTIPI(String eXTIPI) {
			if (String.valueOf(eXTIPI).length() < 2 || String.valueOf(eXTIPI).length() > 3)
				throw new IllegalArgumentException("O tamanho do eXTIPI deve ser no mínimo 2 e no máximo 3.");
			this.eXTIPI = eXTIPI;
			return this;
		}

		/**
		 * 
		 * @param cFOP
		 *            Cfop
		 */
		private void setcFOP(int cFOP) {
			if (!String.valueOf(cFOP).matches("[1,2,3,5,6,7]{1}[0-9]{3}"))
				throw new IllegalArgumentException("O formato do CFOP está errado.");
			this.cFOP = cFOP;
		}

		/**
		 * @param uCom
		 *            Unidade comercial
		 */
		private void setuCom(String uCom) {
			if (String.valueOf(uCom).length() < 1 || String.valueOf(uCom).length() > 6)
				throw new IllegalArgumentException("O tamanho do uCom deve ser no mínimo 1 e no máximo 6.");
			this.uCom = uCom;
			this.uTrib = uCom;
		}

		/**
		 * Quantidade Comercial do produto, alterado para aceitar de 0 a 4 casas
		 * decimais e 11 inteiros.
		 * 
		 * @param qCom
		 *            Quantidade Comercial do produto
		 */
		private void setqCom(String qCom) {
			if (!qCom.matches("0|0\\.[0-9]{1,4}|[1-9]{1}[0-9]{0,10}|[1-9]{1}[0-9]{0,10}(\\.[0-9]{1,4})?"))
				throw new IllegalArgumentException("O formato do qCom está errado.");
			this.qCom = qCom;
			this.qTrib = qCom;
		}

		/**
		 * 
		 * @param vProd
		 *            Valor bruto do produto ou serviço.
		 */
		private void setvProd(String vProd) {
			if (!vProd.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vProd está errado.");
			this.vProd = vProd;
		}

		/**
		 * antigo código EAN ou código de barras
		 * 
		 * @param cEANTrib
		 *            GTIN (Global Trade Item Number) da unidade tributável
		 */
		private void setcEANTrib(String cEANTrib) {
			if (!cEANTrib.matches("[0-9]{0}|[0-9]{8}|[0-9]{12,14}"))
				throw new IllegalArgumentException("O tamanho do cEANTrib deve ser 0, ou 8, ou entre 12 a 14.");
			this.cEANTrib = cEANTrib;
		}

		/**
		 * Este campo deverá ser preenchido com: false – o valor do item (vProd)
		 * não compõe o valor total da NF-e (vProd) true – o valor do item
		 * (vProd) compõe o valor total da NF-e (vProd)
		 * 
		 * @param indTot
		 *            valor do item (vProd)
		 */
		private void setIndTot(IndTot indTot) {
			this.indTot = indTot;
		}

		/**
		 * composta da classe com os impostos do produto
		 * 
		 * @param imposto
		 *            classe Imposto
		 */
		private void setImposto(Imposto imposto) {
			this.imposto = imposto;
		}

		/**
		 * @param vDesc
		 *            Valor do Desconto
		 * @return Builder
		 */
		public Builder comvDesc(String vDesc) {
			if (!vDesc.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vDesc está errado.");
			this.vDesc = vDesc;
			return this;
		}

		/**
		 * @param vFrete
		 *            Valor Total do Frete
		 * @return Builder
		 */
		public Builder comvFrete(String vFrete) {
			if (!vFrete.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vFrete está inválido.");
			this.vFrete = vFrete;
			return this;
		}

		/**
		 * @param vSeg
		 *            Valor Total do Seguro
		 * @return Builder
		 */
		public Builder comvSeg(String vSeg) {
			if (!vSeg.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vSeg está inválido.");
			this.vSeg = vSeg;
			return this;
		}

		/**
		 * @param vOutro
		 *            Outras despesas acessórias
		 * @return Builder
		 */
		public Builder comvOutro(String vOutro) {
			if (!vOutro.matches("0|0\\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\\.[0-9]{2})?"))
				throw new IllegalArgumentException("O formato do vOutro está inválido.");
			this.vOutro = vOutro;
			return this;
		}
		
		public Det build(){
			return new Det(this);
		}
	}

	/**
	 * @return Código do produto ou serviço.
	 */
	public String getcProd() {
		return cProd;
	}

	/**
	 * @return GTIN (Global Trade Item Number) do produto.
	 */
	public String getcEAN() {
		return cEAN;
	}

	/**
	 * @return Descrição do produto ou serviço
	 */
	public String getxProd() {
		return xProd.toUpperCase();
	}

	/**
	 * @return Código NCM (8 posições)
	 */
	public int getnCM() {
		return nCM;
	}

	/**
	 * @return Nomenclatura de Valor aduaneio e Estatístico (Opcional)
	 */
	public String getnVE() {
		return nVE;
	}

	/**
	 * @return Codigo especificador da Substuicao Tributaria
	 */
	public String getcEST() {
		return cEST;
	}

	/**
	 * @return Código EX TIPI (3 posições)
	 */
	public String geteXTIPI() {
		return eXTIPI;
	}

	/**
	 * @return Cfop
	 */
	public int getcFOP() {
		return cFOP;
	}

	/**
	 * @return Unidade comercial
	 */
	public String getuCom() {
		return uCom;
	}

	/**
	 * @return Quantidade Tributária do produto.
	 */
	public String getqCom() {
		DecimalFormat df = new DecimalFormat("##########0.0000");
		return df.format(Double.parseDouble(qCom)).replaceAll(",", ".");
	}

	/**
	 * @return Valor unitário de comercial
	 */
	public String getvUnCom() {
		DecimalFormat df = new DecimalFormat("##########0.0000000000");
		double vUncom = Double.parseDouble(getvProd()) / Double.parseDouble(getqCom());
		return df.format(vUncom).replaceAll(",", ".");
	}

	/**
	 * @return Valor bruto do produto ou serviço.
	 */
	public String getvProd() {
		return vProd;
	}

	/**
	 * @return GTIN (Global Trade Item Number) da unidade tributável
	 */
	public String getcEANTrib() {
		return cEANTrib;
	}

	/**
	 * @return Unidade tributária.
	 */
	public String getuTrib() {
		return uTrib;
	}

	/**
	 * @return Quantidade Tributária do produto
	 */
	public String getqTrib() {
		DecimalFormat df = new DecimalFormat("##########0.0000");
		return df.format(Double.parseDouble(qTrib)).replaceAll(",", ".");
	}

	/**
	 * @return Valor unitário de tributação
	 */
	public String getvUnTrib() {
		DecimalFormat df = new DecimalFormat("##########0.0000000000");
		double vUncom = Double.parseDouble(getvProd()) / Double.parseDouble(getqTrib());
		return df.format(vUncom).replaceAll(",", ".");
	}

	/**
	 * @return valor do item (vProd)
	 */
	public String getIndTot() {
		return indTot.getIndTot();
	}

	/**
	 * composta da classe com os impostos do produto
	 * 
	 * @return classe Imposto do produto
	 */
	public Imposto getImposto() {
		return imposto;
	}

	/**
	 * @return Valor do Desconto
	 */
	public String getvDesc() {
		return vDesc;
	}

	/**
	 * @return Valor Total do Frete
	 */
	public String getvFrete() {
		return vFrete;
	}

	/**
	 * @return Valor Total do Seguro
	 */
	public String getvSeg() {
		return vSeg;
	}

	/**
	 * @return Outras despesas acessórias
	 */
	public String getvOutro() {
		return vOutro;
	}
	
	private Det(Builder builder){
		cProd = builder.cProd;
		cEAN = builder.cEAN;
		xProd = builder.xProd;
		nCM = builder.nCM;
		nVE = builder.nVE;
		cEST = builder.cEST;
		eXTIPI = builder.eXTIPI;
		cFOP = builder.cFOP;
		uCom = builder.uCom;
		qCom = builder.qCom;
		vProd = builder.vProd;
		cEANTrib = builder.cEANTrib;
		uTrib = builder.uTrib;
		qTrib = builder.qTrib;
		vDesc = builder.vDesc;
		vFrete = builder.vFrete;
		vSeg = builder.vSeg;
		vOutro = builder.vOutro;
		indTot = builder.indTot;
		imposto = builder.imposto;
	}
}