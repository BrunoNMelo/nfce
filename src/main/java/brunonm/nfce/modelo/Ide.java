package brunonm.nfce.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.FinNFe;
import brunonm.nfce.enums.IdDest;
import brunonm.nfce.enums.IndFinal;
import brunonm.nfce.enums.IndPag;
import brunonm.nfce.enums.IndPres;
import brunonm.nfce.enums.ModeloNF;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.TpImp;
import brunonm.nfce.enums.TpNF;

/**
 * Classe modelo para informações de identificação geral da NFCe
 * 
 * @author Bruno Melo
 *
 */
public class Ide {

	/**
	 * formato de data padrão UTF
	 */
	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");;

	private Estados cUF;
	private int cNF;
	private String natOP;
	private IndPag indPag;
	private ModeloNF mod = ModeloNF.NFCe;
	private int serie;
	private int nNF;
	private Date dhEmi;
	private Date dhSaiEnt;
	private TpNF tpNF;
	private IdDest idDest;
	private int cMunFG;
	private TpImp tpImp;
	private TpEmis tpEmis;
	private int cDV;
	private TpAmb tpAmb;
	private FinNFe finNFe;
	private IndFinal indFinal;
	private IndPres indPres;
	private int procEmi = 0; // 0 - emissão de NF-e com aplicativo do
								// contribuinte;
	private String verProc;
	private String nfID;
	private String xJust;
	private Date dhCont;

	public static class Builder {
		private Estados cUF;
		private int cNF;
		private String natOP;
		private IndPag indPag;
		private ModeloNF mod = ModeloNF.NFCe;
		private int serie;
		private int nNF;
		private Date dhEmi;
		private Date dhSaiEnt;
		private TpNF tpNF;
		private IdDest idDest;
		private int cMunFG;
		private TpImp tpImp;
		private TpEmis tpEmis;
		private int cDV;
		private TpAmb tpAmb;
		private FinNFe finNFe;
		private IndFinal indFinal;
		private IndPres indPres;
		private String verProc;
		private String nfID;
		private String xJust;
		private Date dhCont;

		/**
		 * Construtor com as informações obrigatórias do Ide no modelo 4.00 da
		 * NFC-e
		 * 
		 * @param cUF
		 *            Código da UF do emitente do Documento Fiscal.
		 * @param cNF
		 *            Código numérico que compõe a Chave de Acesso.
		 * @param natOP
		 *            Descrição da Natureza da Operação
		 * @param serie
		 *            Série do Documento Fiscal
		 * @param dhEmi
		 *            Data e Hora de emissão do Documento Fiscal
		 * @param tpNF
		 *            Tipo do Documento Fiscal
		 * @param idDest
		 *            Identificador de Local de destino da operação
		 * @param cMunFG
		 *            Código do Município de Ocorrência do Fato Gerador
		 * @param tpImp
		 *            Formato de impressão do DANFE
		 * @param tpEmis
		 *            Forma de emissão da NF-e
		 * @param tpAmb
		 *            Identificação do Ambiente
		 * @param finNFe
		 *            Finalidade da emissão da NF-e
		 * @param indFinal
		 *            Indica operação com consumidor final
		 * @param indPres
		 *            Processo de emissão utilizado com a seguinte codificação
		 * @param verProc
		 *            versão do aplicativo utilizado no processo de emissão
		 * @param cNPJ
		 *            CNPJ da empresa emitente
		 */
		public Builder(Estados cUF, int cNF, String natOP, int serie, Date dhEmi, TpNF tpNF, IdDest idDest, int cMunFG,
				TpImp tpImp, TpEmis tpEmis, TpAmb tpAmb, FinNFe finNFe, IndFinal indFinal, IndPres indPres,
				String verProc, String cNPJ) {
			setcUF(cUF);
			setcNF(cNF);
			setnNF(cNF);
			setNatOP(natOP);
			setSerie(serie);
			setDhEmi(dhEmi);
			setTpNF(tpNF);
			setIdDest(idDest);
			setcMunFG(cMunFG);
			setTpImp(tpImp);
			setTpEmis(tpEmis);
			setTpAmb(tpAmb);
			setFinNFe(finNFe);
			setIndFinal(indFinal);
			setIndPres(indPres);
			setVerProc(verProc);

			if (String.valueOf(cNPJ).length() != 14)
				throw new IllegalArgumentException("O tamanho do cNPJ deve ser exatamente 14");

			setcDV(cUF.getCodigoIbge(), dhEmi, cNPJ, String.valueOf(serie), String.valueOf(nNF),
					tpEmis.getFormaEmissaoNFe(), getcNF());
		}

		/**
		 * Construtor com as informações obrigatórias do Ide no modelo 3.10 da
		 * NFC-e
		 * 
		 * @param cUF
		 *            Código da UF do emitente do Documento Fiscal.
		 * @param cNF
		 *            Código numérico que compõe a Chave de Acesso.
		 * @param natOP
		 *            Descrição da Natureza da Operação
		 * @param indPag
		 *            Indicador da forma de pagamento
		 * @param serie
		 *            Série do Documento Fiscal
		 * @param dhEmi
		 *            Data e Hora de emissão do Documento Fiscal
		 * @param tpNF
		 *            Tipo do Documento Fiscal
		 * @param idDest
		 *            Identificador de Local de destino da operação
		 * @param cMunFG
		 *            Código do Município de Ocorrência do Fato Gerador
		 * @param tpImp
		 *            Formato de impressão do DANFE
		 * @param tpEmis
		 *            Forma de emissão da NF-e
		 * @param tpAmb
		 *            Identificação do Ambiente
		 * @param finNFe
		 *            Finalidade da emissão da NF-e
		 * @param indFinal
		 *            Indica operação com consumidor final
		 * @param indPres
		 *            Processo de emissão utilizado com a seguinte codificação
		 * @param verProc
		 *            versão do aplicativo utilizado no processo de emissão
		 * @param cNPJ
		 *            CNPJ da empresa emitente
		 */
		public Builder(Estados cUF, int cNF, String natOP, IndPag indPag, int serie, Date dhEmi, TpNF tpNF,
				IdDest idDest, int cMunFG, TpImp tpImp, TpEmis tpEmis, TpAmb tpAmb, FinNFe finNFe, IndFinal indFinal,
				IndPres indPres, String verProc, String cNPJ) {
			setcUF(cUF);
			setcNF(cNF);
			setNatOP(natOP);
			setIndPag(indPag);
			setSerie(serie);
			setnNF(cNF);
			setDhEmi(dhEmi);
			setTpNF(tpNF);
			setIdDest(idDest);
			setcMunFG(cMunFG);
			setTpImp(tpImp);
			setTpEmis(tpEmis);
			setTpAmb(tpAmb);
			setFinNFe(finNFe);
			setIndFinal(indFinal);
			setIndPres(indPres);
			setVerProc(verProc);

			if (String.valueOf(cNPJ).length() != 14)
				throw new IllegalArgumentException("O tamanho do cNPJ deve ser exatamente 14");

			setcDV(cUF.getCodigoIbge(), dhEmi, cNPJ, String.valueOf(serie), String.valueOf(nNF),
					tpEmis.getFormaEmissaoNFe(), getcNF());
		}

		/**
		 * Gera o Digito Verificador da Chave de Acesso e o Id da NF-e
		 * 
		 * @param cUF
		 *            Código da UF do emitente do Documento Fiscal.
		 * @param dhEmit
		 *            Data e Hora de emissão do Documento Fiscal
		 * @param cNPJ
		 *            CNPJ da empresa emitente
		 * @param serie
		 *            Série do Documento Fiscal
		 * @param nNF
		 *            Número do Documento Fiscal
		 * @param tpEmis
		 *            Forma de emissão da NF-e
		 * @param cNF
		 *            Código numérico que compõe a Chave de Acesso.
		 */
		private void setcDV(int cUF, Date dhEmit, String cNPJ, String serie, String nNF, int tpEmis, String cNF) {
			SimpleDateFormat dataFormat = new SimpleDateFormat("YYMM");

			StringBuilder xSerie = new StringBuilder();
			if (String.valueOf(serie).length() != 3) {
				int xNFTamanho = String.valueOf(this.serie).length();
				int i = 3 - xNFTamanho;
				for (int x = 0; x < i; x++) {
					xSerie.append("0");
				}
				xSerie.append(serie);
			} else {
				xSerie.append(serie);
			}

			StringBuilder xNF = new StringBuilder();
			if (xNF.length() != 9) {
				int xNFTamanho = String.valueOf(nNF).length();
				int i = 9 - xNFTamanho;
				for (int x = 0; x < i; x++) {
					xNF.append("0");
				}
				xNF.append(nNF);
			} else {
				xNF.append(nNF);
			}

			StringBuilder sb = new StringBuilder();
			sb.append(cUF);
			sb.append(dataFormat.format(dhEmit));
			sb.append(cNPJ);
			sb.append(mod.getModeloNF());
			sb.append(xSerie.toString());
			sb.append(xNF.toString());
			sb.append(tpEmis);
			sb.append(cNF);

			int total = 0;
			int peso = 2;
			for (int i = sb.length() - 1; i >= 0; i--) {
				total += (Character.getNumericValue(sb.charAt(i)) * peso);
				if (peso == 9) {
					peso = 2;
				} else {
					peso++;
				}
			}

			int result = 0;
			int resto = total % 11;

			if (resto > 1) {
				result = 11 - resto;
			} else {
				result = 0;
			}

			this.cDV = result;
			this.nfID = "NFe" + sb.append(result).toString();
		}

		/**
		 * @param cUF
		 *            Código da UF do emitente do Documento Fiscal.
		 */
		private void setcUF(Estados cUF) {
			this.cUF = cUF;
		}

		/**
		 * Código numérico que compõe a Chave de Acesso. Número aleatório gerado
		 * pelo emitente para cada NF-e.
		 * 
		 * @param cNF
		 *            Código numérico que compõe a Chave de Acesso.
		 */
		private void setcNF(int cNF) {
			if (String.valueOf(cNF).length() < 1 || String.valueOf(cNF).length() > 8)
				throw new IllegalArgumentException("O tamanho da natOP deve ser no mínimo 1 e no máximo 8.");
			this.cNF = cNF;
		}

		/**
		 * @param natOP
		 *            Descrição da Natureza da Operação
		 */
		private void setNatOP(String natOP) {
			if (String.valueOf(natOP).length() < 1 || String.valueOf(natOP).length() > 60)
				throw new IllegalArgumentException("O tamanho da natOP deve ser no mínimo 1 e no máximo 60.");
			this.natOP = natOP;
		}

		/**
		 * Série do Documento Fiscal série normal 0-889 Avulsa Fisco 890-899
		 * SCAN 900-999
		 * 
		 * @param serie
		 *            Série do Documento Fiscal
		 */
		private void setSerie(int serie) {
			if (String.valueOf(serie).length() < 1 || String.valueOf(serie).length() > 3)
				throw new IllegalArgumentException("O tamanho da serie deve ser no mínimo 1 e no máximo 3.");
			this.serie = serie;
		}

		/*
		 * Número do Documento Fiscal
		 */
		/**
		 * @param nNF
		 *            Número do Documento Fiscal
		 */
		private void setnNF(int nNF) {
			if (String.valueOf(nNF).length() < 1 || String.valueOf(nNF).length() > 9)
				throw new IllegalArgumentException("O tamanho da nNF deve ser no mínimo 1 e no máximo 9.");
			this.nNF = nNF;
		}

		/**
		 * @param dhEmi
		 *            Data e Hora de emissão do Documento Fiscal
		 */
		private void setDhEmi(Date dhEmi) {
			this.dhEmi = dhEmi;
		}

		/**
		 * @param dhSaiEnt
		 *            Data e Hora da saída ou de entrada da mercadoria
		 *            (opcional)
		 * @return Builder
		 */
		public Builder comDhSaiEnt(Date dhSaiEnt) {
			this.dhSaiEnt = dhSaiEnt;
			return this;
		}

		/**
		 * @param tpNF
		 *            Tipo do Documento Fiscal
		 */
		private void setTpNF(TpNF tpNF) {
			this.tpNF = tpNF;
		}

		/**
		 * @param idDest
		 *            Identificador de Local de destino da operação
		 */
		private void setIdDest(IdDest idDest) {
			this.idDest = idDest;
		}

		/**
		 * Código do Município de Ocorrência do Fato Gerador (utilizar a tabela
		 * do IBGE)
		 * 
		 * @param cMunFG
		 *            Código do Município de Ocorrência do Fato Gerador
		 */
		private void setcMunFG(int cMunFG) {
			if (String.valueOf(cMunFG).length() != 7)
				throw new IllegalArgumentException("O tamanho do código do municipio deve ser exatamente 7");
			this.cMunFG = cMunFG;
		}

		/**
		 * @param tpImp
		 *            Formato de impressão do DANFE
		 */
		private void setTpImp(TpImp tpImp) {
			this.tpImp = tpImp;
		}

		/**
		 * @param tpEmis
		 *            Forma de emissão da NF-e
		 */
		private void setTpEmis(TpEmis tpEmis) {
			this.tpEmis = tpEmis;
		}

		/**
		 * @param tpAmb
		 *            Identificação do Ambiente
		 */
		private void setTpAmb(TpAmb tpAmb) {
			this.tpAmb = tpAmb;
		}

		/**
		 * @param finNFe
		 *            Finalidade da emissão da NF-e
		 */
		private void setFinNFe(FinNFe finNFe) {
			this.finNFe = finNFe;
		}

		/**
		 * Indica operação com consumidor final . false = Não true = Consumidor
		 * Final
		 * 
		 * @param indFinal
		 *            Indica operação com consumidor final
		 */
		private void setIndFinal(IndFinal indFinal) {
			this.indFinal = indFinal;
		}

		/**
		 * @param indPres
		 *            Processo de emissão utilizado com a seguinte codificação
		 */
		private void setIndPres(IndPres indPres) {
			this.indPres = indPres;
		}

		/**
		 * @param verProc
		 *            versão do aplicativo utilizado no processo de emissão
		 */
		private void setVerProc(String verProc) {
			if (String.valueOf(verProc).length() < 1 || String.valueOf(verProc).length() > 20)
				throw new IllegalArgumentException("O tamanho da verProc deve ser no mínimo 1 e no máximo 20.");
			this.verProc = verProc;
		}

		/**
		 * usar em caso de envio de contingência.
		 * 
		 * @param xJust
		 *            Informar a Justificativa da entrada
		 * @return Builder
		 */
		public Builder comxJust(String xJust) {
			this.xJust = xJust;
			return this;
		}

		/**
		 * @param dhCont
		 *            Data e Hora da entrada em contingência (opcional)
		 * @return Builder
		 */
		public Builder comDhCont(Date dhCont) {
			this.dhCont = dhCont;
			return this;
		}

		/**
		 * @param indPag
		 *            Indicador da forma de pagamento
		 */
		private void setIndPag(IndPag indPag) {
			this.indPag = indPag;
		}
		
		/**
		 * O valor padrão é NFCe.
		 * @param modeloNF Modelo da Nota Fiscal Eletrônica.
		 * @return Builder
		 */
		public Builder comModeloNF(ModeloNF modeloNF){
			this.mod = modeloNF;
			return this;
		}

		/**
		 * Número aleatório gerado pelo emitente para cada NF-e.
		 * 
		 * @return cUF Código numérico que compõe a Chave de Acesso.
		 */
		private String getcNF() {
			StringBuilder xcNF = new StringBuilder();
			if (String.valueOf(this.cNF).length() != 8) {
				int xNFTamanho = String.valueOf(this.cNF).length();
				int i = 8 - xNFTamanho;
				for (int x = 0; x < i; x++) {
					xcNF.append("0");
				}
				xcNF.append(this.cNF);
			} else {
				xcNF.append(this.cNF);
			}
			return xcNF.toString();
		}

		public Ide build() {
			return new Ide(this);
		}

	}

	private Ide(Builder builder) {
		cUF = builder.cUF;
		cNF = builder.cNF;
		natOP = builder.natOP;
		indPag = builder.indPag;
		serie = builder.serie;
		nNF = builder.nNF;
		dhEmi = builder.dhEmi;
		dhSaiEnt = builder.dhSaiEnt;
		tpNF = builder.tpNF;
		idDest = builder.idDest;
		cMunFG = builder.cMunFG;
		tpImp = builder.tpImp;
		tpEmis = builder.tpEmis;
		cDV = builder.cDV;
		tpAmb = builder.tpAmb;
		finNFe = builder.finNFe;
		indFinal = builder.indFinal;
		indPres = builder.indPres;
		verProc = builder.verProc;
		nfID = builder.nfID;
		xJust = builder.xJust;
		dhCont = builder.dhCont;
	}

	/**
	 * @return Nome do UF do emitente do Documento Fiscal.
	 */
	public Estados getcUF() {
		return cUF;
	}

	/**
	 * @return Código da UF do emitente do Documento Fiscal.
	 */
	public String getcUFNome() {
		return cUF.getNome();
	}

	/**
	 * Número aleatório gerado pelo emitente para cada NF-e.
	 * 
	 * @return cUF Código numérico que compõe a Chave de Acesso.
	 */
	public String getcNF() {
		StringBuilder xcNF = new StringBuilder();
		if (String.valueOf(this.cNF).length() != 8) {
			int xNFTamanho = String.valueOf(this.cNF).length();
			int i = 8 - xNFTamanho;
			for (int x = 0; x < i; x++) {
				xcNF.append("0");
			}
			xcNF.append(this.cNF);
		} else {
			xcNF.append(this.cNF);
		}
		return xcNF.toString();
	}

	/**
	 * @return Descrição da Natureza da Operação
	 */
	public String getNatOP() {
		return natOP.toUpperCase();
	}

	/**
	 * @return Código do modelo do Documento Fiscal.
	 */
	public ModeloNF getMod() {
		return mod;
	}

	/**
	 * @return Série do Documento Fiscal
	 */
	public String getSerie() {
		return String.valueOf(serie);

	}

	/**
	 * @return Número do Documento Fiscal
	 */
	public String getnNF() {
		return String.valueOf(nNF);
	}

	/**
	 * @return Data e Hora de emissão do Documento Fiscal no formato UTF
	 */
	public String getDhEmi() {
		return SDF.format(dhEmi);
	}

	/**
	 * @return Data e Hora da saída ou de entrada da mercadoria
	 */
	public String getDhSaiEnt() {
		if (dhSaiEnt != null) {
			return SDF.format(dhSaiEnt);
		} else {
			return null;
		}

	}

	/**
	 * @return Tipo do Documento Fiscal
	 */
	public int getTpNF() {
		return tpNF.getTipo();
	}

	/**
	 * @return Identificador de Local de destino da operação
	 */
	public String getIdDest() {
		return idDest.getIdDestCod();
	}

	/**
	 * @return Código do Município de Ocorrência do Fato Gerador
	 */
	public int getcMunFG() {
		return cMunFG;
	}

	/**
	 * @return Formato de impressão do DANFE
	 */
	public int getTpImp() {
		return tpImp.getFormatoImpressaoDanfeCod();
	}

	/**
	 * @return Forma de emissão da NF-e
	 */
	public TpEmis getTpEmis() {
		return tpEmis;
	}

	/**
	 * @return Digito Verificador da Chave de Acesso da NF-e
	 */
	public int getcDV() {
		return cDV;
	}

	/**
	 * @return Identificação do Ambiente
	 */
	public TpAmb getTpAmb() {
		return tpAmb;
	}

	/**
	 * @return Finalidade da emissão da NF-e
	 */
	public int getFinNFe() {
		return finNFe.getFinalidadeNfe();
	}

	/**
	 * @return Indica operação com consumidor final
	 */
	public String getIndFinal() {
		return indFinal.getIndFinal();
	}

	/**
	 * @return Processo de emissão utilizado com a seguinte codificação
	 */
	public String getIndPres() {
		return indPres.getIndicadorPresencaComprador();
	}

	/**
	 * @return 0 - emissão de NF-e com aplicativo do contribuinte;
	 */
	public int getProcEmi() {
		return procEmi;
	}

	/**
	 * @return versão do aplicativo utilizado no processo de emissão
	 */
	public String getVerProc() {
		return verProc;
	}

	/**
	 * @return Id da NF-e
	 */
	public String getNfID() {
		return nfID;
	}

	/**
	 * @return Justificativa da entrada
	 */
	public String getxJust() {
		return xJust;
	}

	/**
	 * @return Data e Hora da entrada em contingência
	 */
	public String getDhCont() {
		if (dhCont != null) {
			return SDF.format(dhCont);
		} else {
			return null;
		}

	}

	/**
	 * @return Indicador da forma de pagamento
	 */
	public String getIndPag() {
		return indPag == null ? null : indPag.getIndPag();
	}
}