package brunonm.nfce.exceptions;

public class VersaoNFCeErradaException extends Exception {

	private static final long serialVersionUID = 416960374075007983L;

	// Parameterless Constructor
	public VersaoNFCeErradaException() {
	}

	public VersaoNFCeErradaException(Throwable cause) {
		super(cause);
	}

	// Constructor that accepts a message
	public VersaoNFCeErradaException(String message) {
		super(message);
	}

	public VersaoNFCeErradaException(String message, Throwable cause) {
		super(message, cause);
	}

}
