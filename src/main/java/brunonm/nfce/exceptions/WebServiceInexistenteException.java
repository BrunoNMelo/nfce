package brunonm.nfce.exceptions;

public class WebServiceInexistenteException extends Exception {

	private static final long serialVersionUID = -7759714243361420421L;

	// Parameterless Constructor
	public WebServiceInexistenteException() {
		}

	public WebServiceInexistenteException(Throwable cause) {
			super(cause);
		}

	// Constructor that accepts a message
	public WebServiceInexistenteException(String message) {
			super(message);
		}

	public WebServiceInexistenteException(String message, Throwable cause) {
			super(message, cause);
		}

}
