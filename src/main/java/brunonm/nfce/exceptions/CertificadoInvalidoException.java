package brunonm.nfce.exceptions;

public class CertificadoInvalidoException extends Exception {

	private static final long serialVersionUID = -1857606695649526395L;

	// Parameterless Constructor
	public CertificadoInvalidoException() {
	}

	public CertificadoInvalidoException(Throwable cause) {
		super(cause);
	}

	// Constructor that accepts a message
	public CertificadoInvalidoException(String message) {
		super(message);
	}

	public CertificadoInvalidoException(String message, Throwable cause) {
		super(message, cause);
	}

}
