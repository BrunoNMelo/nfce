package brunonm.nfce.enums;

/**
 * Indicador de presença do comprador no estabelecimento comercial no momento da opepração
 * 0-Não se aplica (ex.: Nota Fiscal complementar ou de ajuste;
 * 1-Operação presencial;
 * 2-Não presencial, internet;
 * 3-Não presencial, teleatendimento;
 * 4-NFC-e entrega em domicílio;
 * 5-Operação presencial, fora do estabelecimento;
 * 9-Não presencial, outros;
 * @author Bruno Melo
 *
 */
public enum IndPres {
	NAO_SE_APLICA("0"),
	PRESENCIAL("1"),
	NAO_PRESENCIAL_INTERNET("2"),
	NAO_PRESENCIAL_TELEATENDIMENTO("3"),
	ENTREGA_DOMICILIO("4"),
	PRESENCIAL_FORA_DO_ESTABELECIMENTO("5"),
	NAO_PRESENCIAL_OUTROS("9");
	
	private String indicadorPresencaComprador;

	private IndPres(String IndicadorPresencaComprador){
		indicadorPresencaComprador = IndicadorPresencaComprador;
	}

	public String getIndicadorPresencaComprador() {
		return indicadorPresencaComprador;
	}	
}
