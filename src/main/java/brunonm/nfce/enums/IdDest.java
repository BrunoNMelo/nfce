package brunonm.nfce.enums;

/**
 * 1 - Interna
 * 2 - Interestadual
 * 3 - Exterior
 * @author Bruno Melo
 *
 */
public enum IdDest {
	INTERNA("1"),
	INTERESTADUAL("2"),
	EXTERIOR("3");
	
	private String idLocalDestino;

	private IdDest(String idLocalDestino){
		this.idLocalDestino = idLocalDestino;
		
	}
	
	public String getIdDestCod() {
		return idLocalDestino;
	}
}
