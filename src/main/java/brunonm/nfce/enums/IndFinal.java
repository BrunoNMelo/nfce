package brunonm.nfce.enums;

/**
 * Tem como objetivo de identificar se a operação foi realizada para consumidor final ou não.
 * 0 - "Normal" quando a operação não for realizada com consumidor final.  
 * 1 - "Consumidor Final" quando a operação for realizada com consumidor final. 
 * @author Bruno Melo
 *
 */
public enum IndFinal {
	NORMAL("0"),
	CONSUMIDOR_FINAL("1");
	
	private String indFinal;

	private IndFinal(String indFinal){
		this.indFinal = indFinal;
	}

	public String getIndFinal() {
		return indFinal;
	}
	
}
