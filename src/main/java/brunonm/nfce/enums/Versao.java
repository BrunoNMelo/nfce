package brunonm.nfce.enums;

/**
 * Versão do Schema da Nota Fiscal Eletrônica.
 * @author Bruno Melo
 *
 */
public enum Versao {
	
	V310("3.10"),
	V400("4.00");
	
	private String versao;

	private Versao(String versao){
		this.versao = versao;
	}

	public String getVersao() {
		return versao;
	}
}
