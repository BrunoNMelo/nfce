package brunonm.nfce.enums;

/**
 * 0 - sem Danfe
 * 1 - Danfe Retrato 
 * 2 - Danfe paisagem 
 * 3 - Danfe Simplificado
 * 4 - Danfe NFCE
 * 5 - Danfe NFCE mensagem eletrônica
 * @author Bruno Melo
 *
 */
public enum TpImp {
	
	SEM_DANFE(0),
	DANFE_RETRATO(1),
	DANFE_PAISAGEM(2),
	DANFE_SIMPLIFICADO(3),
	DANFE_NFCE(4),
	DANFE_NFCE_MENSAGEM_ELETRONICA(5);

	private int formatoImpressaoDanfe;

	private TpImp(int formatoImpressaoDanfe) {
		this.formatoImpressaoDanfe = formatoImpressaoDanfe;
	}
	
	public int getFormatoImpressaoDanfeCod() {
		return formatoImpressaoDanfe;
	}
}
