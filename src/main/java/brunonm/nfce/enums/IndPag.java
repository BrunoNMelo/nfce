package brunonm.nfce.enums;

/**
 * Indicador da forma de pagamento: 
 * 0 – pagamento à vista; 
 * 1 – pagamento à prazo; 
 * 2 – outros.
 * 
 * @author Bruno Melo
 *
 */
public enum IndPag {
	PAG_A_VISTA("0"),
	PAG_A_PRAZO("1"),
	OUTROS("2");
	
	private String indPag;

	private IndPag(String indPag) {
		this.indPag = indPag;
	}

	public String getIndPag() {
		return indPag;
	}
	
}
