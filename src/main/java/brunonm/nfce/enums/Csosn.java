package brunonm.nfce.enums;

/**
 * 101 - Tributada pelo Simples Nacional com permissão de crédito.
 * 102 - Tributada pelo Simples Nacional sem permissão de crédito. 
 * 103 – Isenção do ICMS  no Simples Nacional para faixa de receita bruta.
 * 300 – Imune.
 * 400 – Não tributda pelo Simples Nacional (v.2.0)
 * 201 - Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por Substituição Tributária (v.2.0)
 * 202 - Tributada pelo Simples Nacional sem permissão de crédito e com cobrança do ICMS por Substituição Tributária;
 * 203 -  Isenção do ICMS nos Simples Nacional para faixa de receita bruta e com cobrança do ICMS por Substituição Tributária
 * 500 – ICMS cobrado anterirmente por substituição tributária (substituído) ou por antecipação 
 * 900 - Tributação pelo ICMS
 * @author Bruno Melo
 *
 */
public enum Csosn {
	TRIB_PELO_SIMPLES_COM_PERM_CRED(101),
	TRIB_PELO_SIMPLES_SEM_PERM_CRED(102),
	TRIB_PELO_SIMPLES_COM_PERM_CRED_E_COBR_ICMS(201),
	TRIB_PELO_SIMPLES_SEM_PERM_CRED_E_COBR_ICMS(201),
	ISENCAO_ICMS_COM_SUBS_TRIBUTARIA(203),
	ISENCAO_DO_ICMS(103),
	IMUNE(300),
	NAO_TRIBUTADO_PELO_SIMPLES_NACIONAL(400),
	ICMS_COBRADO_ANTER_POR_SUBS_TRIB(500),
	TRIB_PELO_ICMS(900);
	
	private int codSimples;

	private Csosn(int codSimples) {
		this.codSimples = codSimples;
	}

	public String getCodSimples() {
		return String.valueOf(codSimples);
	}
}
