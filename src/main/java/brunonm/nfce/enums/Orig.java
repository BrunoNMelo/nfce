package brunonm.nfce.enums;

/**
 * origem da mercadoria: 
 * 0 - Nacional 
 * 1 - Estrangeira - Importação direta 
 * 2 - Estrangeira - Adquirida no mercado interno 
 * (v2.0)
 * @author Bruno Melo
 *
 */
public enum Orig {
	NACIONAL(0),
	ESTRANGEIRA_ID(1),
	ESTRANGEIRA_AMI(2);
	
	private int origem;

	private Orig(int origem){
		this.origem = origem;
	}

	public int getOrigem() {
		return origem;
	}

}
