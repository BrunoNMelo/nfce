package brunonm.nfce.enums;

/**
 * Modalidade de determinação da BC do ICMS ST
 * 0 - Preço tabelado ou máximo sugerido; 
 * 1 - Lista Negativa (valor);
 * 2 - Lista Positiva (valor);
 * 3 - Lista Neutra (valor);
 * 4 - Margem Valor Agregado (%);
 * 5 - Pauta (valor);
 * @author Bruno Melo
 *
 */
public enum ModBCST {
	PRECO_TABELADO("0"),
	LISTA_NEGATIVA("1"),
	LISTA_POSITIVA("2"),
	LISTA_NEUTRA("3"),
	MARGEM_VALOR_AGREGADO("4"),
	PAUTA("5");
	
	private String modBCST;

	private ModBCST(String modBCST) {
		this.modBCST = modBCST;
	}

	public String getModBCST() {
		return modBCST;
	}
}
