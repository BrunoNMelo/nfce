package brunonm.nfce.enums;

/**
 * Forma de emissão da NF-e
 * 1 - Normal;
 * 2 - Contingência FS
 * 3 - Contingência SCAN
 * 4 - Contingência DPEC
 * 5 - Contingência FSDA
 * 6 - Contingência SVC - AN
 * 7 - Contingência SVC - RS
 * 9 - Contingência off-line NFC-e
 * @author Bruno Melo
 *
 */
public enum TpEmis {
	
	NORMAL(1),
	CONTIGENCIA_FS(2),
	CONTIGENCIA_SCAN(3),
	CONTIGENCIA_DPEC(4),
	CONTIGENCIA_FSDA(5),
	CONTIGENCIA_SVC_AN(6),
	CONTIGENCIA_SVC_RS(7),
	CONTIGENCIA_OFFLINE(9);
	
	private int formaEmissaoNFe;

	private TpEmis(int formaEmissaoNFe) {
		this.formaEmissaoNFe = formaEmissaoNFe;
	}

	public int getFormaEmissaoNFe() {
		return formaEmissaoNFe;
	}
}
