package brunonm.nfce.enums;

/**
 * Indica se valor do Item (vProd) entra no valor total da NF-e (vProd)
 * 0  o valor do item (vProd) não compõe o valor total da NF-e (vProd) 
 * 1  o valor do item (vProd) compõe o valor total da NF-e (vProd) 
 * @author Bruno Melo
 *
 */
public enum IndTot {
	NAO_COMPOE_O_VALOR_TOTAL("O"),
	COMPOE_O_VALOR_TOTAL("1");
	
	private String indTot;

	private IndTot(String indTot){
		this.indTot = indTot;
	}

	public String getIndTot() {
		return indTot;
	}

}
