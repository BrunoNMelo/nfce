package brunonm.nfce.enums;

/**
 * Modelo da Nota Fiscal Eletrônica;
 * 55 - Nota Fiscal Eletrônica (NFe).
 * 65 - Nota Fiscal Eletrônica ai Consumidor (NFCe).
 * @author Bruno Melo
 *
 */
public enum ModeloNF {
	
	NFe("55"),
	NFCe("65");
	
	private String modeloNF;

	private ModeloNF(String modeloNF){
		this.modeloNF = modeloNF;
	}

	public String getModeloNF() {
		return modeloNF;
	}

}
