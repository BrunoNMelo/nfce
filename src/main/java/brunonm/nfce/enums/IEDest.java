package brunonm.nfce.enums;

/**
 * Indicador da IE do destinatário: 
 * 1 – Contribuinte ICMS pagamento à vista; 
 * 2 – Contribuinte isento de inscrição; 
 * 9 – Não Contribuinte;
 * 
 * @author Bruno Melo
 *
 */
public enum IEDest {
	
	CONTRIBUINTE_ICMS("1"),
	CONTRIBUINTE_ISENTO("2"),
	NAO_CONTRIBUINTE("9");
	
	private String iEdest;

	private IEDest(String iEdest){
		this.iEdest = iEdest;
	}

	public String getiEdest() {
		return iEdest;
	}	

}
