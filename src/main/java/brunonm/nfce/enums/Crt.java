package brunonm.nfce.enums;

/**
 * Código de Regime Tributário. 
 * Este campo será obrigatoriamente preenchido com:
 * 1 – Simples Nacional	;
 * 2 – Simples Nacional – excesso de sublimite de receita bruta;
 * 3 – Regime Normal.
 * @author Bruno Melo
 *
 */
public enum Crt {
	SIMPLES_NACIONAL(1),
	SIMPLES_NACIONAL_EXCESSO_SUBLIMITE_RB(2),
	REGIME_NORMAL(3);
	
	private int regimeTributario;

	private Crt(int regimeTributario){
		this.regimeTributario = regimeTributario;
	}

	public int getRegimeTributario() {
		return regimeTributario;
	}

}
