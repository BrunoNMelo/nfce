package brunonm.nfce.enums;

/**
 * 
 * @author Bruno Melo
 * 00 - Tributada integralmente
 * 10 - Tributada e com cobrança do ICMS por ST
 * 20 - Com redução de base de cálculo
 * 30 - Isenta/Não tributada e com cobrança do ICMS por ST
 * 40 - Isenta
 * 4	1 - Não   Tributada
 * 5	0 - Suspensão
 * 51 - Diferimento
 * 60 - ICMS   Cobrado Anteriormente por ST
 * 71 - Com redução de base de cálculo cobrança do ICMS por ST
 * 90 - Outras
 *
 */
public enum Cst {
	TRIBUTADA_INTEGRALMENTE("00"),
	TRIBUTADA_E_COM_COBR_ICMS("10"),
	COM_RED_BASE_CALCULO("20"),
	ISENTA_NAO_TRIB_E_COM_COBR_ICMS("30"),
	ISENTA("40"),
	NAO_TRIBUTADA("41"),
	SUSPENSAO("50"),
	DIFERIMENTO("51"),
	ICMS_COBRADO_ANTERIORMENTE("60"),
	COM_RED_DE_BASE_CAL_COB_ICMS("70"),
	OUTRAS("90");
	
	private String cST;

	private Cst(String CST){
		cST = CST;
	}

	public String getcST() {
		return cST;
	}
	
}
