package brunonm.nfce.enums;

/**
 * Finalidade da emissão da NF-e:
 * 1 - NFe normal
 * 2 - NFe complementar
 * 3 - NFe de ajuste
 * 4 - Devolução/Retorno
 * @author Bruno Melo
 *
 */
public enum FinNFe {
	NFE_NORMAL(1),
	NFE_COMPLEMENTAR(2),
	NFE_AJUSTE(3),
	DEVOLUCAO(4);
	
	private int finalidadeNfe;

	private FinNFe(int finalidadeNfe) {
		this.finalidadeNfe = finalidadeNfe;
	}

	public int getFinalidadeNfe() {
		return finalidadeNfe;
	}

}
