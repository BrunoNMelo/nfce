package brunonm.nfce.enums;

/**
 * 0 - Entrada
 * 1 - Saida
 * @author Bruno Melo
 *
 */
public enum TpNF {
	ENTRADA(0),
	SAIDA(1);
	
	private int tipo;

	private TpNF(int tipo){
		this.tipo = tipo;
	}
	
	public int getTipo() {
		return tipo;
	}

}
