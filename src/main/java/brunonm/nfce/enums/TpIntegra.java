package brunonm.nfce.enums;

/**
 * Tipo de Integração do processo de pagamento com o sistema de automação da
 * empresa/ 
 * 1 - Pagamento integrado com o sistema de automação da empresa Ex. equipamento TEF , Comercio Eletronico 
 * 2 - Pagamento não integrado com o sistema de automação da empresa Ex: equipamento POS
 * 
 * @author Bruno Melo
 *
 */
public enum TpIntegra {
	
	INTEGRADO("1"),
	NAO_INTEGRADO("2");
	
	private String tpIntegra;

	private TpIntegra(String tpIntegra){
		this.tpIntegra = tpIntegra;
	}

	public String getTpIntegra() {
		return tpIntegra;
	}

}
