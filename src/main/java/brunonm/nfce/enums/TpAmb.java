package brunonm.nfce.enums;

/**
 * 1 - Produção
 * 2 - Homologação
 * @author Bruno Melo
 *
 */
public enum TpAmb {
	PRODUCAO(1),
	HOMOLOGACAO(2);
	
	private int ambiente;

	private TpAmb(int ambiente) {
		this.ambiente = ambiente;
	}

	public int getAmbiente() {
		return ambiente;
	}

}
