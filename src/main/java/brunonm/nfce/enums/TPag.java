package brunonm.nfce.enums;

/**
 * Forma de Pagamento:
 * 01-Dinheiro;
 * 02-Cheque;
 * 03-Cartão de Crédito;
 * 04-Cartão de Débito;
 * 05-Crédito Loja;
 * 10-Vale Alimentação;
 * 11-Vale Refeição;
 * 12-Vale Presente;
 * 13-Vale Combustível;
 * 14 - Duplicata Mercantil;
 * 90 - Sem Pagamento;
 * 99 - Outros
 * @author Bruno Melo
 *
 */
public enum TPag {
	DINHEIRO("01"),
	CHEQUE("02"),
	CARTAO_CREDIDO("03"),
	CARTAO_DEBITO("04"),
	CREDITO_LOJA("05"),
	VALE_ALIMENTACAO("10"),
	VALE_REFEICAO("11"),
	VALE_PRESENTE("12"),
	VALE_COMBUSTIVEL("13"),
	DUPLICATA_MERCANTIL("14"),
	SEM_PAGAMENTO("90"),
	OUTRO("99");
	
	private String tipoPag;

	private TPag(String tipoPag){
		this.tipoPag = tipoPag;
	}

	public String getTipoPag() {
		return tipoPag;
	}
}
