package brunonm.nfce.enums;

/**
 * Bandeira da operadora de cartão de crédito/débito:
 * 01–Visa; 
 * 02–Mastercard; 
 * 03–American Express; 
 * 04–Sorocred;
 * 05-Diners Club;
 * 06-Elo;
 * 07-Hipercard;
 * 08-Aura;
 * 09-Cabal;
 * 99–Outros
 * @author Bruno Melo
 *
 */
public enum TBand {
	VISA("01", "visa"),
	MASTERCARD("02", "mastercard"),
	AMERICAN_EXPRESS("03", "american_express"),
	SOROCRED("04", "sorocred"),
	DINERS_CLUB("05", "diners_club"),
	ELO("06", "elo"),
	HIPERCARD("07", "hipercard"),
	AURA("08", "aura"),
	CABAL("09", "cabal"),
	OUTROS("99", "outros");
	
	private String tBandCod;
	private String tBandName;

	private TBand(String tBandCod, String tBandName) {
		this.tBandCod = tBandCod;
		this.tBandName = tBandName;
	}

	public String gettBandCod() {
		return tBandCod;
	}

	public String gettBandName() {
		return tBandName;
	}
	
}
