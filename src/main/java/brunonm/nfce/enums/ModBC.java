package brunonm.nfce.enums;

/**
 * Modalidade de determinação da BC do ICMS:
 * 0 - Margem Valor Agregado (%);
 * 1 - Pauta (valor);
 * 2 - Preço Tabelado Máximo (valor);
 * 3 - Valor da Operação.
 * @author Bruno Melo
 *
 */
public enum ModBC {
	MARGEM_VALOR_AGREGADO(0),
	PAUTA(1),
	PRECO_TABELADO_MAXIMO(2),
	VALOR_DA_OPERACAO(3);
	
	private int modBC;

	private ModBC(int modBC){
		this.modBC = modBC;
	}

	public String getModBC() {
		return String.valueOf(modBC);
	}

}
