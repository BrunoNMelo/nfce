package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import brunonm.nfce.modelo.InfAdic;

/**
 * Classe de teste para os modelos InfAdic
 * @author Bruno Melo
 *
 */
public class InfAdicTest {
	@Test
	public void testeInfAdic() {
		InfAdic infAdic = new InfAdic.Builder()
				.comInfCpl("Informação Dummy")
				.build();
		
		assertEquals("Informação Dummy", infAdic.getInfCpl());
	}

}
