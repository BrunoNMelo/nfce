package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import brunonm.nfce.enums.TBand;
import brunonm.nfce.enums.TPag;
import brunonm.nfce.enums.TpIntegra;
import brunonm.nfce.modelo.Pag;

/**
 * Classe de teste para os modelos Pag
 * 
 * @author Bruno Melo
 *
 */
public class PagTest {
	@Test
	public void testePag() {
		Pag pag = new Pag.Builder(TPag.DINHEIRO, "2.90").build();

		assertEquals("01", pag.gettPag());
		assertEquals("2.90", pag.getvPag());
		assertEquals(null, pag.getcNPJ());
		assertEquals(null, pag.gettBand());
		assertEquals(null, pag.getcAut());
	}

	@Test
	public void testePagComOutrosValores() {
		Pag pag = new Pag.Builder(TPag.CARTAO_CREDIDO, "20.50", TpIntegra.INTEGRADO)
				.comCNPJ("22819856000126")
				.comtBand(TBand.VISA)
				.comcAut("1234567")
				.build();

		assertEquals("1", pag.getTpIntegra());
		assertEquals("03", pag.gettPag());
		assertEquals("20.50", pag.getvPag());
		assertEquals("22819856000126", pag.getcNPJ());
		assertEquals("01", pag.gettBand());
		assertEquals("1234567", pag.getcAut());
	}

}
