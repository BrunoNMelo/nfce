package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Test;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.FinNFe;
import brunonm.nfce.enums.IdDest;
import brunonm.nfce.enums.IndFinal;
import brunonm.nfce.enums.IndPres;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.TpImp;
import brunonm.nfce.enums.TpNF;
import brunonm.nfce.modelo.Ide;

/**
 * Classe de teste para os modelos Ide
 * @author Bruno Melo
 *
 */
public class IdeTest {
	@Test
	public void testeIde() throws ParseException {
		Ide ide = new Ide.Builder(Estados.PB, 948, "VENDA", 1, Ide.SDF.parse("2015-09-25T21:15:28-03:00"),
				TpNF.SAIDA, IdDest.INTERNA, 2507507, TpImp.DANFE_NFCE, TpEmis.CONTIGENCIA_OFFLINE, TpAmb.PRODUCAO,
				FinNFe.NFE_NORMAL, IndFinal.CONSUMIDOR_FINAL, IndPres.PRESENCIAL, "1.0", "22819856000126")
				.comDhSaiEnt(Ide.SDF.parse("2017-12-04T15:25:10-03:00"))
				.comDhCont(Ide.SDF.parse("2015-09-25T21:15:28-03:00"))
				.comxJust("Nao foi possivel obter comunicacao com o servidor")
				.build();
		
		assertEquals(25, ide.getcUF().getCodigoIbge());
		assertEquals("00000948", ide.getcNF());
		assertEquals("VENDA", ide.getNatOP());
		assertEquals("65", ide.getMod().getModeloNF());
		assertEquals("1", ide.getSerie());
		assertEquals("948", ide.getnNF());
		assertEquals("2015-09-25T21:15:28-03:00", ide.getDhEmi());
		assertEquals("2017-12-04T15:25:10-03:00", ide.getDhSaiEnt());
		assertEquals(1, ide.getTpNF());
		assertEquals("1", ide.getIdDest());
		assertEquals(2507507, ide.getcMunFG());
		assertEquals(4, ide.getTpImp());
		assertEquals(9, ide.getTpEmis().getFormaEmissaoNFe());
		assertEquals(5, ide.getcDV());
		assertEquals(1, ide.getTpAmb().getAmbiente());
		assertEquals(1, ide.getFinNFe());
		assertEquals("1", ide.getIndFinal());
		assertEquals("1", ide.getIndPres());
		assertEquals(0, ide.getProcEmi());
		assertEquals("1.0", ide.getVerProc());
		assertEquals("NFe25150922819856000126650010000009489000009485", ide.getNfID());
		assertEquals("2015-09-25T21:15:28-03:00", ide.getDhCont());
		assertEquals("Nao foi possivel obter comunicacao com o servidor", ide.getxJust());
	}

	@Test
	public void testeIdeComOutrosValores() throws ParseException {
		Ide ide = new Ide.Builder(Estados.PB, 18421, "VENDA", 1, Ide.SDF.parse("2016-10-03T14:03:25-03:00"),
				TpNF.SAIDA, IdDest.INTERNA, 2507507, TpImp.DANFE_NFCE, TpEmis.NORMAL, TpAmb.PRODUCAO, FinNFe.NFE_NORMAL,
				IndFinal.CONSUMIDOR_FINAL, IndPres.PRESENCIAL, "1.0","00146429000191").build();

		assertEquals(25, ide.getcUF().getCodigoIbge());
		assertEquals("00018421", ide.getcNF());
		assertEquals("VENDA", ide.getNatOP());
		assertEquals("65", ide.getMod().getModeloNF());
		assertEquals("1", ide.getSerie());
		assertEquals("18421", ide.getnNF());
		assertEquals("2016-10-03T14:03:25-03:00", ide.getDhEmi());
		assertEquals(null, ide.getDhSaiEnt());
		assertEquals(1, ide.getTpNF());
		assertEquals("1", ide.getIdDest());
		assertEquals(2507507, ide.getcMunFG());
		assertEquals(4, ide.getTpImp());
		assertEquals(1, ide.getTpEmis().getFormaEmissaoNFe());
		assertEquals(1, ide.getcDV());
		assertEquals(1, ide.getTpAmb().getAmbiente());
		assertEquals(1, ide.getFinNFe());
		assertEquals("1", ide.getIndFinal());
		assertEquals("1", ide.getIndPres());
		assertEquals(0, ide.getProcEmi());
		assertEquals("1.0", ide.getVerProc());
		assertEquals("NFe25161000146429000191650010000184211000184211", ide.getNfID());
		assertEquals(null, ide.getDhCont());
		assertEquals(null, ide.getxJust());
	}
}
