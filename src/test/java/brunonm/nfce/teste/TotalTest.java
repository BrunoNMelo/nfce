package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import brunonm.nfce.enums.Csosn;
import brunonm.nfce.enums.Cst;
import brunonm.nfce.enums.IndTot;
import brunonm.nfce.enums.Orig;
import brunonm.nfce.modelo.Det;
import brunonm.nfce.modelo.Imposto;
import brunonm.nfce.modelo.Total;

/**
 * Classe de teste para os modelos Total
 * @author Bruno Melo
 *
 */
public class TotalTest {
	@Test
	public void testeTotal() {
		List<Det> dets = new ArrayList<>();
		Det det = new Det.Builder("9999",
				"",
				"taxa servico",
				88888888,
				5101,
				"UNID",
				"1",
				"7.10",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Csosn.TRIB_PELO_SIMPLES_SEM_PERM_CRED,
						Orig.NACIONAL).build()).build();
		
		dets.add(det);
		
		Total total = new Total(dets);
		
		assertEquals("0.00", total.getvBC());
		assertEquals("0.00", total.getvICMS());
		assertEquals("0.00", total.getvICMSDeson());
		assertEquals("0.00", total.getvBCST());
		assertEquals("0.00", total.getvST());
		assertEquals("7.10", total.getvProd());
		assertEquals("0.00", total.getvFCP());
		assertEquals("0.00", total.getvFCPST());
		assertEquals("0.00", total.getvFCPSTRet());
		assertEquals("0.00", total.getvFrete());
		assertEquals("0.00", total.getvSeg());
		assertEquals("0.00", total.getvDesc());
		assertEquals("0.00", total.getvII());
		assertEquals("0.00", total.getvIPI());
		assertEquals("0.00", total.getvIPIDevol());
		assertEquals("0.00", total.getvPIS());
		assertEquals("0.00", total.getvCOFINS());
		assertEquals("0.00", total.getvOutros());
		assertEquals("7.10", total.getvNF());
	}
	
	@Test
	public void testeTotalComMaisValores() {
		List<Det> dets = new ArrayList<>();
		Det det = new Det.Builder("9999",
				"",
				"taxa servico",
				88888888,
				5101,
				"UNID",
				"1",
				"7.10",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Cst.TRIBUTADA_INTEGRALMENTE,
						Orig.NACIONAL).comvBC("7.10").compICMS("18.00")
				.build()).build();
		
		Det det2 = new Det.Builder("100",
				"",
				"AGUA MINERAL",
				21069090,
				5102,
				"UNID",
				"1",
				"20.90",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Cst.TRIBUTADA_INTEGRALMENTE,
						Orig.NACIONAL).comvBC("20.90").compICMS("18.00")
				.build())
				.comvDesc("0.50")
				.comvFrete("0.50")
				.comvOutro("5.00")
				.comvSeg("2.00").build();
		
		Det det3 = new Det.Builder("100",
				"",
				"AGUA MINERAL",
				21069090,
				5102,
				"UNID",
				"1",
				"5.00",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Cst.ICMS_COBRADO_ANTERIORMENTE,
						Orig.NACIONAL).build())
				.comvDesc("1.00")
				.comvFrete("0.20")
				.comvOutro("1.00")
				.comvSeg("0.30")
				.build();
		
		dets.add(det);
		dets.add(det2);
		dets.add(det3);
		
		Total total = new Total(dets);
		
		assertEquals("28.00", total.getvBC());
		assertEquals("5.04", total.getvICMS());
		assertEquals("0.00", total.getvICMSDeson());
		assertEquals("0.00", total.getvBCST());
		assertEquals("0.00", total.getvST());
		assertEquals("33.00", total.getvProd());
		assertEquals("0.00", total.getvFCP());
		assertEquals("0.00", total.getvFCPST());
		assertEquals("0.00", total.getvFCPSTRet());
		assertEquals("0.70", total.getvFrete());
		assertEquals("2.30", total.getvSeg());
		assertEquals("1.50", total.getvDesc());
		assertEquals("0.00", total.getvII());
		assertEquals("0.00", total.getvIPI());
		assertEquals("0.00", total.getvIPIDevol());
		assertEquals("0.00", total.getvPIS());
		assertEquals("0.00", total.getvCOFINS());
		assertEquals("6.00", total.getvOutros());
		assertEquals("40.50", total.getvNF());
	}

}
