package brunonm.nfce.teste;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.utils.CriadorXmlAutorizacao;
import brunonm.nfce.utils.CriadorXmlStatusServico;


public class SOAPTest {

	public static void main(String[] args) {
		try {
			System.clearProperty("javax.net.ssl.keyStore");
			System.clearProperty("javax.net.ssl.keyStorePassword");
			System.clearProperty("javax.net.ssl.trustStore");
			System.clearProperty("javax.net.ssl.trustStorePassword");
			System.clearProperty("https.protocols");
			
			System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
			System.setProperty("javax.net.ssl.keyStore", "ServiSoft.pfx");
			System.setProperty("javax.net.ssl.keyStorePassword", "12345678");
			
			System.setProperty("javax.net.ssl.trustStoreType", "JKS");
			System.setProperty("javax.net.ssl.trustStore", "resources/cacerts/NFeCacertsProd");
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

			System.setProperty("https.protocols", "TLSv1.2");
			
			
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			CriadorXmlAutorizacao criadorXmlAutorizacao = CriadorXmlTest.criarXmlAutorizacao();
			
			
			CriadorXmlStatusServico criadorXmlConsulta = new CriadorXmlStatusServico(Estados.PB, Versao.V310, TpAmb.HOMOLOGACAO);
//			CriadorEnvelopeSoap criadorEnvelopeSoap = new CriadorEnvelopeSoap(criadorXmlAutorizacao);
			
//			SOAPMessage sOAPRequest = criadorEnvelopeSoap.createSOAPRequest();
//			
//			criadorEnvelopeSoap.printSoapMenssage(sOAPRequest);
	
			
			// Send SOAP Message to SOAP Server
			String url = "https://nfce.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx";
//			SOAPMessage soapResponse = soapConnection.call(sOAPRequest, url);

			// print SOAP Response
			System.out.print("Response SOAP Message:");

//			soapResponse.writeTo(System.out);
			soapConnection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
