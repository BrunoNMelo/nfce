package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import brunonm.nfce.enums.IEDest;
import brunonm.nfce.modelo.Dest;
import brunonm.nfce.modelo.Endereco;

public class DestTest {

	@Test
	public void testeDest() {
		Dest dest = new Dest.Builder("12345678000102", false, IEDest.CONTRIBUINTE_ISENTO).build();

		assertEquals("12345678000102", dest.getCnpj());
		assertEquals("2", dest.getIndIEDest());
		assertEquals(null, dest.getCpf());
		assertEquals(null, dest.getIdEstrangeiro());
		assertEquals(null, dest.getEndereco());
		assertEquals(null, dest.getEmail());
		assertEquals(null, dest.getiE());
		assertEquals(null, dest.getiM());
		assertEquals(null, dest.getiSUF());
		assertEquals(null, dest.getxNome());
	}

	@Test
	public void testeDestComValoresCompletos() {
		Dest dest = new Dest.Builder("12345678000", false, IEDest.NAO_CONTRIBUINTE)
				.comEmail("teste@teste.com")
				.comxNome("fulano de tal")
				.comIE("1234678")
				.comIM("12346789")
				.comISUF("123456789")
				.comEndereco(new Endereco.Builder("rua das rosas", "16", "Carvalho", 2507507, "JOAO PESSOA", "PB").build())
				.build();
	
		
		assertEquals("12345678000", dest.getCpf());
		assertEquals("9", dest.getIndIEDest());
		assertEquals(null, dest.getCnpj());
		assertEquals(null, dest.getIdEstrangeiro());
		assertEquals("TESTE@TESTE.COM", dest.getEmail());
		assertEquals("1234678", dest.getiE());
		assertEquals("12346789", dest.getiM());
		assertEquals("123456789", dest.getiSUF());
		assertEquals("FULANO DE TAL", dest.getxNome());
		assertEquals("RUA DAS ROSAS", dest.getEndereco().getxLgr());
		assertEquals("16", dest.getEndereco().getNro());
		assertEquals("CARVALHO", dest.getEndereco().getxBairro());
		assertEquals(2507507, dest.getEndereco().getcMun());
		assertEquals("JOAO PESSOA", dest.getEndereco().getxMun());
		assertEquals("PB", dest.getEndereco().getUf());
		assertEquals(null, dest.getEndereco().getCep());
	}

}
