package brunonm.nfce.teste;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import brunonm.nfce.enums.Crt;
import brunonm.nfce.enums.Csosn;
import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.FinNFe;
import brunonm.nfce.enums.IEDest;
import brunonm.nfce.enums.IdDest;
import brunonm.nfce.enums.IndFinal;
import brunonm.nfce.enums.IndPag;
import brunonm.nfce.enums.IndPres;
import brunonm.nfce.enums.IndTot;
import brunonm.nfce.enums.Orig;
import brunonm.nfce.enums.TBand;
import brunonm.nfce.enums.TPag;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.TpEmis;
import brunonm.nfce.enums.TpImp;
import brunonm.nfce.enums.TpIntegra;
import brunonm.nfce.enums.TpNF;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;
import brunonm.nfce.modelo.Dest;
import brunonm.nfce.modelo.Det;
import brunonm.nfce.modelo.Emit;
import brunonm.nfce.modelo.Endereco;
import brunonm.nfce.modelo.Ide;
import brunonm.nfce.modelo.Imposto;
import brunonm.nfce.modelo.InfAdic;
import brunonm.nfce.modelo.InfoCert;
import brunonm.nfce.modelo.Pag;
import brunonm.nfce.utils.CriadorXmlAutorizacao;

/**
 * Classe de teste para o criador de xml
 * 
 * @author Bruno Melo
 *
 */
public class CriadorXmlTest {

	public static void main(String[] args) throws ParseException {

		CriadorXmlAutorizacao criadorXml = criarXmlAutorizacao();
	
	}

	/**
	 * @return CriadorXmlAutorizacao já pronto para teste
	 */
	protected static CriadorXmlAutorizacao criarXmlAutorizacao() {
		Ide ide = new Ide.Builder(Estados.PB, 564, "VENDA", IndPag.PAG_A_VISTA, 1, new Date(), TpNF.SAIDA, IdDest.INTERNA,
				2507507, TpImp.DANFE_NFCE, TpEmis.NORMAL, TpAmb.PRODUCAO, FinNFe.NFE_NORMAL, IndFinal.CONSUMIDOR_FINAL,
				IndPres.PRESENCIAL, "1.0", "00146429000191").build();

		// ide.setDhCont(new Date());
		// ide.setxJust("Sistema offline");

		Emit emit = new Emit.Builder("00146429000191", "TESTE22222",
				new Endereco.Builder("RUA SEVERINO NICOLAU DE MELO", "700", "JARDIM OCEANIA", 2507507, "JOAO PESSOA", "PB",
						"58037700").comFone("35760006").build(),
				"161061761", Crt.SIMPLES_NACIONAL, "000003", "D8323BAF-B34F-2062-10AE-B1654204BEE6")
				.comxFant("TESTE2222").build();

		Dest dest = new Dest.Builder("01221969455", false, IEDest.NAO_CONTRIBUINTE)
				.comEmail("brunonobrega.melo@gmail.com").comxNome("Bruno nóbrega de melo")
				.comEndereco(new Endereco.Builder("rua das rosas", "16", "Carvalho", 2507507, "JOAO PESSOA", "PB").build()).build();

		// dest.setiE("1234678");
		// dest.setiM("12346789");
		// dest.setiSUF("123456789");

		List<Det> dets = new ArrayList<>();
		Det det = new Det.Builder("1000", "", "EMBALAGEM", 21069090, 5102, "UN", "1", "1.00", "", IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto
				.Builder(Csosn.TRIB_PELO_SIMPLES_SEM_PERM_CRED, Orig.NACIONAL)
				.build()).build();

		// det.getImposto().setModBC(ModBC.MARGEM_VALOR_AGREGADO);
		// det.getImposto().setpICMS("18.00");

		Det det2 = new Det.Builder("100", "", "AGUA MINERAL", 21069090, 5102, "UNID", "1", "1.50", "",
				IndTot.COMPOE_O_VALOR_TOTAL, new Imposto
				.Builder(Csosn.TRIB_PELO_SIMPLES_SEM_PERM_CRED, Orig.NACIONAL)
				.build())
				.comvDesc("0.50")
				.build();

		// det2.getImposto().setModBCST(ModBCST.MARGEM_VALOR_AGREGADO);
		// det2.getImposto().setpMVAST("75.00");
		// det2.getImposto().setpICMSST("12.00");
		// det2.getImposto().setpCredSN("50.00");
		// det2.getImposto().setpICMS("18.00");


		Det det3 = new Det.Builder("2", "", "fritas rusticas", 21069090, 5101, "UNID", "1", "14.00", "",
				IndTot.COMPOE_O_VALOR_TOTAL, new Imposto
				.Builder(Csosn.TRIB_PELO_SIMPLES_SEM_PERM_CRED, Orig.NACIONAL)
				.build())
				.comCEST("1234567")
				.comEXTIPI("123")
				.comNVE("AB1234")
				.comvDesc("1.10")
				.comvFrete("0.50")
				.comvOutro("5.00")
				.comvSeg("2.00").build();

		dets.add(det);
		dets.add(det2);
		// dets.add(det3);

		List<Pag> pags = new ArrayList<>();
		Pag pag = new Pag.Builder(TPag.CARTAO_CREDIDO, "1.00", TpIntegra.INTEGRADO)
				.comCNPJ("22819856000126")
				.comtBand(TBand.VISA)
				.comcAut("1234567")
				.comvTroco("3.00")
				.build();
		Pag pag2 = new Pag.Builder(TPag.DINHEIRO, "1.00").build();

		pags.add(pag);
		pags.add(pag2);

		InfAdic infAdic = new InfAdic.Builder()
				.comInfCpl("PROCON 0800 083 2015 / Parque Solon de Lucena, 234 (83)3218-6959 MESA/CMD: 1")
				.build();

		String caminhoCertificado = "ServiSoft.pfx";

		InfoCert infoCert = new InfoCert(caminhoCertificado, "12345678");

		CriadorXmlAutorizacao criadorXml = new CriadorXmlAutorizacao(Versao.V310, infoCert, 7, ide, emit, dets, pags,
				infAdic);
		criadorXml.setDest(dest);

		return criadorXml;
	}

}