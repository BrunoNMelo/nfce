package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import brunonm.nfce.enums.Crt;
import brunonm.nfce.modelo.Emit;
import brunonm.nfce.modelo.Endereco;

/**
 * Classe de teste para os modelos Emit
 * 
 * @author Bruno Melo
 *
 */
public class EmitTest {
	@Test
	public void testeEmit() {
		Endereco endereco = new Endereco.Builder("rua professor manoel coutinho", "135", "estados", 2507507,
				"JOAO PESSOA", "PB", "58030222").comFone("08335087000").comxCpl("sl 3").build();

		Emit emit = new Emit.Builder("00146429000191", "servisoft informatica", endereco, "isento",
				Crt.SIMPLES_NACIONAL, "000001", "8e3fbe487ba2dd22e946196b4c3743e")
						.comxFant("restaurante farofinha com arroz").comCNAE("1234567").comIEST("12").comIM("1234")
						.build();

		assertEquals("00146429000191", emit.getCnpj());
		assertEquals(null, emit.getCpf());
		assertEquals("SERVISOFT INFORMATICA", emit.getxNome());
		assertEquals("RUA PROFESSOR MANOEL COUTINHO", emit.getEndereco().getxLgr());
		assertEquals("RESTAURANTE FAROFINHA COM ARROZ", emit.getxFant());
		assertEquals("135", emit.getEndereco().getNro());
		assertEquals("ESTADOS", emit.getEndereco().getxBairro());
		assertEquals(2507507, emit.getEndereco().getcMun());
		assertEquals("SL 3", emit.getEndereco().getxCpl());
		assertEquals("JOAO PESSOA", emit.getEndereco().getxMun());
		assertEquals("PB", emit.getEndereco().getUf());
		assertEquals("58030222", emit.getEndereco().getCep());
		assertEquals("08335087000", emit.getEndereco().getFone());
		assertEquals("ISENTO", emit.getIe());
		assertEquals(1, emit.getCrt());
		assertEquals("1234567", emit.getcNAE());
		assertEquals("12", emit.getiEST());
		assertEquals("1234", emit.getiM());
	}

	@Test
	public void testeEmitComOutrosValores() {
		Endereco endereco = new Endereco.Builder("R ARTHUR ENEDINO DOS ANJOS", "078", "ALTIPLANO", 2507507,
				"JOAO PESSOA", "PB", "58046180").comFone("08332447605").build();
		
		Emit emit = new Emit.Builder("22819856000126", "RENAN FONSECA RODRIGUES - ME", endereco, "162564910",
				Crt.SIMPLES_NACIONAL, "000001", "8e3fbe487ba2dd22e946196b4c3743e").build();


		assertEquals("22819856000126", emit.getCnpj());
		assertEquals(null, emit.getCpf());
		assertEquals("RENAN FONSECA RODRIGUES - ME", emit.getxNome());
		assertEquals("R ARTHUR ENEDINO DOS ANJOS", emit.getEndereco().getxLgr());
		assertEquals(null, emit.getxFant());
		assertEquals("078", emit.getEndereco().getNro());
		assertEquals("ALTIPLANO", emit.getEndereco().getxBairro());
		assertEquals(2507507, emit.getEndereco().getcMun());
		assertEquals(null, emit.getEndereco().getxCpl());
		assertEquals("JOAO PESSOA", emit.getEndereco().getxMun());
		assertEquals("PB", emit.getEndereco().getUf());
		assertEquals("58046180", emit.getEndereco().getCep());
		assertEquals("08332447605", emit.getEndereco().getFone());
		assertEquals("162564910", emit.getIe());
		assertEquals(1, emit.getCrt());
		assertEquals(null, emit.getcNAE());
		assertEquals(null, emit.getiEST());
		assertEquals(null, emit.getiM());
	}

}
