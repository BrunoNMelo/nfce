package brunonm.nfce.teste;

import brunonm.nfce.enums.Estados;
import brunonm.nfce.enums.TpAmb;
import brunonm.nfce.enums.Versao;
import brunonm.nfce.exceptions.CertificadoInvalidoException;
import brunonm.nfce.exceptions.VersaoNFCeErradaException;
import brunonm.nfce.exceptions.WebServiceInexistenteException;
import brunonm.nfce.modelo.InfoCert;
import brunonm.nfce.utils.CriadorXmlAutorizacao;
import brunonm.nfce.utils.CriadorXmlStatusServico;
import brunonm.nfce.utils.EnviarNFCe;
import brunonm.nfce.utils.RespostaConsultaStatus;

public class EnviarNFCeTest {

	public static void main(String[] args) {
		try {
			CriadorXmlStatusServico criadorXmlConsulta = new CriadorXmlStatusServico(Estados.PB, Versao.V310, TpAmb.PRODUCAO);
			CriadorXmlAutorizacao criadorXmlAutorizacao = CriadorXmlTest.criarXmlAutorizacao();

			String caminhoCertificado = "ServiSoft.pfx";

			InfoCert infoCert = new InfoCert(caminhoCertificado, "12345678");

			EnviarNFCe nfCe = new EnviarNFCe(infoCert);
			nfCe.configuracao()
			.salvaArquivosDeEnvioEResposta(true, "teste")
			.pastaArquivosNFe("NFe")
			.salvarEnvelopeSOAP(true, "teste/")
			.separarPastasMensalmente(true)
			.salvarConfiguracoes();

			RespostaConsultaStatus resposta = nfCe.requisicaoConsultaStatusServico(criadorXmlConsulta);
			
			System.out.println("resposta " + resposta.getxMotivo());

		} catch (CertificadoInvalidoException | VersaoNFCeErradaException | WebServiceInexistenteException e) {
			e.printStackTrace();
		}
	}

}
