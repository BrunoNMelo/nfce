package brunonm.nfce.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import brunonm.nfce.enums.Csosn;
import brunonm.nfce.enums.Cst;
import brunonm.nfce.enums.IndTot;
import brunonm.nfce.enums.ModBC;
import brunonm.nfce.enums.Orig;
import brunonm.nfce.modelo.Det;
import brunonm.nfce.modelo.Imposto;

/**
 * Classe de teste para os modelos Det
 * @author Bruno Melo
 *
 */
public class DetTest {
	@Test
	public void testeDet() {
		Det det = new Det.Builder("100",
				"",
				"AGUA MINERAL",
				21069090,
				5102,
				"UNID",
				"1",
				"12.69",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Cst.TRIBUTADA_INTEGRALMENTE, Orig.NACIONAL)
				.comModBC(ModBC.MARGEM_VALOR_AGREGADO)
				.comvBC("12.69")
				.compICMS("18.00")
				.build())
				.comCEST("1234567")
				.comEXTIPI("123")
				.comNVE("AB1234")
				.comvDesc("1.10")
				.comvFrete("0.50")
				.comvOutro("5.00")
				.comvSeg("2.00")
				.build();
		
		assertEquals("100", det.getcProd());
		assertEquals("AGUA MINERAL", det.getxProd());
		assertEquals("", det.getcEAN());
		assertEquals(21069090, det.getnCM());
		assertEquals(5102, det.getcFOP());
		assertEquals("UNID", det.getuCom());
		assertEquals("1.0000", det.getqCom());
		assertEquals("12.6900000000", det.getvUnCom());
		assertEquals("12.69", det.getvProd());
		assertEquals("", det.getcEANTrib());
		assertEquals("UNID", det.getuTrib());
		assertEquals("1.0000", det.getqTrib());
		assertEquals("12.6900000000", det.getvUnTrib());
		assertEquals("1", det.getIndTot());
		assertEquals("1234567", det.getcEST());
		assertEquals("123", det.geteXTIPI());
		assertEquals("AB1234", det.getnVE());
		assertEquals("1.10", det.getvDesc());
		assertEquals("0.50", det.getvFrete());
		assertEquals("5.00", det.getvOutro());
		assertEquals("2.00", det.getvSeg());
		//Imposto
		assertEquals(0, det.getImposto().getOrig());
		assertEquals(null, det.getImposto().getcSOSN());
		assertEquals("00", det.getImposto().getcST());
		assertEquals("12.69", det.getImposto().getvBC());
		assertEquals("0", det.getImposto().getModBC());
		assertEquals("12.69", det.getImposto().getvBC());
		assertEquals("18.00", det.getImposto().getpICMS());
		assertEquals("2.28", det.getImposto().getvICMS());
		
		assertEquals("0.0000", det.getImposto().getpPIS());
		assertEquals("0.00", det.getImposto().getvPIS());
		assertEquals("0.0000", det.getImposto().getpCOFINS());
		assertEquals("0.00", det.getImposto().getvCOFINS());
	}
	
	@Test
	public void testeDetComOutrosValores() {
		Det det = new Det.Builder("9999",
				"",
				"taxa servico",
				88888888,
				5101,
				"UNID",
				"1",
				"7.10",
				"",
				IndTot.COMPOE_O_VALOR_TOTAL,
				new Imposto.Builder(Csosn.TRIB_PELO_SIMPLES_SEM_PERM_CRED,
						Orig.NACIONAL).build()).build();
		
		assertEquals("9999", det.getcProd());
		assertEquals("TAXA SERVICO", det.getxProd());
		assertEquals("", det.getcEAN());
		assertEquals(88888888, det.getnCM());
		assertEquals(5101, det.getcFOP());
		assertEquals("UNID", det.getuCom());
		assertEquals("1.0000", det.getqCom());
		assertEquals("7.1000000000", det.getvUnCom());
		assertEquals("7.10", det.getvProd());
		assertEquals("", det.getcEANTrib());
		assertEquals("UNID", det.getuTrib());
		assertEquals("1.0000", det.getqTrib());
		assertEquals("7.1000000000", det.getvUnTrib());
		assertEquals("1", det.getIndTot());
		assertEquals(null, det.getcEST());
		assertEquals(null, det.geteXTIPI());
		assertEquals(null, det.getnVE());
		assertEquals(null, det.getvDesc());
		assertEquals(null, det.getvFrete());
		assertEquals(null, det.getvOutro());
		assertEquals(null, det.getvSeg());
		//Imposto
		assertEquals(0, det.getImposto().getOrig());
		assertEquals("102", det.getImposto().getcSOSN());
		assertEquals(null, det.getImposto().getcST());
		assertEquals(null, det.getImposto().getvBC());
		assertEquals("0.0000", det.getImposto().getpPIS());
		assertEquals(null, det.getImposto().getvPIS());
		assertEquals("0.0000", det.getImposto().getpCOFINS());
		assertEquals(null, det.getImposto().getvCOFINS());
	}
}
